Namespace.declare("net.lugdunon.server.worldgen.procedural.post");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.post.IPostProcessor");

////

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.init=function(initData)
{
	this.config        =initData.config;
	this.changeDelegate=initData.changeDelegate;
	this.content       =null;
	
	return(this);
}

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.show=function()
{
	var context=this;
	
	this.content=net.lugdunon.ui.Dialog.confirm(
		this.getName(),
		{
			dialogWidth :this.getWidth(),
			dialogHeight:this.getHeight()
		},
		this,
		function(okClicked)
		{
			if(okClicked)
			{
				context.changeDelegate.postProcessorConfigurationUpdated.call(context.changeDelegate);
			}
			
			return(true);
		},
		function(body,ok,cancel)
		{
			context.renderContent.call(context,body)
		}
	);
};

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.getName=function()
{
	return("Implement Me: getName()!");
}

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.getDescription=function()
{
	return("Implement Me: getDescription()!");
}

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.getWidth=function()
{
	return(320);
}

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.getHeight=function()
{
	return(240);
}

net.lugdunon.server.worldgen.procedural.post.IPostProcessor.prototype.renderContent=function(parentEl)
{
	;
}