package net.lugdunon.server.worldgen.procedural.post;

import java.util.Random;

import org.json.JSONObject;

public interface IPostProcessor
{
	public void process(float[] heightMap, int dim, Random rand, JSONObject args) throws Exception;
}
