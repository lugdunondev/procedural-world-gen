package net.lugdunon.server.worldgen.procedural.post;

import java.util.Arrays;
import java.util.Random;

import net.lugdunon.util.FastMath;

import org.json.JSONObject;

public class ErosionPostProcessor implements IPostProcessor
{
	private int[][]  DIRECTIONS=new int[][]
	{
		{-1,-1},
		{-1, 0},
		{-1, 1},
		{ 0,-1},
		{ 0, 0},
		{ 0, 1},
		{ 1,-1},
		{ 1, 0},
		{ 1, 1}
	};

	private long            maxCount;
	private long []         freqCount;
	private float[]         heightMap;
	private int             dim;
	private Random          rand;
	private Random          dirRand;
	private int             x;
	private int             y;
	private int             nx;
	private int             ny;
	private float           nv;
	
	@Override
	public void process(float[] heightMap, int dim, Random rand, JSONObject args) throws Exception
	{
		int    passes           =args.getInt("passes");
		int    iterationsPerPass=args.getInt("iterationsPerPass");
		
		this.freqCount     =new long[dim*dim];
		this.heightMap     =heightMap;
		this.dim           =dim;
		this.rand          =rand;
		
		//INITIALIZE
		Arrays.fill(freqCount, 0);
		maxCount=0;
		
		int xx;
		int yy;
		
		for(int i=0;i<passes;i++)
		{
			xx     =this.rand.nextInt(dim);
			yy     =this.rand.nextInt(dim);
			dirRand=new Random(this.rand.nextLong());
		
			for(int j=0;j<iterationsPerPass;j++)
			{
				x=xx;
				y=yy;
				
				while(valueAt(x,y) > 0.0)
				{
					chooseDirections();
					
					if(!advance(0))
					{
						if(!advance(1))
						{
							if(!advance(2))
							{
								if(!advance(3))
								{
									if(!advance(4))
									{
										if(!advance(5))
										{
											if(!advance(6))
											{
												if(!advance(7))
												{
													if(!advance(8))
													{
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
//		long   l=0;
//		long   h=0;
//		
//		for(int i=0;i<freqCount.length;i++)
//		{
//			l+=freqCount[i];
//			
//			if(freqCount[i] > 0)
//			{
//				h++;
//			}
//		}
//		
//		BufferedImage er=new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);
//		BufferedImage hm=new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);
//		Graphics2D    eg =er.createGraphics();
//		Graphics2D    hg =hm.createGraphics();
//
//		for(int y=0;y<dim;y++)
//		{
//			for(int x=0;x<dim;x++)
//			{
//				float v=(float) (1.0-(freqCount[x+(y*dim)]/(double)maxCount));
//				
//				eg.setColor(new Color(v,v,v));
//				eg.fillRect(x, y, 1, 1);
//				
//				v=heightMap[x+(y*dim)];
//
//				hg.setColor(new Color(v,v,v));
//				hg.fillRect(x, y, 1, 1);
//			}
//		}
//
//		ImageIO.write(er, "png", new FileOutputStream(new File("/Users/christophergray/Desktop/ls/dat/erosion.png"  )));
//		ImageIO.write(hm, "png", new FileOutputStream(new File("/Users/christophergray/Desktop/ls/dat/heightmap.png")));
//		
//		System.out.println(maxCount);
//		System.out.println((dim*dim)+" "+l+" "+h);
//		System.exit(0);
	}
	
	private void chooseDirections()
	{
		int   j;
		int[] a;
		
		for (int i=DIRECTIONS.length-1;i>0;i--)
		{
			j=dirRand.nextInt(i + 1);
			a=DIRECTIONS[j];
			DIRECTIONS[j] = DIRECTIONS[i];
			DIRECTIONS[i] = a;
		}
	}

	private boolean advance(int d)
    {
	    nx=FastMath.wrap(x+DIRECTIONS[d][0],dim);
	    ny=FastMath.wrap(y+DIRECTIONS[d][1],dim);
		
		nv=valueAt(nx,ny);
		
		if(nv < valueAt(x,y))
		{
			erode();
			
			return(true);
		}
		
		return(false);
    }

	private void erode()
    {
		erodePoint(x,y);
		
		for(int i=0;i<DIRECTIONS.length;i++)
		{
			erodePoint(
				FastMath.wrap(x+DIRECTIONS[i][0],dim),
				FastMath.wrap(y+DIRECTIONS[i][1],dim)
			);
		}
		
		x=nx;
		y=ny;
    }
	
	private float valueAt(int x, int y)
    {
	    return(heightMap[x+(y*dim)]);
    }
	
	private void erodePoint(int x, int y)
    {
	    heightMap[x+(y*dim)]=nv;
	    freqCount[x+(y*dim)]++;
	    
	    if(freqCount[x+(y*dim)] > maxCount)
	    {
	    	maxCount=freqCount[x+(y*dim)];
	    }
    }
}
