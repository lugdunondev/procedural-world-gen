package net.lugdunon.server.worldgen.procedural.client.command.rest;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import net.lugdunon.client.Client;
import net.lugdunon.client.command.CommandBridge;
import net.lugdunon.client.command.Request;
import net.lugdunon.client.command.rest.IClientCommandRESTHandler;
import net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner;

import org.json.JSONObject;

public class PreviewImageProgressClientCommandRESTHandler implements IClientCommandRESTHandler
{
	@Override
    public String getContext()
    {
	    return("net/lugdunon/server/worldgen/procedural/preview/image/progress/");
    }

	@Override
    public byte[] processRequest(Request request, OutputStream response)
    {
    	JSONObject o=new JSONObject();
    	
	    try
	    {
	    	CommandBridge.initRes(request,response,HttpServletResponse.SC_OK);
	    	
	    	PreviewImageGeneratorRunner r=(PreviewImageGeneratorRunner) Client.getRunner("net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner");
	    	
	    	if(r != null && !r.processHasExited())
	    	{
	    		o.put("logEntry", r.getLogEntries());
	    	}
	    }
	    catch(Throwable t)
	    {
	    	t.printStackTrace();
	    }

	    return(o.toString().getBytes());
    }
}
