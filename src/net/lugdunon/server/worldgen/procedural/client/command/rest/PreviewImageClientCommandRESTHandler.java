package net.lugdunon.server.worldgen.procedural.client.command.rest;

import java.io.File;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import net.lugdunon.client.Client;
import net.lugdunon.client.command.CommandBridge;
import net.lugdunon.client.command.Request;
import net.lugdunon.client.command.rest.IClientCommandRESTHandler;
import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;
import net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGenerator;
import net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner;
import net.lugdunon.util.FileUtils;

import org.json.JSONObject;

public class PreviewImageClientCommandRESTHandler implements IClientCommandRESTHandler
{
	@Override
    public String getContext()
    {
	    return("net/lugdunon/server/worldgen/procedural/preview/image/");
    }

	@Override
    public byte[] processRequest(Request request, OutputStream response)
    {
    	JSONObject o=new JSONObject();
    	
	    try
	    {
	    	CommandBridge.initRes(request,response,HttpServletResponse.SC_OK);
	    	
	    	Client.startRunner(
    			"net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner",
    			new PreviewImageGeneratorRunner(
    				"net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner",
    				request.getPayloadProperty("config")==null?null:new JSONObject(request.getPayloadProperty("config"))
    			)
    		);
        	
	    	PreviewImageGeneratorRunner r=(PreviewImageGeneratorRunner) Client.getRunner("net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGeneratorRunner");
	    	
	    	while(!r.processHasExited())
	    	{
	    		try{Thread.sleep(100);}catch(Exception e){;}
	    	}
	    	
	    	if(r.getExitValue() == PreviewImageGenerator.EXIT_SUCCESS)
	    	{
		    	try
		    	{
			    	o.put("status", true                            );
			    	o.put("images", r.getImages ()                  );
			    	o.put("tileset",r.getTileset()                  );
			    	o.put("order",  ProceduralWorldGen.TERRAIN_ORDER);
		    	}
		    	catch(Exception e)
		    	{
		    		;
		    	}
	    	}
	    	else
	    	{
		    	try
		    	{
			    	o.put("status",false);
		    	}
		    	catch(Exception e)
		    	{
		    		;
		    	}
		    	
		    	if(r.getWorldId() != null)
		    	{
		    		File p=new File(FileUtils.getActualPath(""            ));
		    		File w=new File(FileUtils.getActualPath(r.getWorldId()));
		    		
		    		if(w.getParentFile().equals(p))
		    		{
		    			FileUtils.removeRecursive(w);
		    		}
		    	}
	    	}
	    }
	    catch(Throwable t)
	    {
	    	t.printStackTrace();
		    
	    	try
	    	{
		    	o.put("status",false);
	    	}
	    	catch(Exception e)
	    	{
	    		;
	    	}
	    }

	    return(o.toString().getBytes());
    }
}
