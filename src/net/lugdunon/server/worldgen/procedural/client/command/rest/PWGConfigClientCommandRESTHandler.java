package net.lugdunon.server.worldgen.procedural.client.command.rest;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import net.lugdunon.client.Client;
import net.lugdunon.client.command.CommandBridge;
import net.lugdunon.client.command.Request;
import net.lugdunon.client.command.rest.IClientCommandRESTHandler;
import net.lugdunon.server.mod.ServerMod;
import net.lugdunon.util.FileUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class PWGConfigClientCommandRESTHandler implements IClientCommandRESTHandler
{
	@Override
    public String getContext()
    {
	    return("net/lugdunon/server/worldgen/procedural/configuration/");
    }

	@Override
    public byte[] processRequest(Request request, OutputStream response)
    {
    	JSONObject o=new JSONObject();
    	
	    try
	    {
	    	JSONArray n=new JSONArray();
	    	JSONArray p=new JSONArray();
	    	
	    	CommandBridge.initRes(request,response,HttpServletResponse.SC_OK);
	    	
	    	//GET THE DEFAULT CONFIG
	    	o.put(
	    		"pwgConfig",
	    		FileUtils.getJSONObject(
	    			FileUtils.resourceAsStream(
		    			"net/lugdunon/server/worldgen/procedural/etc/instances/0/instance.json"
		    		)
		    	)
	    	);
	    	
	    	//GATHER UP ALL THE AVAILABLE NOISE GENS AND POST PROCESSORS
	    	for(ServerMod sMod:Client.SERVER_MOD_CACHE.listServerMods())
	    	{
	    		if(
	    			 sMod.getEtc().has   ("net.lugdunon.server.worldgen.procedural.noise") &&
	    			!sMod.getEtc().isNull("net.lugdunon.server.worldgen.procedural.noise")
	    		)
	    		{
	    			for(int i=0;i<sMod.getEtc().getJSONArray("net.lugdunon.server.worldgen.procedural.noise").length();i++)
	    			{
	    				n.put(sMod.getEtc().getJSONArray("net.lugdunon.server.worldgen.procedural.noise").get(i));
	    			}
	    		}
	    		
	    		if(
	    			 sMod.getEtc().has   ("net.lugdunon.server.worldgen.procedural.post") &&
	    			!sMod.getEtc().isNull("net.lugdunon.server.worldgen.procedural.post")
	    		)
	    		{
	    			for(int i=0;i<sMod.getEtc().getJSONArray("net.lugdunon.server.worldgen.procedural.post").length();i++)
	    			{
	    				p.put(sMod.getEtc().getJSONArray("net.lugdunon.server.worldgen.procedural.post").get(i));
	    			}
	    		}
	    	}

	    	o.put("noise",n);
	    	o.put("post", p);
	    }
	    catch(Throwable t)
	    {
	    	t.printStackTrace();
		    
	    	try
	    	{
		    	o.put("error",t.getMessage());
	    	}
	    	catch(Exception e)
	    	{
	    		;
	    	}
	    }

	    return(o.toString().getBytes());
    }
}