package net.lugdunon.server.worldgen.procedural.client.runner;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.lugdunon.client.Client;
import net.lugdunon.client.runner.Runner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PreviewImageGeneratorRunner extends Runner implements Runnable
{
	private JSONObject                    images;
	private JSONObject                    tileset;
	private JSONObject                    config;
	private String 						  worldId;
	private ConcurrentLinkedQueue<String> logEntries;
	
	public PreviewImageGeneratorRunner(String id, JSONObject config)
    {
		super(id);
		
		setServerModsToClasspath(true);

	    this.config=config;
	    images     =new JSONObject();
	    logEntries =new ConcurrentLinkedQueue<String>();
    }
	
	@Override
    protected String getXmx()
    {
		String xmx="512m";
		
		try
		{
			int ws=config.getJSONObject("world.cfg").getInt("net.lugdunon.server.worldgen.procedural.world.size");

			if(ws >= 16384)
			{
				xmx="4096m";
			}
			else if(ws >= 8192)
			{
				xmx="2048m";
			}
			else if(ws >= 4096)
			{
				xmx="1024m";
			}
		}
		catch(Exception e)
		{
			;
		}
		
	    return(xmx);
    }

	@Override
    protected void populateRunnerArguments(JSONObject args) throws JSONException
    {
		args.put("config",config  );
		args.put("appDir",(Client.getAppDir()+Client.LOCAL_INSTANCE_DAT_DIR).replace("\\","/"));
		args.put("logDir",(Client.getAppDir()                              ).replace("\\","/"));
    }

	@Override
    protected void handleProcessOutput(String line)
    {
		try
		{
			if(line.startsWith("INFO: :: "))
			{
				if(line.startsWith("INFO: :: TERRAIN_IMAGE"))
				{
					String[] segments=line.split("\\|", 3);
					
					images.put(segments[1],new JSONObject(segments[2]));
				}
				else if(line.startsWith("INFO: :: TILESET"))
				{
					tileset=new JSONObject(line.substring(16));
				}
				else if(line.startsWith("INFO: :: WORLD_ID"))
				{
					worldId=line.substring(17);
				}
			}
			else if(line.startsWith("INFO: "))
			{
				logEntries.add(line.substring(6));
			}
		}
		catch(JSONException e)
		{
			;
		}
    }

	@Override
    protected void handleProcessOutput(String type, String line)
    {
	    // TODO Auto-generated method stub
    }

	@Override
    protected String getRunnerClass()
    {
	    return("net.lugdunon.server.worldgen.procedural.client.runner.PreviewImageGenerator");
    }
	
	public JSONObject getImages()
	{
		return(images);
	}

	public JSONObject getTileset()
    {
	    return(tileset);
    }
	
	public String getWorldId()
	{
		return(worldId);
	}
	
	public JSONArray getLogEntries()
	{
		JSONArray a=new JSONArray();
		
		for(String logEntry:logEntries)
		{
			a.put(logEntry);
		}
		
		logEntries.clear();
		
		return(a);
	}
}