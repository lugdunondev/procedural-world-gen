package net.lugdunon.server.worldgen.procedural.client.runner;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import net.lugdunon.server.mod.ServerModCache;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;
import net.lugdunon.util.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreviewImageGenerator
{
	public static final int EXIT_SUCCESS             =0;
	public static final int EXIT_FAIL_INVALID_CONFIG =1;
	public static final int EXIT_FAIL_WORLD_GEN_ERROR=2;

	private static JSONObject     WORLD_GEN_CONFIG;
	private static String         APP_DIR;
	private static Logger         LOGGER;

	public static void main(String[] args)
	{
		parseArgs(args);
		
		if(APP_DIR != null)
		{
			FileUtils.setBaseFilePath(APP_DIR);
		}
		
		LOGGER=LoggerFactory.getLogger(PreviewImageGenerator.class);
		
		try
        {
			//LOAD SERVER MODS
			WorldGenerator.setServerModCache(new ServerModCache());
			WorldGenerator.getServerModCache(                    ).init();
			
	    	ProceduralWorldGen w=new ProceduralWorldGen();

	    	w.setConfiguration   (WORLD_GEN_CONFIG);
	    	w.setWorldId         (String.valueOf(System.currentTimeMillis()));
	    	
	    	try
	    	{
				LOGGER.info(":: WORLD_ID"+w.getWorldId());
				
		    	w.generateTerrainData(true);
	
				LOGGER.info(":: TILESET"+w.getTileset().getTilesetInfo().toString());
		    	
		    	logImage(ProceduralWorldGen.TERRAIN_IMAGE_RAW,      w);
		    	logImage(ProceduralWorldGen.TERRAIN_IMAGE_ELEVATION,w);
		    	logImage(ProceduralWorldGen.TERRAIN_IMAGE_BIOMES,   w);
		    	logImage(ProceduralWorldGen.TERRAIN_IMAGE_TERRAIN,  w);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}

			FileUtils.removeRecursive(new File(FileUtils.getActualPath(w.getWorldId())));
        }
        catch (Exception e)
        {
            e.printStackTrace();
    		System.exit(EXIT_FAIL_WORLD_GEN_ERROR);
        }
		
		System.exit(EXIT_SUCCESS);
	}
	
	public static void logImage(String imageId, ProceduralWorldGen w) throws IOException
	{
		JSONObject image=new JSONObject();
		
		try
		{
			image.put("image",w.getImageAsDataURL(imageId));
			image.put("name", w.getImageName     (imageId));
		}
		catch(JSONException e)
		{
			;
		}
		
		LOGGER.info(":: TERRAIN_IMAGE|"+imageId+"|"+image.toString());
	}

	private static void parseArgs(String[] args)
    {
		boolean parsed=false;
		
	    if(args.length == 1)
		{
	    	JSONObject config=null;
	    	
			try
			{
				config=new JSONObject(new String(DatatypeConverter.parseBase64Binary(args[0])));
			}
			catch(Exception e)
			{
				config=FileUtils.getJSONObject(args[0], true);
				e.printStackTrace();
			}
			
			if(config != null)
			{
				try
				{
					APP_DIR         =config.has("appDir")?config.getString("appDir"):null;
					WORLD_GEN_CONFIG=config.getJSONObject("config"   );
					
					if(APP_DIR != null && WORLD_GEN_CONFIG != null)
					{
						parsed=true;
					}
				}
				catch(Exception e)
				{
					config=FileUtils.getJSONObject(args[0], true);
					e.printStackTrace();
				}
			}
		}
	    
	    if(parsed == false)
	    {
			System.exit(EXIT_FAIL_INVALID_CONFIG);
	    }
    }
}
