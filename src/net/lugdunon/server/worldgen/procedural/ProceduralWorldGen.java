package net.lugdunon.server.worldgen.procedural;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;

import net.lugdunon.math.Point;
import net.lugdunon.server.mod.ServerMod;
import net.lugdunon.server.mod.ServerModCache;
import net.lugdunon.server.worldgen.BaseWorldGen;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.ex.WorldConfigurationException;
import net.lugdunon.server.worldgen.ex.WorldGenerationException;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.server.worldgen.procedural.instance.spawner.spawn.ISpawnPointGenerator;
import net.lugdunon.server.worldgen.procedural.noise.INoiseGen;
import net.lugdunon.server.worldgen.procedural.post.IPostProcessor;
import net.lugdunon.state.Manifest;
import net.lugdunon.state.State;
import net.lugdunon.state.SubsystemBase;
import net.lugdunon.state.World;
import net.lugdunon.state.character.NpcDefinitions;
import net.lugdunon.state.character.advancement.Advancements;
import net.lugdunon.state.character.procedural.ProceduralNpcDefinitions;
import net.lugdunon.state.character.stats.ICharacterStatsSystem;
import net.lugdunon.state.crafting.CraftingDisciplines;
import net.lugdunon.state.deity.Deities;
import net.lugdunon.state.dialogue.DialogueDefinitions;
import net.lugdunon.state.item.ItemDefinitions;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.mod.Mod;
import net.lugdunon.state.mod.ModCache;
import net.lugdunon.state.name.INameProvider;
import net.lugdunon.state.quest.QuestDefinitions;
import net.lugdunon.state.recipe.Recipes;
import net.lugdunon.state.settlement.Settlements;
import net.lugdunon.state.spell.SpellSchools;
import net.lugdunon.state.vendor.VendorCache;
import net.lugdunon.state.vendor.VendorDefinitions;
import net.lugdunon.util.FastMath;
import net.lugdunon.util.FileUtils;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.environment.IEnvironment;
import net.lugdunon.world.instance.Instance;
import net.lugdunon.world.politics.Politics;
import net.lugdunon.world.politics.map.generator.IPoliticalMapGenerator;
import net.lugdunon.world.terrain.Terrain;
import net.lugdunon.world.terrain.Tileset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProceduralWorldGen extends BaseWorldGen
{
	private static final Logger                    LOGGER                 =LoggerFactory.getLogger(ProceduralWorldGen.class);
	
	public  static final long                      EXIT_PORTAL_ID         =0;
	public  static final String                    TERRAIN_IMAGE_RAW      ="rawData";
	public  static final String                    TERRAIN_IMAGE_BIOMES   ="biomes";
	public  static final String                    TERRAIN_IMAGE_ELEVATION="elevation";
	public  static final String                    TERRAIN_IMAGE_TERRAIN  ="terrain";
	public  static final String[]                  TERRAIN_ORDER          =new String[]{TERRAIN_IMAGE_RAW,TERRAIN_IMAGE_ELEVATION,TERRAIN_IMAGE_BIOMES,TERRAIN_IMAGE_TERRAIN};
	private static       Map<String,BufferedImage> TERRAIN_IMAGES         =new HashMap<String,BufferedImage>();
	private static       Map<String,String       > TERRAIN_NAMES          =new HashMap<String,String       >();
	
	{
		TERRAIN_NAMES.put(TERRAIN_IMAGE_RAW,      "Raw Data");
		TERRAIN_NAMES.put(TERRAIN_IMAGE_BIOMES,   "Biomes");
		TERRAIN_NAMES.put(TERRAIN_IMAGE_ELEVATION,"Elevation");
		TERRAIN_NAMES.put(TERRAIN_IMAGE_TERRAIN,  "Terrain Layers");
	}

	private ItemDefinitions          itemDefinitions;
	private Recipes                  recipes;
	private Advancements             advancements;
	private DialogueDefinitions      dialogueDefinitions;
	private QuestDefinitions         questDefinitions;
	private Manifest                 manifest;
	private ICharacterStatsSystem    characterStats;
	private IEnvironment             environment;
	private String                   currency;
//	private SpriteDefinitions        spriteDefinitions;
	private NpcDefinitions           npcDefinitions;
	private ProceduralNpcDefinitions proceduralNpcDefinitions;
	private VendorDefinitions        vendorDefinitions;
	private VendorCache			     vendorCache;
	private SpellSchools             spellSchools;
	private Deities                  deities;
	private CraftingDisciplines      craftingDisciplines;
	private INameProvider            nameProvider;
	private String                   preset;
	private Tileset                  tileset;
	private Random                   rand;
	private JSONObject               proceduralConfig;
	private JSONObject               instanceConfig;
	private float[]                  rawTerrain;
	
	private float[]                  processedTerrain;
	
	private int                      worldSize;
	
	private Terrain                  terrain;
	private PlaceableItemCache       placeableItemCache;
	
	private long                     instanceId;
	
	private boolean                  makeRoads;
	private boolean                  makeProvincialRoads;
	
	public ProceduralWorldGen()
	{
		;
	}
	
	////
	
	@Override
    public void generateWorld() throws WorldGenerationException, WorldIdNotSetException
    {
		try
		{
			//GENERATE TERRAIN DATA
			generateTerrainData(false);
			
			//CREATE ITEMS AND NPCS
			setPlaceableItemCache(
				initBiomes(
				    instanceConfig,
					terrain,
				    getItemDefinitions(),
					getTileset(),
					getPreset ()
				)
			);
			
			//SET SPAWN POINT
			getSpawnPointGenerator(instanceConfig).setSpawnPoint(
			    instanceConfig,
				getRandom(),
				terrain,
				placeableItemCache
			);
			
			//POLITICS
			{
				WorldGenerator.setIndependentPreset (preset            );
				WorldGenerator.setPlaceableItemCache(placeableItemCache);
				WorldGenerator.setTerrain           (terrain           );
				WorldGenerator.setTileset           (tileset           );
				
				Politics politics=generatePolitics();
				
				if(politics != null)
				{
					FileUtils.writeJSON(
						getEtcPath()+"politics.json",
						politics.toJSONObject(),
						true
					);
				}
				
				WorldGenerator.setIndependentPreset (null              );
				WorldGenerator.setPlaceableItemCache(null              );
				WorldGenerator.setTerrain           (null              );
				WorldGenerator.setTileset           (null              );
			}
			
			//SAVE TERRAIN, PLACEABLE ITEM CACHE, AND INSTANCE
			{
				terrain.saveTerrainData(new File(FileUtils.getActualPath(getInstancePath(instanceId,"terrain.dat"))));
				
				FileUtils.writeJSON(
					getInstancePath(instanceId,"placeableItemCache.json"),
					placeableItemCache.toJSONObject(),
					true
				);
				
				FileUtils.writeJSON(
					getInstancePath(instanceId,"instance.json"),
					instanceConfig,
					true
				);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		WorldGenerator.reset();
    }

	protected Politics generatePolitics() throws JSONException, WorldIdNotSetException
    {
		//IF POLITICAL GENERATOR DEFINED, THEN SET UP POLITICS
		for(ServerMod sm:WorldGenerator.getServerModCache().listServerMods())
		{
			//GET FIRST
			if(sm.getEtc().has("net.lugdunon.server.worldgen.political.map.type"))
			{
				return(
					initPoliticsWithMap(
						sm.getEtc().getJSONObject(
							"net.lugdunon.server.worldgen.political.map.type"
						)
					)
				);
			}
		}
		
		return(null);
    }

	protected Politics initPoliticsWithMap(JSONObject politicalMapGenerator) throws JSONException, WorldIdNotSetException
    {
		try
		{
			Politics               politics   =null;
			IPoliticalMapGenerator pmg        =(IPoliticalMapGenerator) State.makeObjectFromJSONDefinition(politicalMapGenerator);
			Settlements            settlements=new Settlements(
				FileUtils.getJSONArray(
					getEtcPath()+"settlements.json"
				)
		    );

			pmg.generate(
				settlements,
				terrain.getSize()
			);
			
			LOGGER.info("Establishing political systems.");
			
			politics=new Politics(
				pmg
			);
			
		    makeRoads          =getWorldConfigProperty("worldgen.procedural.make.roads",           true);
		    makeProvincialRoads=getWorldConfigProperty("worldgen.procedural.make.provincial.roads",true);
			
			//RELOAD
			if(makeRoads || makeProvincialRoads)
			{
				settlements=new Settlements(
					FileUtils.getJSONArray(
						getEtcPath()+"settlements.json"
					)
			    );
	
				terrain.recomputeImpassabilityFlags();
				
				long i=System.nanoTime();	
	
				if(makeProvincialRoads)
				{
					long p=System.nanoTime();
					LOGGER.info("Generating provincial roads.");
					
					pmg.makeProvincialRoads(
						politics,
						placeableItemCache,
						terrain
					);
		
					LOGGER.info("Provincial roads took {} seconds.",((System.nanoTime()-p)/(1000000000.00)));
				}
				
				if(makeRoads)
				{
					long t=System.nanoTime();			
					LOGGER.info("Generating roads between towns.");
					
					pmg.makeInterTownRoads(
						politics,
						placeableItemCache,
						terrain
					);
					
					LOGGER.info("Inter-town roads took {} seconds.",((System.nanoTime()-t)/(1000000000.00)));
				}
				
				LOGGER.info("Total road creation took {} seconds.",((System.nanoTime()-i)/(1000000000.00)));
				
				foo(terrain);
			}

			return(politics);
		}
        catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
        {
			 LOGGER.error("Political map generator '{}' could not be loaded.",politicalMapGenerator,e);
        }
		
		return(null);
    }
	
	private  static void foo(Terrain terrain)
	{
		try
		{
			BufferedImage bi =new BufferedImage(terrain.getSize(), terrain.getSize(), BufferedImage.TYPE_INT_RGB);
			int           min=Byte.MAX_VALUE;
			int           max=Byte.MIN_VALUE;
			int           rng=0;
			int           cur=0;
			int[]         grd=null;
			
			for(int i=0;i<terrain.getSize();i++)
			{
				for(int j=0;j<terrain.getSize();j++)
				{
					cur=terrain.getElevationAt(i,j);
					
					if(min > cur){min=cur;}
					if(max < cur){max=cur;}
				}
			}
			
			rng=max-min;
			grd=new int[rng+1];
			
			for(int i=0;i<grd.length;i++)
			{
				cur   =(int) Math.floor(i*((1.00/grd.length)*255));
				grd[i]=(cur<<16)|(cur<< 8)|cur;
			}
			
			for(int i=0;i<terrain.getSize();i++)
			{
				for(int j=0;j<terrain.getSize();j++)
				{
					cur=terrain.getElevationAt(i,j);
					cur-=min;

					bi.setRGB(i,j,grd[cur]);
				}
			}
			
	        ImageIO.write(bi,"png",new FileOutputStream(FileUtils.getActualPath("elevation.png")));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try
		{
			JSONArray     m =WorldGenerator.getTileset().getLayersPreset(WorldGenerator.getLayersPreset(Instance.WORLD_INSTANCE_ID)).getJSONArray("layers");
			BufferedImage bi=new BufferedImage(terrain.getSize(), terrain.getSize(), BufferedImage.TYPE_INT_RGB);
			int[]         c =new int[m.length()];
			
			for(int i=0;i<c.length;i++)
			{
				c[i] =Integer.parseInt(m.getJSONObject(i).getString("color"),16);
			}

			for(int i=0;i<terrain.getSize();i++)
			{
				for(int j=0;j<terrain.getSize();j++)
				{
					bi.setRGB(i,j,c[terrain.getTopmostLayerAt(i,j)]);
				}
			}
			
	        ImageIO.write(bi,"png",new FileOutputStream(FileUtils.getActualPath("terrain.png")));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
//		System.exit(1);
	}

	public static ISpawnPointGenerator getSpawnPointGenerator(JSONObject instanceConfig)
    {
		ISpawnPointGenerator spg=null;
		
		try
		{
			spg=(ISpawnPointGenerator) ProceduralWorldGen.class.getClassLoader().loadClass(instanceConfig.getJSONObject("proceduralConfiguration").getJSONObject("spawnPoint").getString("class")).newInstance();
			spg.init(instanceConfig.getJSONObject("proceduralConfiguration").getJSONObject("spawnPoint").getJSONObject("args"));
		}
		catch(Exception e)
		{
			 LOGGER.error("Spawn point generator could not be loaded.",e);
		}
		
	    return(spg);
    }

	@Override
    public void configureWorld() throws WorldConfigurationException, WorldIdNotSetException
    {
	    super.configureWorld();
    }
	
	////
	
	public void setNameProvider(INameProvider nameProvider)
	{
		this.nameProvider=nameProvider;
	}
	
	public INameProvider getNameProvider()
	{
		return(nameProvider);
	}
	
	public void setInstanceId(long instanceId)
	{
		this.instanceId=instanceId;
	}
	
	public long getInstanceId()
	{
		return(instanceId);
	}
	
	public PlaceableItemCache getPlaceableItemCache()
	{
		return(placeableItemCache);
	}
	
	private void setPlaceableItemCache(PlaceableItemCache placeableItemCache)
	{
		this.placeableItemCache=placeableItemCache;
	}
	
	public ItemDefinitions getItemDefinitions()
	{
		return(itemDefinitions);
	}
	
	private void setItemDefinitions(ItemDefinitions itemDefinitions)
	{
		this.itemDefinitions=itemDefinitions;
	}
	
	public Recipes getRecipes()
	{
		return(recipes);
	}
	
	private void setRecipes(Recipes recipes)
	{
		this.recipes=recipes;
	}
	
	public Advancements getAdvancements()
	{
		return(advancements);
	}
	
	private void setAdvancements(Advancements advancements)
	{
		this.advancements=advancements;
	}
	
	public DialogueDefinitions getDialogueDefinitions()
	{
		return(dialogueDefinitions);
	}
	
	private void setDialogueDefinitions(DialogueDefinitions dialogueDefinitions)
	{
		this.dialogueDefinitions=dialogueDefinitions;
	}
	
	public QuestDefinitions getQuestDefinitions()
	{
		return(questDefinitions);
	}
	
	private void setQuestDefinitions(QuestDefinitions questDefinitions)
	{
		this.questDefinitions=questDefinitions;
	}
	
	public Manifest getManifest()
	{
		return(manifest);
	}
	
	private void setManifest(Manifest manifest)
	{
		this.manifest=manifest;
	}
	
//	public SpriteDefinitions getSpriteDefinitions()
//	{
//		return(spriteDefinitions);
//	}
//	
//	private void setSpriteDefinitions(SpriteDefinitions spriteDefinitions)
//	{
//		this.spriteDefinitions=spriteDefinitions;
//	}
	
	public ICharacterStatsSystem getCharacterStatsSystem()
	{
		return(characterStats);
	}
	
	private void setCharacterStatsSystem(ICharacterStatsSystem characterStats)
	{
		this.characterStats=characterStats;
	}
	
	public IEnvironment getEnvironment()
	{
		return(environment);
	}
	
	private void setEnvironment(IEnvironment environment)
	{
		this.environment=environment;
	}
	
	public String getCurrency()
	{
		return(currency);
	}
	
	private void setCurrency(String currency)
	{
		this.currency=currency;
	}
	
	public NpcDefinitions getNpcDefinitions()
	{
		return(npcDefinitions);
	}
	
	private void setNpcDefinitions(NpcDefinitions npcDefinitions)
	{
		this.npcDefinitions=npcDefinitions;
	}
	
	public ProceduralNpcDefinitions getProceduralNpcDefinitions()
	{
		return(proceduralNpcDefinitions);
	}
	
	private void setProceduralNpcDefinitions(ProceduralNpcDefinitions proceduralNpcDefinitions)
	{
		this.proceduralNpcDefinitions=proceduralNpcDefinitions;
	}
	
	public VendorDefinitions getVendorDefinitions()
	{
		return(vendorDefinitions);
	}
	
	private void setVendorDefinitions(VendorDefinitions vendorDefinitions)
	{
		this.vendorDefinitions=vendorDefinitions;
	}
	
	public VendorCache getVendorCache()
	{
		return(vendorCache);
	}
	
	private void setVendorCache(VendorCache vendorCache)
	{
		this.vendorCache=vendorCache;
	}
	
	public SpellSchools getSpellSchools()
	{
		return(spellSchools);
	}
	
	private void setSpellSchools(SpellSchools spellSchools)
	{
		this.spellSchools=spellSchools;
	}
	
	public Deities getDeities()
	{
		return(deities);
	}
	
	private void setDeities(Deities deities)
	{
		this.deities=deities;
	}
	
	public CraftingDisciplines getCraftingDisciplines()
	{
		return(craftingDisciplines);
	}
	
	private void setCraftingDisciplines(CraftingDisciplines craftingDisciplines)
	{
		this.craftingDisciplines=craftingDisciplines;
	}
	
	public String getPreset()
	{
		return(preset);
	}
	
	private void setPreset(String preset)
	{
		this.preset=preset;
	}
	
	public Tileset getTileset()
	{
		return(tileset);
	}
	
	private void setTileset(Tileset tileset)
	{
		this.tileset=tileset;
	}
	
	public Random getRandom()
	{
		return(rand);
	}
	
	private void setRandom(Random rand)
	{
		this.rand=rand;
	}
	
	public int getWorldSize()
	{
		return(worldSize);
	}
	
	private void setWorldSize(int worldSize)
	{
		this.worldSize=worldSize;
	}
	
	public static void main(String[] args) throws Exception
	{		
		Instance.getInstancePath();
		
		new ProceduralWorldGen().generateWorld();
	}
	
	public Object getSeed()
	{
		try
		{
			if(getWorldConfig() != null && getWorldConfig().containsKey("seed"))
			{
				return(getWorldConfig().get("seed"));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return("LUGDUNON");
	}
	
	////

	public void generateTerrainData(boolean genImages) throws Exception
    {
		generateTerrainData(
			Instance.WORLD_INSTANCE_ID,
			genImages
		);
    }

	public void generateTerrainData(long instanceId,boolean genImages) throws Exception
    {
		WorldGenerator.setNoState(true);
		
		//SET INSTANCE ID AND PROCEDURAL WORLD GEN CONFIG
		setInstanceId(instanceId);

		//STUB /etc FROM WORLD GEN
		setDefaultConfiguration();
		
		ServerModCache sModCache=new ServerModCache();
		sModCache.init();
		
		//LOAD FROM /etc
		String   tileset       =null;
		String   characterStats=null;
		String   environment   =null;
		String   currency      =null;
		String   manifest      =getEtcPath()+"manifest.json";
		String   items         =getEtcPath()+"items.json";
		String   recipes       =getEtcPath()+"recipes.json";
		String   advancements  =getEtcPath()+"advancements.json";
		String   dialogue      =getEtcPath()+"dialogue.json";
		String   npcs          =getEtcPath()+"npcs.json";
		String   npcPresets    =getEtcPath()+"proceduralNpcs.json";
		String   quests        =getEtcPath()+"quests.json";
		String   vendors       =getEtcPath()+"vendors.json";
		String   vendorCache   =getEtcPath()+"vendorCache.json";
		String   spellSchools  =getEtcPath()+"spellSchools.json";
		String   deities       =getEtcPath()+"deities.json";
		String   crafts        =getEtcPath()+"craftingDisciplines.json";
		String   mods          =getEtcPath()+"mods.json";
		ModCache modCache      =new ModCache(
			FileUtils.getJSONArray(
				mods
			)
		);
		
		for(Mod m:modCache.listMods())
		{
			for(String subsystemId:m.listSubsystems())
			{
				if("TILESET".equals(subsystemId))
				{
					tileset=m.getSubsystemImpl(subsystemId).replace(".","/")+".json";
				}
				else if("CHARACTER.STATS".equals(subsystemId))
				{
					characterStats=m.getSubsystemImpl(subsystemId);
				}
				else if("ENVIRONMENT".equals(subsystemId))
				{
					environment=m.getSubsystemImpl(subsystemId);
				}
				else if("CURRENCY".equals(subsystemId))
				{
					currency=m.getSubsystemImpl(subsystemId);
				}
			}
		}
		
		if(tileset == null)
		{
			LOGGER.error("Tileset {} could not be located.",tileset);
		}
		
		if(characterStats == null)
		{
			LOGGER.error("Character Stats {} could not be located.",characterStats);
		}
		
		if(environment == null)
		{
			LOGGER.error("Environment {} could not be located.",environment);
		}
		
		setRandom         (FastMath.randomFromSeed(getSeed()));
		setWorldSize      (Integer.parseInt(getWorldConfig().get("net.lugdunon.server.worldgen.procedural.world.size")));
		setPreset         ("overWorld"                       );

		setCurrency       (currency                          );
		
		WorldGenerator.setActiveWorldGen (this                );
		
		WorldGenerator.setRandom         (getRandom         ());
		WorldGenerator.setEtcPath        (getEtcPath        ());
		WorldGenerator.setModCache       (modCache            );
		WorldGenerator.setServerModCache (sModCache           );
		
		WorldGenerator.setCurrency       (getCurrency       ());

		setItemDefinitions(
			new ItemDefinitions(
				FileUtils.getJSONArray(
					items
				)
			)
		);
		setRecipes(
			new Recipes(
				FileUtils.getJSONArray(
					recipes
				)
			)
		);
		setAdvancements(
			new Advancements(
				FileUtils.getJSONArray(
					advancements
				)
			)
		);
		setDialogueDefinitions(
			new DialogueDefinitions(
				FileUtils.getJSONArray(
					dialogue
				)
			)
		);
		setQuestDefinitions(
			new QuestDefinitions(
				FileUtils.getJSONObject(
					quests
				)
			)
		);
		setManifest(
			new Manifest(
				FileUtils.getJSONObject(
					manifest
				)
			)
		);
//		setSpriteDefinitions(
//			new SpriteDefinitions(
//				getManifest()
//			)
//		);
		setNpcDefinitions(
			new NpcDefinitions(
				FileUtils.getJSONArray(
					npcs
				)
			)
		);
		setProceduralNpcDefinitions(
			new ProceduralNpcDefinitions(
				FileUtils.getJSONObject(
					npcPresets
				)
			)
		);
		setVendorDefinitions(
			new VendorDefinitions(
				FileUtils.getJSONArray(
					vendors
				)
			)
		);
		setSpellSchools(
			new SpellSchools(
				FileUtils.getJSONArray(
					spellSchools
				)
			)
		);
		setDeities(
			new Deities(
				FileUtils.getJSONArray(
					deities
				)
			)
		);
		setCraftingDisciplines(
			new CraftingDisciplines(
				FileUtils.getJSONArray(
					crafts
				)
			)
		);
		
		try
		{
			try
			{
				setNameProvider(
					(net.lugdunon.state.name.INameProvider) World.class.getClassLoader().loadClass(
			    		getWorldConfig().get(
		    				"name.provider"
		    			)
		    		).newInstance()
				);
			}
			catch(Exception e)
			{
				setNameProvider(
					(net.lugdunon.state.name.INameProvider) World.class.getClassLoader().loadClass(
			    		"net.lugdunon.state.name.ListNameProvider"
		    		).newInstance()
				);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		setCharacterStatsSystem(
			(
				(ICharacterStatsSystem) ProceduralWorldGen.class.getClassLoader().loadClass(
					characterStats
				).newInstance()
			)
		);
		
		setEnvironment(
			(
				(IEnvironment) ProceduralWorldGen.class.getClassLoader().loadClass(
					environment
				).newInstance()
			)
		);

		WorldGenerator.setCharacterStatsSystem    (getCharacterStatsSystem    ());
		WorldGenerator.setEnvironment             (getEnvironment             ());
		WorldGenerator.setItemDefinitions         (getItemDefinitions         ());
		WorldGenerator.setRecipes                 (getRecipes                 ());
		WorldGenerator.setAdvancements            (getAdvancements            ());
		WorldGenerator.setDialogueDefinitions     (getDialogueDefinitions     ());
		WorldGenerator.setQuestDefinitions        (getQuestDefinitions        ());
		WorldGenerator.setManifest                (getManifest                ());
//		WorldGenerator.setSpriteDefinitions       (getSpriteDefinitions       ());
		WorldGenerator.setNpcDefinitions          (getNpcDefinitions          ());
		WorldGenerator.setProceduralNpcDefinitions(getProceduralNpcDefinitions());
		WorldGenerator.setVendorDefinitions       (getVendorDefinitions       ());

		setVendorCache(
			new VendorCache(
				FileUtils.getJSONObject(
					vendorCache
				)
			)
		);
		
		WorldGenerator.setVendorCache             (getVendorCache             ());
		WorldGenerator.setSpellSchools            (getSpellSchools            ());
		WorldGenerator.setDeities                 (getDeities                 ());
		WorldGenerator.setCraftingDisciplines     (getCraftingDisciplines     ());
		WorldGenerator.setNameProvider            (getNameProvider            ());
		
		setTileset(
			new Tileset(
				FileUtils.getJSONObject(
					FileUtils.resourceAsStream(tileset)
				),
				(short) 8
			)
		);
		//GET INSTANCE AND PROCEDURAL CONFIGS
		instanceConfig  =FileUtils.getJSONObject(getInstancePath(instanceId,"instance.json"));
		proceduralConfig=instanceConfig.getJSONObject("proceduralConfiguration");

		//SET STARTING YEAR
		if(proceduralConfig.has("startingYearBounds") && !proceduralConfig.isNull("startingYearBounds"))
		{
			assignRandomStartingYear(proceduralConfig.getJSONObject("startingYearBounds"));
		}
		
		rawTerrain=generateRawTerrainData();
		
		//POSTERIZE AND SMOOTH TERRAIN
		processedTerrain=posterizeAndSmooth(
			rawTerrain,
			proceduralConfig.getInt("elevationSteps"),
			proceduralConfig.getInt("smoothing"     ),
			getWorldSize()
		);

		//CREATE TERRAIN
		terrain=createTerrain(
			instanceId,
			instanceConfig,
			processedTerrain,
			getTileset(),
			getPreset(),
			getRandom(),
			getSeed(),
			getWorldSize(),
			genImages
		);
    };
    
    private void assignRandomStartingYear(JSONObject yearBounds)
    {
    	try
    	{
	    	int low =yearBounds.getInt("low" );
	    	int high=yearBounds.getInt("high");

	    	getEnvironment().getCalendar().setYear(
    			getRandom().nextInt(
    				high-low
    			)+low
	    	);
	    	getEnvironment().getCalendar().setMonth(
    			getRandom().nextInt(
    				getEnvironment().getCalendar().getMaximumAllowableMonth()
    			)
	    	);
	    	getEnvironment().getCalendar().setDate(
    			getRandom().nextInt(
    				getEnvironment().getCalendar().getMaximumAllowableDate()
    			)
	    	);
	    	getEnvironment().getCalendar().setHours(
    			getRandom().nextInt(
    				getEnvironment().getCalendar().getMaximumAllowableHour()
    			)
	    	);
	    	getEnvironment().getCalendar().setMinutes(
    			getRandom().nextInt(
    				getEnvironment().getCalendar().getMaximumAllowableMinute()
    			)
	    	);
	    	getEnvironment().getCalendar().setSeconds(
    			getRandom().nextInt(
    				getEnvironment().getCalendar().getMaximumAllowableSecond()
    			)
	    	);
	    	
	    	((SubsystemBase) getEnvironment()).saveState();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }

	public static float[] posterizeAndSmooth(float[] terrain, int steps, int smoothing, int dimension)
    {
		float   stepInterval=1.0f/steps;
		float[] processedTerrain=new float[terrain.length];

		for(int i=0;i<terrain.length;i++)
		{
			processedTerrain[i]=posterize(terrain[i],stepInterval,steps);
		}

		for(int i=0;i<smoothing;i++)
		{
			smooth(processedTerrain,dimension);
		}
		
		return(processedTerrain);
    }
	
	public static PlaceableItemCache initBiomes(JSONObject instanceConfig, Terrain terrain, ItemDefinitions itemDefinitions, Tileset tileset, String preset)
    {
		WorldGenerator.setIndependentPreset(preset );
		WorldGenerator.setTerrain          (terrain);

		PlaceableItemCache placeableItemCache=new PlaceableItemCache(terrain);
	
		//ADD RETURN PORTAL FIRST
		try
		{
	    	if(instanceConfig.has("returnPortal") && !instanceConfig.isNull("returnPortal"))
	    	{
	    		placeableItemCache.newPlaceableItemReference(
	    			itemDefinitions.getItemDef(instanceConfig.getString("returnPortal")),
	    			new Point(-100,-100)
	    		);
	    		
	    		LOGGER.info("Return portal of type '{}' successfully placed.",instanceConfig.getString("returnPortal"));
	    	}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	    for(byte id:terrain.listBiomeIds())
	    {
	    	terrain.getBiome(id).init(terrain, placeableItemCache);
	    }
	    
	    terrain.getPlacementStrategy().placeItems(instanceConfig, terrain, placeableItemCache);

		WorldGenerator.setIndependentPreset(null);
	    WorldGenerator.setTerrain          (null);
	    
	    return(placeableItemCache);
    }

	public static Terrain createTerrain(
			long instanceId,
			JSONObject instanceConfig,
			float[] processedTerrain,
			Tileset tileset,
			String preset,
			Random random,
			Object seed,
			int dimension,
			boolean genImages
	) throws Exception
    {
		Terrain         terrain;
		Float[]         eLevels;
		byte            curElevation           =(byte) 0x00;
		JSONObject      proceduralConfiguration=instanceConfig.getJSONObject("proceduralConfiguration");
		JSONArray       elevationsToBiomes     =proceduralConfiguration.getJSONArray("elevationsToBiomes");
		Map<Float,Byte> elevationMapping       =new HashMap<Float,Byte>();
		float[][]       biomeDifferentiators   =new float[elevationsToBiomes.length()][];
		Set<Float>      eIndices               =new TreeSet<Float>();
		byte []         biomeData              =new byte [processedTerrain.length];
		byte []         elevationData          =new byte [processedTerrain.length];
		short[]         terrainData            =new short[processedTerrain.length];
		Map<Byte,Biome> biomes                 =Terrain.initBiomes(instanceConfig);
		
		//INITIALIZE BIOME ARRAY TO NULL BIOME
		Arrays.fill(biomeData, Biome.NULL_BIOME);
		
		//GET ELEVATION INDICES
		{
			for(int i=0;i<processedTerrain.length;i++)
			{
				eIndices.add(processedTerrain[i]);
			}
	
			eLevels=eIndices.toArray(new Float[eIndices.size()]);
		}
		
		// DECIDE FINAL ELEVATION LEVELS
		for(int i=0;i<elevationsToBiomes.length();i++)
		{
			JSONObject biomeRecord=elevationsToBiomes.getJSONObject(i);
			Biome      biome      =biomes.get((byte) biomeRecord.getJSONArray("biomes").getInt(0));
			
			//NO ELEVATION STEPS
			if(!biome.allowsElevationSteps() || biomeRecord.getJSONArray("biomes").length() > 1)
			{
				for(int j=0;j<biomeRecord.getJSONArray("elevations").length();j++)
				{
					elevationMapping.put(
						eLevels[biomeRecord.getJSONArray("elevations").getInt(j)],
						(biome.getFixedElevation() != Biome.NO_FIXED_ELEVATION)?
						biome.getFixedElevation():
						curElevation
					);
				}
			}
			else
			{
				for(int j=0;j<biomeRecord.getJSONArray("elevations").length();j++)
				{
					elevationMapping.put(eLevels[biomeRecord.getJSONArray("elevations").getInt(j)],curElevation++);
				}
			}
		}
		
		// GENERATE BIOME, ELEVATION, AND TERRAIN DATA FROM RAW PROCESSED DATA
		for(int i=0;i<processedTerrain.length;i++)
		{
			Biome biome;
			int   eIndex     =Arrays.binarySearch(eLevels,processedTerrain[i]);
			int   biomeRecord=getBiomeRecord(elevationsToBiomes,eIndex);
			int   biomeCount =elevationsToBiomes.getJSONObject(biomeRecord).getJSONArray("biomes").length();
			
			if(biomeCount > 1)
			{
				if(biomeDifferentiators[biomeRecord] == null)
				{
					biomeDifferentiators[biomeRecord]=generateBiomeDifferentiator(random,instanceId,instanceConfig,proceduralConfiguration,dimension,biomeCount);
				}

				biomeData[i]=(byte) elevationsToBiomes.getJSONObject(biomeRecord).getJSONArray("biomes").getInt(
					(int) (
						biomeDifferentiators[biomeRecord][i] /
						(
							1.0 / 
							biomeCount
						)
					)
				);
			}
			else
			{
				biomeData[i]=(byte) elevationsToBiomes.getJSONObject(biomeRecord).getJSONArray("biomes").getInt(0);
			}

			biome           =biomes.get(biomeData[i]);
			elevationData[i]=elevationMapping.get(processedTerrain[i]);
			terrainData  [i]=biome.getLayerComposite();
		}

		//FIX ELEVATION / TERRAIN RELATIONSHIP
		for(int x=0,i=0;x<dimension;x++)
		{
			for(int y=0;y<dimension;y++,i++)
			{
				int idx=Integer.MIN_VALUE;
				
				// > ELEVATION > TERRAIN LAYER
				if     ((elevationData[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y-1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y-1,dimension)+(FastMath.wrap(x-1,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x  ,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y-1,dimension)+(FastMath.wrap(x  ,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y-1,dimension)+(FastMath.wrap(x  ,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+1,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y  ,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y  ,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y  ,dimension)+(FastMath.wrap(x-1,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y  ,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y  ,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y  ,dimension)+(FastMath.wrap(x+1,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y+1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y+1,dimension)+(FastMath.wrap(x-1,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x  ,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y+1,dimension)+(FastMath.wrap(x  ,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y+1,dimension)+(FastMath.wrap(x  ,dimension)*dimension);
				}
				else if((elevationData[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > elevationData[i]) &&
						(terrainData  [FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)] > terrainData  [i]))
				{
					idx=FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension);
				}
				
				if(idx != Integer.MIN_VALUE)
				{
					terrainData[i]=biomes.get(biomeData[idx]).getLayerComposite();
				}
			}
		}
		
		//MAKE SURE ELEVATION CHANGES ARE SPACED PROPERLY
		for(int x=0;x<dimension;x++)
		{
			for(int y=0;y<dimension;y++)
			{
				elevationProximityCheck(elevationData,x,y,dimension);
			}
		}

		//POST PROCESS BIOMES
		for(Biome biome:biomes.values())
		{
			if(biome.isPostProcessed())
			{
				LOGGER.info("Running {} biome post processing.",biome.getName());
				
				biome.postProcess(random,dimension,processedTerrain,biomeData,elevationData,terrainData);
			}
		}

		//DEBUG WRITE IMAGES
		if(genImages)
		{
			writeImage(dimension,processedTerrain,TERRAIN_IMAGE_RAW                                            );
			writeImage(dimension,biomeData,       TERRAIN_IMAGE_BIOMES,   instanceConfig.getJSONArray("biomes"));
			writeImage(dimension,elevationData,   TERRAIN_IMAGE_ELEVATION,(float) curElevation                 );
			writeImage(dimension,terrainData,     TERRAIN_IMAGE_TERRAIN,  tileset.getLayers(preset)            );
		}

		//GENERATE TERRAIN
		WorldGenerator.setIndependentPreset(preset);
		terrain=new Terrain(
			instanceId,
			terrainData,
			elevationData,
			biomeData,
			tileset,
			instanceConfig,
			seed
		);
		WorldGenerator.setIndependentPreset(null);
		
		return(terrain);
    }

	private static void elevationProximityCorrection(byte[] elevationData, int i, int j)
    {
		if(FastMath.diff(elevationData[i],elevationData[j]) > 1)
		{
			if(elevationData[i] > elevationData[j])
			{
				elevationData[j]++;
			}
			else
			{
				elevationData[j]--;
			}
		}
    }

	private static void elevationProximityCheck(byte[] elevationData, int x, int y, int dimension)
    {
		for(int i=x-2;i<=x+2;i++)
		{
			for(int j=y-2;j<=y+2;j++)
			{
				if(i != x && j != y)
				{
					elevationProximityCorrection(
						elevationData,
						FastMath.wrap(j,dimension)+(FastMath.wrap(i,dimension)*dimension),
						FastMath.wrap(y,dimension)+(FastMath.wrap(x,dimension)*dimension)
					);
				}
			}
		}
    }

	private static float[] generateBiomeDifferentiator(Random random, long instanceId, JSONObject instanceConfig, JSONObject proceduralWorldGenConfig, int dimension, int biomeCount) throws JSONException, Exception
    {
		JSONObject           noiseDef    =proceduralWorldGenConfig.getJSONObject("biomeDifferentiatorNoise");
		INoiseGen            noiseGen    =(INoiseGen) ProceduralWorldGen.class.getClassLoader().loadClass(
				noiseDef.getString("class")
		).newInstance();
		float[]             rawData     =noiseGen.computeNoise(random,instanceId,instanceConfig,dimension,noiseDef.getJSONObject("args"));

		for(int i=0;i<rawData.length;i++)
		{
			rawData[i]=posterize(rawData[i],1.0f/biomeCount,biomeCount);
		}

		smooth(rawData,dimension);
		
		return(rawData);
    }

	private float[] generateRawTerrainData() throws JSONException, Exception
    {
	    int                  halfDim     =getWorldSize()/2;
		JSONObject           noiseDef    =proceduralConfig.getJSONObject("noise");
	    JSONArray            ppDefs      =proceduralConfig.getJSONArray ("postProcessing");
		INoiseGen            noiseGen    =(INoiseGen) ProceduralWorldGen.class.getClassLoader().loadClass(
				noiseDef.getString("class")
		).newInstance();
		float[]             rawTerrain  =noiseGen.computeNoise(getRandom(),getInstanceId(),instanceConfig,halfDim,noiseDef.getJSONObject("args"));
		
		for(int i=0;i<ppDefs.length();i++)
		{
			try
			{
				IPostProcessor pp=(IPostProcessor) ProceduralWorldGen.class.getClassLoader().loadClass(
					ppDefs.getJSONObject(i).getString("class")
				).newInstance();
			
				pp.process(rawTerrain, halfDim, getRandom(), ppDefs.getJSONObject(i).has("args")?ppDefs.getJSONObject(i).getJSONObject("args"):null);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		rawTerrain=upscale(
			rawTerrain,
			halfDim  
		);
		
		return(rawTerrain);
    }
	
	public static void smooth(float[] rawTerrain, int dimension)
    {
		float[] matrix=new float[16];

	    for(int y=0;y<dimension;y++)
	    {
		    for(int x=0;x<dimension;x++)
		    {
		    	matrix[ 0]=rawTerrain[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 1]=rawTerrain[FastMath.wrap(y-1,dimension)+(                           x*dimension)];
		    	matrix[ 2]=rawTerrain[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)];

		    	matrix[ 4]=rawTerrain[                           y+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 5]=rawTerrain[                           y+(                           x*dimension)];
		    	matrix[ 6]=rawTerrain[                           y+(FastMath.wrap(x+1,dimension)*dimension)];

		    	matrix[ 8]=rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 9]=rawTerrain[FastMath.wrap(y+1,dimension)+(                           x*dimension)];
		    	matrix[10]=rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)];
		    	
		    	//OUTER CORNERS
		    	if(
		    		matrix[5] == matrix[ 0] &&
		    		matrix[5] == matrix[ 1] &&
		    		matrix[5] == matrix[ 4] &&
		    		matrix[5] >  matrix[ 2] &&
		    		matrix[5] >  matrix[ 6] &&
		    		matrix[5] >  matrix[ 8] &&
		    		matrix[5] >  matrix[ 9] &&
		    		matrix[5] >  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[2];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 1] &&
		    		matrix[5] == matrix[ 2] &&
		    		matrix[5] == matrix[ 6] &&
		    		matrix[5] >  matrix[ 0] &&
		    		matrix[5] >  matrix[ 4] &&
		    		matrix[5] >  matrix[ 8] &&
		    		matrix[5] >  matrix[ 9] &&
		    		matrix[5] >  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 6] &&
		    		matrix[5] == matrix[ 9] &&
		    		matrix[5] == matrix[10] &&
		    		matrix[5] >  matrix[ 0] &&
		    		matrix[5] >  matrix[ 1] &&
		    		matrix[5] >  matrix[ 2] &&
		    		matrix[5] >  matrix[ 4] &&
		    		matrix[5] >  matrix[ 8]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 4] &&
		    		matrix[5] == matrix[ 8] &&
		    		matrix[5] == matrix[ 9] &&
		    		matrix[5] >  matrix[ 0] &&
		    		matrix[5] >  matrix[ 1] &&
		    		matrix[5] >  matrix[ 2] &&
		    		matrix[5] >  matrix[ 6] &&
		    		matrix[5] >  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    	
		    	//INNER CORNERS
		    	else if(
		    		matrix[5] == matrix[ 0] &&
		    		matrix[5] == matrix[ 1] &&
		    		matrix[5] == matrix[ 4] &&
		    		matrix[5] <  matrix[ 2] &&
		    		matrix[5] <  matrix[ 6] &&
		    		matrix[5] <  matrix[ 8] &&
		    		matrix[5] <  matrix[ 9] &&
		    		matrix[5] <  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[2];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 1] &&
		    		matrix[5] == matrix[ 2] &&
		    		matrix[5] == matrix[ 6] &&
		    		matrix[5] <  matrix[ 0] &&
		    		matrix[5] <  matrix[ 4] &&
		    		matrix[5] <  matrix[ 8] &&
		    		matrix[5] <  matrix[ 9] &&
		    		matrix[5] <  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 6] &&
		    		matrix[5] == matrix[ 9] &&
		    		matrix[5] == matrix[10] &&
		    		matrix[5] <  matrix[ 0] &&
		    		matrix[5] <  matrix[ 1] &&
		    		matrix[5] <  matrix[ 2] &&
		    		matrix[5] <  matrix[ 4] &&
		    		matrix[5] <  matrix[ 8]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 4] &&
		    		matrix[5] == matrix[ 8] &&
		    		matrix[5] == matrix[ 9] &&
		    		matrix[5] <  matrix[ 0] &&
		    		matrix[5] <  matrix[ 1] &&
		    		matrix[5] <  matrix[ 2] &&
		    		matrix[5] <  matrix[ 6] &&
		    		matrix[5] <  matrix[10]
		    	)
		    	{
		    		rawTerrain[y+(x*dimension)]=matrix[0];
		    	}
		    }
	    }
	    
	    for(int y=0;y<dimension;y++)
	    {
		    for(int x=0;x<dimension;x++)
		    {
		    	matrix[ 0]=rawTerrain[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 1]=rawTerrain[FastMath.wrap(y-1,dimension)+(                           x*dimension)];
		    	matrix[ 2]=rawTerrain[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)];
		    	matrix[ 3]=rawTerrain[FastMath.wrap(y-1,dimension)+(FastMath.wrap(x+2,dimension)*dimension)];

		    	matrix[ 4]=rawTerrain[                           y+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 5]=rawTerrain[                           y+(                           x*dimension)];
		    	matrix[ 6]=rawTerrain[                           y+(FastMath.wrap(x+1,dimension)*dimension)];
		    	matrix[ 7]=rawTerrain[                           y+(FastMath.wrap(x+2,dimension)*dimension)];

		    	matrix[ 8]=rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[ 9]=rawTerrain[FastMath.wrap(y+1,dimension)+(                           x*dimension)];
		    	matrix[10]=rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)];
		    	matrix[11]=rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+2,dimension)*dimension)];

		    	matrix[12]=rawTerrain[FastMath.wrap(y+2,dimension)+(FastMath.wrap(x-1,dimension)*dimension)];
		    	matrix[13]=rawTerrain[FastMath.wrap(y+2,dimension)+(                           x*dimension)];
		    	matrix[14]=rawTerrain[FastMath.wrap(y+2,dimension)+(FastMath.wrap(x+1,dimension)*dimension)];
		    	matrix[15]=rawTerrain[FastMath.wrap(y+2,dimension)+(FastMath.wrap(x+2,dimension)*dimension)];
		    	
		    	//TETRIS L BLOCKS
		    	if(
		    		matrix[9] == matrix[ 6] &&
		    		matrix[9] == matrix[10] &&
		    		matrix[9] != matrix[ 1] &&
		    		matrix[9] != matrix[ 2] &&
		    		matrix[9] != matrix[ 4] &&
		    		matrix[9] != matrix[ 5] &&
		    		matrix[9] != matrix[ 7] &&
		    		matrix[9] != matrix[ 8] &&
		    		matrix[9] != matrix[11] &&
		    		matrix[9] != matrix[13] &&
		    		matrix[9] != matrix[14]
		    	)
		    	{
		    		rawTerrain[                           y+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
		    		rawTerrain[FastMath.wrap(y+1,dimension)+(                           x*dimension)]=matrix[ 1];
			    	rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
		    	}
		    	else if(
		    		matrix[9] == matrix[ 5] &&
		    		matrix[9] == matrix[ 6] &&
		    		matrix[9] != matrix[ 1] &&
		    		matrix[9] != matrix[ 2] &&
		    		matrix[9] != matrix[ 4] &&
		    		matrix[9] != matrix[ 7] &&
		    		matrix[9] != matrix[ 8] &&
				    matrix[9] != matrix[10] &&
		    		matrix[9] != matrix[11] &&
		    		matrix[9] != matrix[13] &&
		    		matrix[9] != matrix[14]
		    	)
		    	{
			    	rawTerrain[                           y+(                           x*dimension)]=matrix[ 1];
			    	rawTerrain[                           y+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
		    		rawTerrain[FastMath.wrap(y+1,dimension)+(                           x*dimension)]=matrix[ 1];
		    	}
		    	else if(
		    		matrix[9] == matrix[ 5] &&
		    		matrix[9] == matrix[10] &&
		    		matrix[9] != matrix[ 1] &&
		    		matrix[9] != matrix[ 2] &&
		    		matrix[9] != matrix[ 4] &&
				    matrix[9] != matrix[ 6] &&
		    		matrix[9] != matrix[ 7] &&
		    		matrix[9] != matrix[ 8] &&
		    		matrix[9] != matrix[11] &&
		    		matrix[9] != matrix[13] &&
		    		matrix[9] != matrix[14]
		    	)
		    	{
			    	rawTerrain[                           y+(                           x*dimension)]=matrix[ 1];
			    	rawTerrain[FastMath.wrap(y+1,dimension)+(                           x*dimension)]=matrix[ 1];
			    	rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
		    	}
		    	else if(
		    		matrix[5] == matrix[ 6] &&
		    		matrix[5] == matrix[10] &&
		    		matrix[5] != matrix[ 1] &&
		    		matrix[5] != matrix[ 2] &&
		    		matrix[5] != matrix[ 4] &&
		    		matrix[5] != matrix[ 7] &&
		    		matrix[5] != matrix[ 8] &&
				    matrix[5] != matrix[ 9] &&
		    		matrix[5] != matrix[11] &&
		    		matrix[5] != matrix[13] &&
		    		matrix[5] != matrix[14]
		    	)
		    	{
			    	rawTerrain[                           y+(                           x*dimension)]=matrix[ 1];
			    	rawTerrain[                           y+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
			    	rawTerrain[FastMath.wrap(y+1,dimension)+(FastMath.wrap(x+1,dimension)*dimension)]=matrix[ 1];
		    	} 
		    	
		    	//SINGLE GAPS
		    	if(matrix[0] == matrix[2] && matrix[0] != matrix[1])
		    	{
			    	rawTerrain[FastMath.wrap(y-1,dimension)+(                           x*dimension)]=matrix[0];
		    	}
		    	else if(matrix[0] == matrix[8] && matrix[0] != matrix[4])
		    	{
		    		rawTerrain[                           y+(FastMath.wrap(x-1,dimension)*dimension)]=matrix[0];
		    	}
		    }
	    }
	    
	    //TODO: CORRECT FOR HIGH CLIFFS
    }

	public static float[] upscale(float[] rawTerrain,int dimension)
    {
		float[] upscaled=new float[rawTerrain.length*4];
	    
	    for(int x=0;x<dimension*2;x+=2)
	    {
		    for(int y=0;y<dimension*2;y+=2)
		    {
		    	upscaled[(x+0)+((y+0)*dimension*2)]=rawTerrain[(x/2)+((y/2)*dimension)];
		    	upscaled[(x+1)+((y+0)*dimension*2)]=rawTerrain[(x/2)+((y/2)*dimension)];
		    	upscaled[(x+0)+((y+1)*dimension*2)]=rawTerrain[(x/2)+((y/2)*dimension)];
		    	upscaled[(x+1)+((y+1)*dimension*2)]=rawTerrain[(x/2)+((y/2)*dimension)];
		    }
	    }
	    
	    return(upscaled);
    }

	private static final float posterize(float val, float stepInterval, int steps)
	{
		return((float) Math.floor(val/stepInterval)/steps);
	}

	private static int getBiomeRecord(JSONArray elevationsToBiomes, int eIndex) throws JSONException
    {
	    for(int i=0;i<elevationsToBiomes.length();i++)
	    {
	    	JSONObject biomeRecord=elevationsToBiomes.getJSONObject(i);
	    	
	    	for(int j=0;j<biomeRecord.getJSONArray("elevations").length();j++)
	    	{
	    		if(eIndex == biomeRecord.getJSONArray("elevations").getInt(j))
	    		{
	    		    return(i);
	    		}
	    	}
	    }
	    
	    return(-1);
    }
	
	
	
	//////
	
	
	
	private static void writeImage(int d, float[] data, String name)
	{
		BufferedImage        bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
		int                  vv;
		
		////

		for(int y=0;y<d;y++)
		{
			for(int x=0;x<d;x++)
			{
				vv=(int)(data[y+(x*d)]*255);
				bi.setRGB(
					x,
					y,
					(vv<<16)|
					(vv<<8)|
					(vv)
				);
			}
		}

		if(d <= 512)
		{
			TERRAIN_IMAGES.put(name,bi);
		}
		else
		{
			BufferedImage sbi=new BufferedImage(512,512,BufferedImage.TYPE_INT_RGB);
			Graphics2D    g2d=sbi.createGraphics();
			
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g2d.drawImage(bi, 0, 0, 512, 512, null);
			
			TERRAIN_IMAGES.put(name,sbi);
		}
	}
	
	private static void writeImage(int d, byte[] data, String name, float divisor)
	{
		BufferedImage        bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
		int                  vv;
		
		////

		for(int y=0;y<d;y++)
		{
			for(int x=0;x<d;x++)
			{
				vv=(int)(((data[y+(x*d)])/divisor)*255);
				bi.setRGB(
					x,
					y,
					(vv<<16)|
					(vv<<8)|
					(vv)
				);
			}
		}

		if(d <= 512)
		{
			TERRAIN_IMAGES.put(name,bi);
		}
		else
		{
			BufferedImage sbi=new BufferedImage(512,512,BufferedImage.TYPE_INT_RGB);
			Graphics2D    g2d=sbi.createGraphics();
			
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g2d.drawImage(bi, 0, 0, 512, 512, null);
			
			TERRAIN_IMAGES.put(name,sbi);
		}
	}
	
	private static void writeImage(int d, byte[] data, String name, JSONArray biomes) throws NumberFormatException, JSONException
	{
		BufferedImage     bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
		Map<Byte,Integer> c =new HashMap<Byte,Integer>();
		
		for(int i=0;i<biomes.length();i++)
		{
			c.put((byte) biomes.getJSONObject(i).getInt("id"),Integer.parseInt(biomes.getJSONObject(i).getString("color"),16));
		}

		for(int i=0;i<d;i++)
		{
			for(int j=0;j<d;j++)
			{
				bi.setRGB(i,j,c.get(data[j+(i*d)]));
			}
		}

		if(d <= 512)
		{
			TERRAIN_IMAGES.put(name,bi);
		}
		else
		{
			BufferedImage sbi=new BufferedImage(512,512,BufferedImage.TYPE_INT_RGB);
			Graphics2D    g2d=sbi.createGraphics();
			
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g2d.drawImage(bi, 0, 0, 512, 512, null);
			
			TERRAIN_IMAGES.put(name,sbi);
		}
	}

	private static void writeImage(int d, short[] data, String name, JSONArray layers) throws NumberFormatException, JSONException
    {
		BufferedImage bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
		int[]         c =new int[layers.length()];
		
		for(int i=0;i<c.length;i++)
		{
			c[i] =Integer.parseInt(layers.getJSONObject(i).getString("color"),16);
		}

		for(int i=0;i<d;i++)
		{
			for(int j=0;j<d;j++)
			{
				bi.setRGB(i,j,c[getTopmostLayerAt(i,j,data,d)]);
			}
		}

		if(d <= 512)
		{
			TERRAIN_IMAGES.put(name,bi);
		}
		else
		{
			BufferedImage sbi=new BufferedImage(512,512,BufferedImage.TYPE_INT_RGB);
			Graphics2D    g2d=sbi.createGraphics();
			
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g2d.drawImage(bi, 0, 0, 512, 512, null);
			
			TERRAIN_IMAGES.put(name,sbi);
		}
    }

	private static int getTopmostLayerAt(int x, int y, short[] l, int d)
	{
		short idx   =15;
		short layers=l[y+(x*d)];
	    
	    while(idx > 0)
	    {
	    	if(((layers >> idx) & 0x01) == 1)
	    	{
	    		return(idx);
	    	}
	    	
	    	idx--;
	    }
	    
	    return(0);
	}
	
	public String getImageAsDataURL(String imageId) throws IOException
	{
		return(
			FileUtils.imageToDataURL(
				TERRAIN_IMAGES.get(
					imageId
				)
			)
		);
	}

	public String getImageName(String imageId)
    {
	    return(TERRAIN_NAMES.get(imageId));
    }
}