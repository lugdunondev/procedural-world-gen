package net.lugdunon.server.worldgen.procedural.noise;

import java.util.Random;

import org.json.JSONObject;

public class DiamondSquare implements INoiseGen
{
	private float values[];
	private int    dim;
	private Random rand;

	private float min;
	private float max;
	
	public float[] computeNoise(Random rand,long instanceId,JSONObject instanceDef,int dimension,JSONObject args) throws Exception
	{
		float   m;
		float   n;
		float   v;
		float[] r;
		int     featureSize=       args.getInt    ("featureSize");
		float   scale      =(float) args.getDouble("scale"      );
		float   lowerBound =(float) args.getDouble("lowerBound" );
		float   upperBound =(float) args.getDouble("upperBound" );
		
		this.rand        =rand;
		this.dim         =dimension;
		
		this.values      =new float[dim*dim];
		this.min         =Float.MAX_VALUE;
		this.max         =Float.MIN_VALUE;
		
		r                =new float[dim*dim];
		
		for(int y=0;y<dim;y+=featureSize)
		{
		    for(int x=0;x<dim;x+=featureSize)
		    {
		        assignValue(x, y, fudge(1.0f));
		    }
		}
		 
		while(featureSize > 1)
		{
			step(featureSize, scale);

		    featureSize/=2;
		    scale      /=2.0f;
		}
		
		m=max-min;
		n=Math.abs(min);
		
		for(int i=0;i<values.length;i++)
		{
			v=(values[i]+n)/m;
			  
			if(v < lowerBound)
			{
				v=0.0f;
			}
			  
			if(v > upperBound)
			{
				v=1.0f;
			}
			  
			r[i]=v;
		}
		
		return(r);
	}
	
	private float fudge(float v)
	{
	    return((float) (((rand.nextDouble()*2.0)-1.0)*v));
	}
	
	private float valueAt(int x, int y)
	{
	    return values[(x & (dim - 1)) + (y & (dim - 1)) * dim];
	}
	 
	private void assignValue(int x, int y, float value)
	{
		if(min > value)
		{
			min=value;
		}
		
		if(max < value)
		{
			max=value;
		}
		
	    values[(x & (dim - 1)) + (y & (dim - 1)) * dim] = value;
	}
	
	private void sampleSquare(int x, int y, int size, float value)
	{
	    int hs=size/2;

	    assignValue(
	    	x,
	    	y,
	    	(
	    		(
	    			valueAt(x - hs, y - hs) +
	    			valueAt(x + hs, y - hs) +
	    			valueAt(x - hs, y + hs) +
	    			valueAt(x + hs, y + hs)
	    		)
	    		/4.0f
	    	)+value
	    );
	 
	}
	 
	private void sampleDiamond(int x, int y, int size, float value)
	{
	    int hs=size/2;

	    assignValue(
	    	x, 
	    	y,
	    	(
	    		(
	    			valueAt(x - hs, y)+
	    			valueAt(x + hs, y)+
	    			valueAt(x, y - hs)+
	    			valueAt(x, y + hs)
	    		)
	    		/4.0f
	    	)+value
	    );
	}
	
	private void step(int stepsize, float scale)
	{
	    int halfstep=stepsize/2;
	 
	    for(int y=halfstep;y<dim+halfstep;y+=stepsize)
	    {
	        for(int x=halfstep;x<dim+halfstep;x+=stepsize)
	        {
	            sampleSquare(x,y,stepsize,fudge(scale));
	        }
	    }
	 
	    for(int y=0;y<dim;y+=stepsize)
	    {
	        for(int x=0;x<dim;x+=stepsize)
	        {
	            sampleDiamond(x+halfstep,y,stepsize,fudge(scale));
	            sampleDiamond(x,y+halfstep,stepsize,fudge(scale));
	        }
	    }
	}
}
