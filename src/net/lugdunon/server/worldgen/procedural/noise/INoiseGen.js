Namespace.declare("net.lugdunon.server.worldgen.procedural.noise");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.noise.INoiseGen");

////

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.init=function(initData)
{
	this.config        =initData.config;
	this.changeDelegate=initData.changeDelegate;
	this.content       =null;
	
	return(this);
}

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.show=function()
{
	var context=this;
	
	this.content=net.lugdunon.ui.Dialog.confirm(
		this.getName(),
		{
			content     :"",
			dialogWidth :this.getWidth(),
			dialogHeight:this.getHeight()
		},
		this,
		function(okClicked)
		{
			if(okClicked)
			{
				context.changeDelegate.noiseGenConfigurationUpdated.call(context.changeDelegate);
			}
			
			return(true);
		},
		function(body,ok,cancel)
		{
			context.renderContent.call(context,body)
		}
	);
};

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.getName=function()
{
	return("Implement Me: getName()!");
}

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.getDescription=function()
{
	return("Implement Me: getDescription()!");
}

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.getWidth=function()
{
	return(320);
}

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.getHeight=function()
{
	return(240);
}

net.lugdunon.server.worldgen.procedural.noise.INoiseGen.prototype.renderContent=function(parentEl)
{
	;
}