package net.lugdunon.server.worldgen.procedural.noise;

import java.util.Random;

import org.json.JSONObject;

public interface INoiseGen
{
	public float[] computeNoise(Random rand,long instanceId,JSONObject instanceDef,int dimension,JSONObject args) throws Exception;
}
