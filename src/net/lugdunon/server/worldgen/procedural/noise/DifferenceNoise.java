package net.lugdunon.server.worldgen.procedural.noise;

import java.util.Random;

import org.json.JSONObject;

public class DifferenceNoise implements INoiseGen
{
	@Override
	public float[] computeNoise(Random rand, long instanceId, JSONObject instanceDef,int dimension, JSONObject args) throws Exception
	{
		INoiseGen noiseGen          =(INoiseGen) DifferenceNoise.class.getClassLoader().loadClass(args.getJSONObject("noise"          ).getString("class")).newInstance();
		INoiseGen differenceNoiseGen=(INoiseGen) DifferenceNoise.class.getClassLoader().loadClass(args.getJSONObject("differenceNoise").getString("class")).newInstance();
		float[]   noise             =noiseGen          .computeNoise(rand, instanceId, instanceDef, dimension, args.getJSONObject("noise"          ).getJSONObject("args"));
		float[]   differenceNoise   =differenceNoiseGen.computeNoise(rand, instanceId, instanceDef, dimension, args.getJSONObject("differenceNoise").getJSONObject("args"));
		
		for(int i=0;i<noise.length;i++)
		{
			if(noise[i] > differenceNoise[i])
			{
				noise[i]=noise          [i]-differenceNoise[i];
			}
			else
			{
				noise[i]=differenceNoise[i]-noise          [i];
			}
		}

		return(noise);
	}
}