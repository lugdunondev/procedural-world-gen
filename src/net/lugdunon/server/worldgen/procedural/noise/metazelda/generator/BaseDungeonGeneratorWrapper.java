package net.lugdunon.server.worldgen.procedural.noise.metazelda.generator;

import java.util.Random;

import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.generators.DungeonGenerator;
import net.bytten.metazelda.util.Coords;
import net.bytten.metazelda.util.GenerationFailureException;
import net.lugdunon.io.JSONObjectSerializable;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.constraints.PlanarConstraints;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.room.IRoomGenerator;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseDungeonGeneratorWrapper implements JSONObjectSerializable
{
	private int            maxKeys;
	private int            maxSwitches;
	
	private long           seed;
	private int            maxSpaces;
	
	private int            dimension;
	private int            roomWidth;
	private int            roomHeight;
	private int            wallThickness;
	private int            doorWidth;
	
	private IRoomGenerator roomGenerator;
	private IDungeon       dungeon;
	
	public BaseDungeonGeneratorWrapper(JSONObject o)
	{
		try
		{
			fromJSONObject(o);
		}
		catch(JSONException e)
		{
			;
		}
	}
	
	public static void main(String[] args)
	{
		Coords[] i=new Coords[1];
		DungeonGenerator generator=new DungeonGenerator(
			System.currentTimeMillis(),
			new PlanarConstraints(
				8,
				8,
				48,
				5,
				0
			)
		);
		
		generator.setGenerateGoal  (true);
		generator.setBossRoomLocked(true);
		generator.generate         (    );
		
		IDungeon dungeon=generator.getDungeon();
		
		for(Room room:dungeon.getRooms())
		{
			System.out.println(room.id+" "+room.getCoords()+" "+room.getItem()+" "+room.getPrecond().getKeyLevel()+" "+room.getIntensity());
		};
		
		System.out.println();
		System.out.println();
		
		for(int x=-3;x<5;x++)
		{
			for(int y=-3;y<5;y++)
			{
				Room r=null;
				
				for(Room room:dungeon.getRooms())
				{
					room.getCoords().toArray(i);
					
					if(i[0].x == x && i[0].y == y)
					{
						r=room;
						break;
					}
				};
				
				if(r != null)
				{
					if(r.isBoss())
					{
						System.out.print("*");
					}
					else if(r.isGoal())
					{
						System.out.print("&");
					}
					else if(r.isStart())
					{
						System.out.print("S");
					}
					else if(r.isSwitch())
					{
						System.out.print("W");
					}
					else if(r.getItem() != null)
					{
						System.out.print(r.getItem());
					}
					else
					{
						System.out.print(r.getPrecond().getKeyLevel());
					}
				}
				else
				{
					System.out.print("-");
				}
				
				System.out.print(" ");
			}

			System.out.println();
			System.out.println();
		}
	}
	
	protected IDungeon generate() throws GenerationFailureException
	{
		DungeonGenerator generator=new DungeonGenerator(
			seed,
			new PlanarConstraints(
				dimension/roomWidth,
				dimension/roomHeight,
				maxSpaces,
				maxKeys,
				maxSwitches
			)
		);
		
		generator.setGenerateGoal  (roomGenerator.hasGoal());
		generator.setBossRoomLocked(false                  );
		generator.generate         (                       );
		
		dungeon=generator.getDungeon();
		
		return(dungeon);
	}

	public IDungeon generate(long seed, int dimension, int roomWidth, int roomHeight, int wallThickness, int doorWidth) throws GenerationFailureException
	{
		this.seed         =seed;
		this.maxSpaces    =(new Random(seed).nextInt((dimension/roomWidth)*(dimension/roomHeight))/2)+(((dimension/roomWidth)*(dimension/roomHeight))/2);
		
		this.dimension    =dimension;
		this.roomWidth    =roomWidth;
		this.roomHeight   =roomHeight;
		this.wallThickness=wallThickness;
		this.doorWidth    =doorWidth;
	
		return(generate());
	}

	public IDungeon generate(Random random, int dimension, int roomWidth, int roomHeight, int wallThickness, int doorWidth) throws GenerationFailureException
	{
		this.seed         =random.nextLong();
		this.maxSpaces    =(new Random(seed).nextInt((dimension/roomWidth)*(dimension/roomHeight))/2)+(((dimension/roomWidth)*(dimension/roomHeight))/2);
		
		this.dimension    =dimension;
		this.roomWidth    =roomWidth;
		this.roomHeight   =roomHeight;
		this.wallThickness=wallThickness;
		this.doorWidth    =doorWidth;
	
		return(generate());
	}
	
	public IDungeon getDungeon()
    {
    	return dungeon;
    }

	public int getDimension()
    {
    	return dimension;
    }

	public int getRoomWidth()
    {
    	return roomWidth;
    }

	public int getRoomHeight()
    {
    	return roomHeight;
    }

	public int getWallThickness()
    {
    	return wallThickness;
    }

	public int getDoorWidth()
    {
    	return doorWidth;
    }

	public long getSeed()
    {
    	return seed;
    }

	public int getMaxSpaces()
    {
    	return maxSpaces;
    }

	public int getMaxKeys()
    {
    	return maxKeys;
    }

	public int getMaxSwitches()
    {
    	return maxSwitches;
    }
	
	public void setRoomGenerator(IRoomGenerator roomGenerator)
    {
	    this.roomGenerator=roomGenerator;
    }
	
	public IRoomGenerator getRoomGenerator()
	{
		return(roomGenerator);
	}

	@Override
    public void fromJSONObject(JSONObject o) throws JSONException
    {
	    this.maxKeys    =o.getInt("maxKeys"    );
	    this.maxSwitches=o.getInt("maxSwitches");
	    
	    if(o.has("seed"))
	    {
	    	this.seed         =o.getLong("seed"         );
	    	this.maxSpaces    =o.getInt ("maxSpaces"    );

	    	this.dimension    =o.getInt ("dimension"    );
	    	this.roomWidth    =o.getInt ("roomWidth"    );
	    	this.roomHeight   =o.getInt ("roomHeight"   );
	    	this.wallThickness=o.getInt ("wallThickness");
	    	this.doorWidth    =o.getInt ("doorWidth"    );
	    	
	    	this.dungeon      =generate();
	    }
    }

	@Override
    public JSONObject toJSONObject() throws JSONException
    {
		JSONObject o=new JSONObject();

		o.put("maxKeys",      maxKeys      );
		o.put("maxSwitches",  maxSwitches  );

		o.put("seed",         seed         );
		o.put("maxSpaces",    maxSpaces    );

		o.put("dimension",    dimension    );
		o.put("roomWidth",    roomWidth    );
		o.put("roomHeight",   roomHeight   );
		o.put("wallThickness",wallThickness);
		o.put("doorWidth",    doorWidth    );
		
	    return(o);
    }
}
