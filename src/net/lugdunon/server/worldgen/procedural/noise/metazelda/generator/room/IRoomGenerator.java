package net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.room;

import net.bytten.metazelda.Edge;
import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Direction;
import net.lugdunon.math.Rect;
import net.lugdunon.world.instance.Instance;

import org.json.JSONObject;

public interface IRoomGenerator
{
	public void    init                     (JSONObject args);
	
	public boolean hasGoal                  ();
	
	public void    generateRoom             (IDungeon dungeon, Room room, Rect roomDim, float[] map, int mapSize, int wallThickness, int doorThickness);
	public void    generateConnector        (IDungeon dungeon, Room room, Rect roomDim, float[] map, int mapSize, int wallThickness, int doorThickness, Direction orientation);
	
	public void    generateNPCsForRoom      (Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness);
	public void    generateItemsForRoom     (Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness);
	public void    generateDoorForConnector (Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness, Direction orientation, Edge edge);
}
