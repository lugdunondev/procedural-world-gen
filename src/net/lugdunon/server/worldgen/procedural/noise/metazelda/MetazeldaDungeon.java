package net.lugdunon.server.worldgen.procedural.noise.metazelda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import net.bytten.metazelda.Edge;
import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Coords;
import net.bytten.metazelda.util.GenerationFailureException;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.procedural.noise.INoiseGen;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.BaseDungeonGeneratorWrapper;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.room.IRoomGenerator;

import org.json.JSONException;
import org.json.JSONObject;

public class MetazeldaDungeon implements INoiseGen
{
	Random                      rand         ;
	long                        seed         ;
	int                         dimension    ;
	int                         roomWidth    ;
	int                         roomHeight   ;
	int                         wallThickness;
	int                         doorWidth    ;
	BaseDungeonGeneratorWrapper dgw          ;
	IDungeon                    dungeon      ;
	IRoomGenerator              roomGenerator;
	
	List<Rect> rooms=new ArrayList<Rect>();
	
	public IDungeon getDungeon()
	{
		return(dungeon);
	}
	
	@Override
	public float[] computeNoise(Random rand, long instanceId, JSONObject instanceDef, int dimension, JSONObject args) throws Exception
	{
		float[] noise=new float[dimension*dimension];
		
		Arrays.fill(noise,0);
		
		this.rand=rand;
		
		generateDungeon(dimension, args);
		
		cacheSeed(instanceDef);

		createNoiseMapFromRooms(noise,dungeon);
		
		return(noise);
	}
	
	public BaseDungeonGeneratorWrapper generateDungeon(int dimension, JSONObject args) throws Exception
	{
		this.dimension=dimension;
		
		if(args.has("seed") && !args.isNull("seed"))
		{
			this.rand=null;
			this.seed=args.getLong("seed");
		}
		
		roomWidth     =args.getInt("roomWidth"    );
		roomHeight    =args.getInt("roomHeight"   );
		wallThickness =args.getInt("wallThickness");
		doorWidth     =args.getInt("doorWidth"    );
		dgw           =(BaseDungeonGeneratorWrapper) MetazeldaDungeon.class.getClassLoader().loadClass(
			args.getJSONObject("generator").getString    ("class")
		).getConstructor(JSONObject.class).newInstance(
			args.getJSONObject("generator").getJSONObject("args" )
		);
		roomGenerator =(IRoomGenerator) MetazeldaDungeon.class.getClassLoader().loadClass(
			args.getJSONObject("roomGenerator").getString    ("class")
		).newInstance();

		roomGenerator.init(args.getJSONObject("roomGenerator").getJSONObject("args"));
		
		dgw.setRoomGenerator(roomGenerator);
		
		generateDungeon();
		
		return(dgw);
	}

	private void cacheSeed(JSONObject instanceDef) throws JSONException
    {
		instanceDef.getJSONObject("proceduralConfiguration").getJSONObject("noise").getJSONObject("args").put("seed", dgw.getSeed());
    }

	private void generateDungeon()
    {
		//SEED WAS CACHED IN INSTANCE CONFIG
		if(rand == null)
		{
			dungeon=dgw.generate(seed, dimension, roomWidth, roomHeight, wallThickness, doorWidth);
		}
		else
		{
			//KEEP TRYING UNTIL WE GET IT
			while(dungeon == null)
			{
				try
				{
					dungeon=dgw.generate(rand, dimension, roomWidth, roomHeight, wallThickness, doorWidth);
				}
				catch (GenerationFailureException e)
				{
					rand.nextLong();
				}
			}
		}
    }

	private void createNoiseMapFromRooms(float[] noise, IDungeon dungeon)
    {
		Rect   r;
		Coords c;
		Coords d=new Coords(
			dungeon.getExtentBounds().left,
			dungeon.getExtentBounds().top
		);
		int    b=wallThickness/2;
		int    p=doorWidth    /2;
		
		for(Room room:dungeon.getRooms())
		{
			c=room.getCoords().iterator().next().subtract(d);
			r=new Rect(
				c.x*roomWidth,
				c.y*roomHeight,
				roomWidth,
				roomHeight
			);
			
			roomGenerator.generateRoom(
				dungeon,
				room,
				r,
				noise,
				dimension,
				b,
				p
			);

	    	for(Edge edge:room.getEdges())
	    	{
				roomGenerator.generateConnector(
					dungeon,
					room,
					r,
					noise,
					dimension,
					b,
					p,
					room.getCoords().iterator().next().getDirectionTo(dungeon.get(edge.getTargetRoomId()).getCoords().iterator().next())
				);
	    	}
		}
    }

	////
	
//	public static void main(String[] args)
//	{
//		try
//        {
//			int        dimension=256;
//	        float[]    noise    =new MetazeldaDungeon().computeNoise(
//	        	FastMath.randomFromSeed("FLOCRAB"),
//	        	-1,
//	        	FileUtils.getJSONObject(
//	        		MetazeldaDungeon.class.getClassLoader().getResourceAsStream("net/lugdunon/server/worldgen/procedural/etc/instanceTemplates/LOWLAND_RUINS/instance.json")
//            	),
//	        	dimension,
//	        	FileUtils.getJSONObject(
//	        		MetazeldaDungeon.class.getClassLoader().getResourceAsStream("net/lugdunon/server/worldgen/procedural/etc/instanceTemplates/LOWLAND_RUINS/instance.json")
//            	).getJSONObject("proceduralConfiguration").getJSONObject("noise").getJSONObject("args")
//	        );
//
//	        writeImage(dimension,noise,"diff");
//        }
//        catch (Exception e)
//        {
//	        e.printStackTrace();
//        }
//	}
//	
//	private static void writeImage(int d, float[] data, String name) throws FileNotFoundException, IOException
//	{
//		BufferedImage        bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
//		int                  vv;
//		
//		////
//
//		for(int y=0;y<d;y++)
//		{
//			for(int x=0;x<d;x++)
//			{
//				vv=(int)(data[y+(x*d)]*255);
//
//				bi.setRGB(
//					x,
//					y,
//					(vv<<16)|
//					(vv<<8)|
//					(vv)
//				);
//			}
//		}
//		
//		ImageIO.write(bi, "png", new FileOutputStream(new File("/Users/christophergray/Desktop/ls/"+name+".png")));
//	}
}