package net.lugdunon.server.worldgen.procedural.noise.metazelda.constraints;

import org.json.JSONException;
import org.json.JSONObject;

import net.bytten.metazelda.constraints.CountConstraints;
import net.bytten.metazelda.util.Coords;
import net.lugdunon.io.JSONObjectSerializable;

public class PlanarConstraints extends CountConstraints implements JSONObjectSerializable
{
	public static final int	DEFAULT_MAX_KEYS	= 4, DEFAULT_MAX_SWITCHES = 1;

	private int maxWidth;
	private int maxHeight;

//	private int negContraintsX;
//	private int negContraintsY;
//	private int posContraintsX;
//	private int posContraintsY;
	
	public PlanarConstraints(JSONObject o) throws JSONException
	{
		this(
			o.getInt("maxWidth"   ),
			o.getInt("maxHeight"  ),
			o.getInt("maxSpaces"  ),
			o.getInt("maxKeys"    ),
			o.getInt("maxSwitches")
	    );
	}
	
	public PlanarConstraints(int maxWidth, int maxHeight, int maxSpaces)
	{
		this(maxWidth,maxHeight,maxSpaces,DEFAULT_MAX_KEYS,DEFAULT_MAX_SWITCHES);
	}
	
	public PlanarConstraints(int maxWidth, int maxHeight, int maxSpaces, int maxKeys, int maxSwitches)
	{
		super(maxSpaces < maxWidth*maxHeight ? maxSpaces : maxWidth*maxHeight,maxKeys, maxSwitches);

		this.maxWidth      =maxWidth;
		this.maxHeight     =maxHeight;
		
//		this.negContraintsX=0;
//		this.negContraintsY=0;
//		this.posContraintsX=0;
//		this.posContraintsY=0;
	}

	@Override
	protected boolean validRoomCoords(Coords c)
	{
//		if(FastMath.diff(c.x, negContraintsX) > maxWidth  || FastMath.diff(c.x, posContraintsX) > maxWidth )
//		{
//			return(false);
//		}
//
//		if(FastMath.diff(c.y, negContraintsY) > maxHeight || FastMath.diff(c.y, posContraintsY) > maxHeight)
//		{
//			return(false);
//		}
//		
//		if(c.x < negContraintsX)
//		{
//			negContraintsX=c.x;
//		}
//		else if(c.x > posContraintsX)
//		{
//			posContraintsX=c.x;
//		}
//		
//		if(c.y < negContraintsY)
//		{
//			negContraintsY=c.y;
//		}
//		else if(c.y > posContraintsY)
//		{
//			posContraintsY=c.y;
//		}
//		
//		return(true);
		
		return(
			c.x > -(maxWidth /2) && c.x <= (maxWidth /2) &&
			c.y > -(maxHeight/2) && c.y <= (maxHeight/2)
		);
	}

	@Override
    public void fromJSONObject(JSONObject o) throws JSONException
    {
		maxWidth   =o.getInt("maxWidth"   );
		maxHeight  =o.getInt("maxHeight"  );
		maxSpaces  =o.getInt("maxSpaces"  );
		maxKeys    =o.getInt("maxKeys"    );
		maxSwitches=o.getInt("maxSwitches");
    }

	@Override
    public JSONObject toJSONObject() throws JSONException
    {
	    JSONObject o=new JSONObject();

	    o.put("maxWidth",   maxWidth   );
	    o.put("maxHeight",  maxHeight  );
	    o.put("maxSpaces",  maxSpaces  );
	    o.put("maxKeys",    maxKeys    );
	    o.put("maxSwitches",maxSwitches);
	    
	    return(o);
    }
}
