package net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.room;

import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.lugdunon.math.Rect;

public class EasedCornersRoomGenerator extends DefaultRoomGenerator
{
	@Override
    public void generateRoom(IDungeon dungeon, Room room, Rect roomDim, float[] map, int mapSize, int wallThickness, int doorThickness)
    {
    	for(int x=roomDim.getPosition().getX()+wallThickness;x<roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness;x++)
    	{
	    	for(int y=roomDim.getPosition().getY()+wallThickness;y<roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness;y++)
	    	{
	    		if(
	    			!(
		    			(x == roomDim.getPosition().getX()+wallThickness                            && y == roomDim.getPosition().getY()+wallThickness                           ) ||
		    			(x == roomDim.getPosition().getX()+wallThickness                            && y == roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness-1) ||
		    			(x == roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness-1 && y == roomDim.getPosition().getY()+wallThickness                           ) ||
		    			(x == roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness-1 && y == roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness-1)
	    			)
	    		)
	    		{
	    			map[y+(x*mapSize)]=1.0f;
	    		}
	    	}
    	}
    }
}
