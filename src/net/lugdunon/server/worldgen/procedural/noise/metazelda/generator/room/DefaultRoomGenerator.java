package net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.room;

import java.util.ArrayList;
import java.util.List;

import net.bytten.metazelda.Edge;
import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Direction;
import net.lugdunon.math.Point;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.procedural.biome.MetazeldaDungeonBiome;
import net.lugdunon.server.worldgen.procedural.room.BaseIntermediaryRoom;
import net.lugdunon.state.State;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.instance.Instance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DefaultRoomGenerator implements IRoomGenerator
{
	protected static final int VERTICAL_CONNECTOR_PADDING_TOP    = 1;
	protected static final int VERTICAL_CONNECTOR_PADDING_BOTTOM = 1;
	protected static final int HORIZONTAL_CONNECTOR_PADDING_LEFT = 0;
	protected static final int HORIZONTAL_CONNECTOR_PADDING_RIGHT=-1;

	private String                     boss;
	private Item                       goal;
	
	private List<BaseIntermediaryRoom> intermediaryRooms;
	
	@Override
	public void init(JSONObject args)
	{
	    try
	    {
	    	JSONArray     ira=(JSONArray) args.getJSONArray("intermediaryRooms");

	    	intermediaryRooms=new ArrayList<BaseIntermediaryRoom>();
	    	boss             =(String) args.getString("boss");
	    	goal             =State.instance().getWorld().getItemDefinitions().getItemDef((String) args.getString("goal"));
	    	
	    	for(int i=0;i<ira.length();i++)
	    	{
	    		intermediaryRooms.add(
	    			(BaseIntermediaryRoom) MetazeldaDungeonBiome.class.getClassLoader().loadClass(
    					ira.getJSONObject(i).getString    ("class")
		    		).getConstructor(JSONObject.class).newInstance(
	    				ira.getJSONObject(i).getJSONObject("args" )
		    		)
	    		);
	    	}
	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/////

	public String getBoss()
    {
		if(boss.indexOf('|') > -1)
		{
			String[] bosses=boss.split("\\|");
			
			boss=bosses[(int) Math.floor(WorldGenerator.getRandom().nextDouble()*bosses.length)];
		}
		
    	return boss;
    }
	
	public Item getGoal()
    {
    	return goal;
    }
	
	public List<BaseIntermediaryRoom> getIntermediaryRooms()
	{
		return(intermediaryRooms);
	}
	
	/////

	@Override
	public boolean hasGoal()
    {
    	return(getGoal() != null);
    }
	
	@Override
    public void generateRoom(IDungeon dungeon, Room room, Rect roomDim, float[] map, int mapSize, int wallThickness, int doorThickness)
    {
    	for(int x=roomDim.getPosition().getX()+wallThickness;x<roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness;x++)
    	{
	    	for(int y=roomDim.getPosition().getY()+wallThickness;y<roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness;y++)
	    	{
	    		map[y+(x*mapSize)]=1.0f;
	    	}
    	}
    }

	@Override
    public void generateConnector(IDungeon dungeon, Room room, Rect roomDim, float[] map, int mapSize, int wallThickness, int doorThickness, Direction orientation)
    {
		if(orientation == Direction.N)
		{
			for(
				int x=roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)-doorThickness-HORIZONTAL_CONNECTOR_PADDING_LEFT;
					x<roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)+doorThickness+HORIZONTAL_CONNECTOR_PADDING_RIGHT;
					x++
			)
	    	{
		    	for(int y=roomDim.getPosition().getY();y<roomDim.getPosition().getY()+wallThickness;y++)
		    	{
		    		map[y+(x*mapSize)]=1.0f;
		    	}
	    	}
		}
		else if(orientation == Direction.S)
		{
			for(
				int x=roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)-doorThickness-HORIZONTAL_CONNECTOR_PADDING_LEFT;
					x<roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)+doorThickness+HORIZONTAL_CONNECTOR_PADDING_RIGHT;
					x++
			)
	    	{
		    	for(int y=roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness;y<roomDim.getPosition().getY()+roomDim.getSize().getH();y++)
		    	{
		    		map[y+(x*mapSize)]=1.0f;
		    	}
	    	}
		}
		else if(orientation == Direction.W)
		{
	    	for(int x=roomDim.getPosition().getX();x<roomDim.getPosition().getX()+wallThickness;x++)
	    	{
		    	for(
		    		int y=roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)-doorThickness-VERTICAL_CONNECTOR_PADDING_TOP;
		    			y<roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)+doorThickness+VERTICAL_CONNECTOR_PADDING_BOTTOM;
		    			y++
		    	)
		    	{
		    		map[y+(x*mapSize)]=1.0f;
		    	}
	    	}
		}
		else if(orientation == Direction.E)
		{
			for(int x=roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness;x<roomDim.getPosition().getX()+roomDim.getSize().getW();x++)
	    	{
		    	for(
		    		int y=roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)-doorThickness-VERTICAL_CONNECTOR_PADDING_TOP;
		    			y<roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)+doorThickness+VERTICAL_CONNECTOR_PADDING_BOTTOM;
		    			y++
		    	)
		    	{
		    		map[y+(x*mapSize)]=1.0f;
		    	}
	    	}
		}
    }

	@Override
    public void generateItemsForRoom(Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness)
    {
		try
		{
	    	//PLACE REGENERATING CHEST
		    if(room.isGoal())
		    {
		    	instance.getPlaceableItemCache().newPlaceableItemReference(
		    		getGoal(),
		    		new Point(
		    			roomDim.getPosition().getX()+roomDim.getSize().getW()/2,
		    			roomDim.getPosition().getY()+roomDim.getSize().getH()/2
		    		).sub(
		    			getGoal().getGroundingPoint()
		    		)
		    	);
		    }
		    //PLACE ITEMS FOR REMAINING ROOMS
		    else
		    {
				for(BaseIntermediaryRoom iRoom:getIntermediaryRooms())
				{
					//ROOM IS OF THE RIGHT INTENSITY
					if(iRoom.getIntensityThreshold() <= room.getIntensity())
					{
						iRoom.placeItems(
							instance,
							dungeon,
							room,
							roomDim,
							wallThickness,
							doorThickness
						);
						break;
					}
				}
		    }
		}
		catch(Exception e)
		{
			e.printStackTrace();
//			logger.error("Error generating items for dungeon instance with id {}.",instance.getInstanceId());
		}
    }

	@Override
    public void generateDoorForConnector(Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness, Direction orientation, Edge edge)
    {
		PlaceableItemInstance door=null;
		
		if(orientation == Direction.N)
		{
			for(
				int x=roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)-doorThickness-HORIZONTAL_CONNECTOR_PADDING_LEFT,i=0;
					x<roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)+doorThickness+HORIZONTAL_CONNECTOR_PADDING_RIGHT;
					x++,i++
			)
	    	{
				int y=roomDim.getPosition().getY();
				
				PlaceableItemInstance pii=instance.getPlaceableItemCache().newPlaceableItemReference(
	    			State.instance().getWorld().getItemDefinitions().getItemDef(i==1?"FENCE.STONE.GATE":"FENCE.STONE"),
	    			new Point(x,y+(i==1?-2:-1))
	    		);
				
				if(i==1)
				{
					door=pii;
				}
	    	}
		}
		else if(orientation == Direction.S)
		{
			for(
				int x=roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)-doorThickness-HORIZONTAL_CONNECTOR_PADDING_LEFT,i=0;
					x<roomDim.getPosition().getX()+(roomDim.getSize().getW()/2)+doorThickness+HORIZONTAL_CONNECTOR_PADDING_RIGHT;
					x++,i++
			)
	    	{
				int y=roomDim.getPosition().getY()+roomDim.getSize().getH()-wallThickness;

				PlaceableItemInstance pii=instance.getPlaceableItemCache().newPlaceableItemReference(
	    			State.instance().getWorld().getItemDefinitions().getItemDef(i==1?"FENCE.STONE.GATE":"FENCE.STONE"),
	    			new Point(x,y+(i==1?-1: 0))
	    		);
				
				if(i==1)
				{
					door=pii;
				}
	    	}
		}
		else if(orientation == Direction.W)
		{
	    	int x=roomDim.getPosition().getX();

	    	for(
	    		int y=roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)+2-doorThickness-VERTICAL_CONNECTOR_PADDING_TOP,i=0;
	    			y<roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)-1+doorThickness+VERTICAL_CONNECTOR_PADDING_BOTTOM;
	    			y++,i++
	    	)
	    	{
	    		PlaceableItemInstance pii=instance.getPlaceableItemCache().newPlaceableItemReference(
	    			State.instance().getWorld().getItemDefinitions().getItemDef(i==1?"FENCE.STONE.GATE":"FENCE.STONE"),
	    			new Point(x,y+(i==1?-1:0))
	    		);
				
				if(i==1)
				{
					door=pii;
				}
	    	}
		}
		else if(orientation == Direction.E)
		{
			int x=roomDim.getPosition().getX()+roomDim.getSize().getW()-wallThickness;

	    	for(
	    		int y=roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)+2-doorThickness-VERTICAL_CONNECTOR_PADDING_TOP,i=0;
	    			y<roomDim.getPosition().getY()+(roomDim.getSize().getH()/2)-1+doorThickness+VERTICAL_CONNECTOR_PADDING_BOTTOM;
	    			y++,i++
	    	)
	    	{
	    		PlaceableItemInstance pii=instance.getPlaceableItemCache().newPlaceableItemReference(
	    			State.instance().getWorld().getItemDefinitions().getItemDef(i==1?"FENCE.STONE.GATE":"FENCE.STONE"),
	    			new Point(x,y+(i==1?-1:0))
	    		);
				
				if(i==1)
				{
					door=pii;
				}
	    	}
		}
		
	    //TODO: GENERATE LOCKED GATES BETWEEN ROOMS
		
		if(door != null)
		{
			if(door.getUserDefinedData() == null)
			{
				door.setUserDefinedData(new JSONObject());
			}
			
			try
            {
	            door.getUserDefinedData().put("net.lugdunon.server.worldgen.procedural.room.door",      true);
	            door.getUserDefinedData().put("net.lugdunon.server.worldgen.procedural.source.room.id", room.id);
	            door.getUserDefinedData().put("net.lugdunon.server.worldgen.procedural.target.room.id", edge.getTargetRoomId());
            }
            catch (JSONException e)
            {
            	;
            }
		}
    }

	@Override
    public void generateNPCsForRoom(Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness)
    {
		try
		{
			//SPAWN NPCS
			if(room.isBoss())
			{
				//SPAWN BOSS
				instance.getNpcCache().newNpc(
					getBoss()
				).setLocation(
					new Point(
		    			roomDim.getPosition().getX()+roomDim.getSize().getW()/2,
		    			roomDim.getPosition().getY()+roomDim.getSize().getH()/2
		    		)
				);
			}
			else
			{
				for(BaseIntermediaryRoom iRoom:getIntermediaryRooms())
				{
					//ROOM IS OF THE RIGHT INTENSITY
					if(iRoom.getIntensityThreshold() <= room.getIntensity())
					{
						iRoom.spawnNPCs(
							instance,
							dungeon,
							room,
							roomDim,
							wallThickness,
							doorThickness
						);
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
//			logger.error("Error generating npcs for dungeon instance with id {}.",instance.getInstanceId());
		}
    }
}
