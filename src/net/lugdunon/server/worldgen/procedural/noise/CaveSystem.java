package net.lugdunon.server.worldgen.procedural.noise;

import java.util.Random;

import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;

import org.json.JSONObject;

public class CaveSystem implements INoiseGen
{
	@Override
	public float[] computeNoise(Random rand, long instanceId, JSONObject instanceDef, int dimension, JSONObject args) throws Exception
	{
	    int             halfDim=dimension/2;
		DifferenceNoise dn     =new DifferenceNoise();
		float[]         noise  =dn.computeNoise(rand, instanceId, instanceDef, halfDim, args.getJSONObject("noise").getJSONObject("args"));
		
		noise=ProceduralWorldGen.posterizeAndSmooth(noise, args.getInt("steps"), args.getInt("smoothing"), halfDim);
		noise=createCaverns(noise);
		noise=ProceduralWorldGen.upscale(noise, halfDim);
		
		return(noise);
	}
	
	private float[] createCaverns(float[] noise)
    {
		float[] caverns=new float[noise.length];
		
		for(int i=0;i<noise.length;i++)
		{
			caverns[i]=(noise[i] == 0.0f)?1.0f:0.0f;
		}
		
	    return(caverns);
    }

//	public static void main(String[] args)
//	{
//		try
//        {
//			JSONObject instConf =FileUtils.getJSONObject(
//        		CaveSystem.class.getClassLoader().getResourceAsStream("net/lugdunon/server/worldgen/procedural/etc/instanceTemplates/LOWLAND_CAVERN/instance.json")
//	        );
//			JSONObject procConf =instConf.getJSONObject("proceduralConfiguration").getJSONObject("noise").getJSONObject("args");
//			int        dimension=instConf.getInt("dimension");
//	        float[]    noise    =new CaveSystem().computeNoise(
//	        	FastMath.randomFromSeed("LUGDUNON"),
//	        	dimension,
//	        	procConf
//	        );
//
//	        writeImage(dimension,noise,"diff");
//        }
//        catch (Exception e)
//        {
//	        e.printStackTrace();
//        }
//	}
//	
//	private static void writeImage(int d, float[] data, String name) throws FileNotFoundException, IOException
//	{
//		BufferedImage        bi=new BufferedImage(d,d,BufferedImage.TYPE_INT_RGB);
//		int                  vv;
//		
//		////
//
//		for(int y=0;y<d;y++)
//		{
//			for(int x=0;x<d;x++)
//			{
//				vv=(int)(data[y+(x*d)]*255);
//
//				bi.setRGB(
//					x,
//					y,
//					(vv<<16)|
//					(vv<<8)|
//					(vv)
//				);
//			}
//		}
//		
//		ImageIO.write(bi, "png", new FileOutputStream(new File("/Users/christophergray/Desktop/ls/"+name+".png")));
//	}
}
