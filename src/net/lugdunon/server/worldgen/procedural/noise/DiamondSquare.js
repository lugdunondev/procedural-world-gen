Namespace.declare("net.lugdunon.server.worldgen.procedural.noise");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.noise.DiamondSquare","net.lugdunon.server.worldgen.procedural.noise.INoiseGen");

////

net.lugdunon.server.worldgen.procedural.noise.DiamondSquare.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.noise.DiamondSquare,"init",[initData])
	
	return(this);
}