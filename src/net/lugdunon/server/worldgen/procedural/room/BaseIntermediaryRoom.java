package net.lugdunon.server.worldgen.procedural.room;

import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.io.JSONObjectSerializable;
import net.lugdunon.math.Point;
import net.lugdunon.math.Rect;
import net.lugdunon.state.State;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.instance.Instance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseIntermediaryRoom implements JSONObjectSerializable
{
	protected static final int PADDING_TOP   =3;
	protected static final int PADDING_BOTTOM=1;
	protected static final int PADDING_LEFT  =1;
	protected static final int PADDING_RIGHT =1;
	
	private double    intensityThreshold;
	private JSONArray constants;
	private JSONArray variables;
	private JSONArray npcs;
	
	public BaseIntermediaryRoom(JSONObject o)
	{
		try
		{
			fromJSONObject(o);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	////

	public double getIntensityThreshold()
    {
    	return intensityThreshold;
    }
	
	public void placeItems(Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness)
	{
		try
		{
			double probability=State.instance().getWorld().getRandom().nextDouble();
			
			//PLACE CONSTANT ITEMS
			for(int i=0;i<constants.length();i++)
			{
				placeItem(
					constants.getJSONObject(i),
					instance,
					dungeon,
					room,
					roomDim,
					wallThickness,
					doorThickness
				);
			}
			
			//PLACE VARIABLE ITEMS
			for(int i=0;i<variables.length();i++)
			{
				if(variables.getJSONObject(i).getDouble("probability") >= probability)
				{
					JSONObject varibleItemDef=variables.getJSONObject(i);
					
					for(int j=0;j<varibleItemDef.getJSONArray("items").length();j++)
					{
						placeItem(
							varibleItemDef.getJSONArray("items").getJSONObject(j),
							instance,
							dungeon,
							room,
							roomDim,
							wallThickness,
							doorThickness
						);
					}
					break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void placeItem(JSONObject item, Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness) throws JSONException
	{
		PlaceableItemInstance pii=instance.getPlaceableItemCache().newPlaceableItemReference(
			State.instance().getWorld().getItemDefinitions().getItemDef(item.getString("id")),
			getItemLocation(
				State.instance().getWorld().getItemDefinitions().getItemDef(item.getString("id")),
				item.getJSONObject("location"),
				roomDim,
				wallThickness,
				doorThickness
			),
			(item.has("state") && !item.isNull("state"))?item.getString("state"):null,
			true,
			null,
			null,
			null,
			null
		);
		
		//CHECK LIGHTSOURCE
		if(pii.getItemDef().isLightsource())
		{
			if(pii.getUserDefinedData() == null)
			{
				pii.setUserDefinedData(new JSONObject());
			}
	
			pii.getUserDefinedData().put("net.lugdunon.server.worldgen.procedural.room.lightsource",true   );
			pii.getUserDefinedData().put("net.lugdunon.server.worldgen.procedural.room.id",         room.id);
		}
	}
	
	private Point getItemLocation(Item item, JSONObject location, Rect roomDim, int wallThickness, int doorThickness) throws JSONException
    {
		Point p=new Point(
			roomDim.getPosition().getX(),
			roomDim.getPosition().getY()
		);
		
		p.sub(item.getGroundingPoint());
		
		if(location.has("left") && !location.isNull("left"))
		{
			p.addX(wallThickness);
			p.addX(location.getInt("left"));
		}
		else if(location.has("right") && !location.isNull("right"))
		{
			p.addX(roomDim.getSize().getW());
			p.subX(wallThickness);
			p.subX(location.getInt("right"));
		}
		else if(location.has("hcenter") && !location.isNull("hcenter"))
		{
			p.addX(roomDim.getSize().getW()/2);
			p.addX(location.getInt("hcenter"));
		}
		
		if(location.has("top") && !location.isNull("top"))
		{
			p.addY(wallThickness);
			p.addY(location.getInt("top"));
		}
		else if(location.has("bottom") && !location.isNull("bottom"))
		{
			p.addY(roomDim.getSize().getH());
			p.subY(wallThickness);
			p.subY(location.getInt("bottom"));
		}
		else if(location.has("vcenter") && !location.isNull("vcenter"))
		{
			p.addY(roomDim.getSize().getH()/2);
			p.addY(location.getInt("vcenter"));
		}
		
	    return(p);
    }
	
	////
	
	public void spawnNPCs(Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness)
	{
		try
		{
			double probability=State.instance().getWorld().getRandom().nextDouble();
			
			//SPAWN NPCS
			for(int i=0;i<npcs.length();i++)
			{
				if(npcs.getJSONObject(i).getDouble("probability") >= probability)
				{
					JSONObject npcDef=npcs.getJSONObject(i);
					
					for(int j=0;j<npcDef.getJSONArray("npcs").length();j++)
					{
						spawnNPC(
							npcDef.getJSONArray("npcs").getString(j),
							instance,
							dungeon,
							room,
							roomDim,
							wallThickness,
							doorThickness
						);
					}
					break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void spawnNPC(String npcId, Instance instance, IDungeon dungeon, Room room, Rect roomDim, int wallThickness, int doorThickness) throws JSONException
	{
		int count=1;
		
		if(npcId.indexOf("|") > -1)
		{
			String[] id=npcId.split("\\|");
			
			npcId=id[0];
			
			try
			{
				if(id[1].indexOf("-") > -1)
				{
					String[] range=id[1].split("\\-");
					
					count=FastMath.boundedRandomValue(Integer.parseInt(range[0]),Integer.parseInt(range[1]));
				}
				else
				{
					count=Integer.parseInt(id[1]);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		for(int i=0;i<count;i++)
		{
			try
			{
				CommandProperties  props=new CommandProperties();
				Point              loc  =new Point(
					FastMath.boundedRandomValue(
						roomDim.getPosition().getX()+wallThickness+PADDING_LEFT,
						roomDim.getPosition().getX()+roomDim.getSize().getW()-(wallThickness*2)-PADDING_RIGHT
					),
					FastMath.boundedRandomValue(
						roomDim.getPosition().getY()+wallThickness+PADDING_TOP,
						roomDim.getPosition().getY()+roomDim.getSize().getH()-(wallThickness*2)-PADDING_BOTTOM
					)
				);
				
				props.setString  ("npcTemplateId",npcId   );
				props.setPoint   ("location",     loc     );
				props.setInstance("instance",     instance);
				
		        State.instance().getGame().addIncomingRequest(
		    		"CORE.COMMAND.SPAWN.NPC",
		    		props
		        );
			}
			catch(Exception e)
			{
				;
			}
		}
	}

	////
	
	@Override
    public void fromJSONObject(JSONObject o) throws JSONException
    {
		intensityThreshold=o.getDouble("intensityThreshold");
		constants         =new JSONArray();
		variables         =new JSONArray();
		npcs              =new JSONArray();

		if(o.has("contents") && !o.isNull("contents"))
		{
			if(o.getJSONObject("contents").has("constants") && !o.getJSONObject("contents").isNull("constants"))
			{
				constants=o.getJSONObject("contents").getJSONArray("constants");
			}

			if(o.getJSONObject("contents").has("variables") && !o.getJSONObject("contents").isNull("variables"))
			{
				variables=o.getJSONObject("contents").getJSONArray("variables");	
			}
		}
		
		if(o.has("npcs") && !o.isNull("npcs"))
		{
			npcs=o.getJSONArray("npcs");	
		}
    }

	@Override
    public JSONObject toJSONObject() throws JSONException
    {
	    // TODO Auto-generated method stub
	    return null;
    }
}
