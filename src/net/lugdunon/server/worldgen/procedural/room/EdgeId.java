package net.lugdunon.server.worldgen.procedural.room;

public class EdgeId
{
	private int startRoom;
	private int targetRoom;
	
	public EdgeId(int startRoom, int targetRoom)
	{
		this.startRoom =startRoom;
		this.targetRoom=targetRoom;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof EdgeId)
		{
			return(
				(startRoom == ((EdgeId) o).startRoom  && targetRoom == ((EdgeId) o).targetRoom) ||
				(startRoom == ((EdgeId) o).targetRoom && targetRoom == ((EdgeId) o).startRoom )
			);
		}
		
		return(false);
	}
}
