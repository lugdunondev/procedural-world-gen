package net.lugdunon.server.worldgen.procedural.biome;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.bytten.metazelda.Edge;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Coords;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;
import net.lugdunon.server.worldgen.procedural.item.placement.metazelda.MetazeldaDungeonPlacementStrategy;
import net.lugdunon.server.worldgen.procedural.noise.INoiseGen;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.MetazeldaDungeon;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.BaseDungeonGeneratorWrapper;
import net.lugdunon.server.worldgen.procedural.room.EdgeId;
import net.lugdunon.state.State;
import net.lugdunon.state.World;
import net.lugdunon.state.character.NonPlayerCharacter;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.instance.Instance;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetazeldaDungeonBiome extends Biome
{
	protected Logger logger;
	
	private long              timeoutInterval;
	private long              accumulatedInterval;
	
	private boolean           needsInit;
	
	private Set<Long                               > openedDoors;
	private Map<Integer,List<PlaceableItemInstance>> roomsToLightsources;
	
	public MetazeldaDungeonBiome(JSONObject o)
    {
	    super(o);
	    
		//RESET
		needsInit          =true;
		accumulatedInterval=0;
	    
	    logger             =LoggerFactory.getLogger(MetazeldaDungeonBiome.class);
    	timeoutInterval    =State.instance().getWorld().getWorldConfigProperty("dungeon.timeout.interval", World.DEFAULT_DUNGEON_TIMEOUT_INTERVAL);

    	openedDoors        =new TreeSet<Long                               >();
    	roomsToLightsources=new HashMap<Integer,List<PlaceableItemInstance>>();
    }
	
	/////

	@Override
	public void init(Terrain terrain, PlaceableItemCache placeableItemCache)
	{
		
	}

	@Override
	public void init(Instance instance)
	{
		//GRAB dungeonGeneratorWrapper
		if(!instance.hasRuntimeProperty("dungeonGeneratorWrapper"))
		{
			try
			{
				JSONObject noiseDef=instance.getInstanceDefinition().getJSONObject("proceduralConfiguration").getJSONObject("noise");
				INoiseGen  noiseGen=(INoiseGen) MetazeldaDungeonPlacementStrategy.class.getClassLoader().loadClass(
						noiseDef.getString("class")
				).newInstance();
				
				instance.setRuntimeProperty(
					"dungeonGeneratorWrapper",
					((MetazeldaDungeon) noiseGen).generateDungeon(instance.getTerrain().getSize(),noiseDef.getJSONObject("args"))
				);
			}
			catch(Exception e)
			{
				logger.error("Metazelda dungeon generator wrapper initialization failed for instance with id: {}.",instance.getInstanceId(),e);
			}
		}
	}

	@Override
    public void update(long delta, Instance instance)
    {	
		int                         apii          =State.instance().listActiveCharactersInInstance(instance.getInstanceId()).size();
		BaseDungeonGeneratorWrapper dgw           =(BaseDungeonGeneratorWrapper) instance.getRuntimeProperty("dungeonGeneratorWrapper");
		List<EdgeId>                completedEdges=new ArrayList<EdgeId>();

		//REPOPULATE INSTANCE
		if(needsInit)
		{
			Rect   r;
			Coords c;
			Coords d=new Coords(
				dgw.getDungeon().getExtentBounds().left,
				dgw.getDungeon().getExtentBounds().top
			);
			int    b=dgw.getWallThickness()/2;
			int    p=dgw.getDoorWidth    ()/2;
			
			//FIRST, CLEAN UP INSTANCE
			cleanInstance(instance);

			//PLACE ITEMS
			for(Room room:dgw.getDungeon().getRooms())
			{
				c=room.getCoords().iterator().next().subtract(d);
				r=new Rect(
					c.x*dgw.getRoomWidth (),
					c.y*dgw.getRoomHeight(),
					    dgw.getRoomWidth (),
					    dgw.getRoomHeight()
				);
				
				dgw.getRoomGenerator().generateItemsForRoom(
					instance,
					dgw.getDungeon(),
					room,
					r,
					b,
					p
				);
				
				//PLACE DOORS
				for(Edge edge:room.getEdges())
		    	{
					EdgeId edgeId=new EdgeId(room.id,edge.getTargetRoomId());
					
					//FILTER OUT DUPLICATES
					if(!completedEdges.contains(edgeId))
					{
						completedEdges.add(edgeId);
			    		dgw.getRoomGenerator().generateDoorForConnector(
							instance,
		    				dgw.getDungeon(), 
		    				room, 
		    				r, 
		    				b, 
		    				p, 
							room.getCoords().iterator().next().getDirectionTo(
								dgw.getDungeon().get(
									edge.getTargetRoomId()
								).getCoords().iterator().next()
							),
							edge
						);
					}
		    	}
				
				//SPAWN NPCS
				dgw.getRoomGenerator().generateNPCsForRoom(
					instance,
					dgw.getDungeon(),
					room,
					r,
					b,
					p
				);
			}
			
			//SET UP DOOR AND LIGHTING STATES
			Collection<PlaceableItemInstance> piis=instance.getPlaceableItemCache().list();
			
			for(PlaceableItemInstance pii:piis)
			{
				if(pii.getUserDefinedData() != null)
				{
					Room room;
					
					try
					{
						if(isLightsource(pii))
						{
							room=getRoom(dgw, pii, "net.lugdunon.server.worldgen.procedural.room.id");
							
							if(!room.isStart())
							{
								pii.setLightingEnabled(false);
								
								if(!roomsToLightsources.containsKey(room.id))
								{
									roomsToLightsources.put(room.id, new ArrayList<PlaceableItemInstance>());
								}
								
								roomsToLightsources.get(room.id).add(pii);
							}
						}
					}
					catch(Exception e)
					{
						;
					}
				}
			}
			
			//FORCE IMPASSABLE UPDATE
			instance.getTerrain().recomputeImpassabilityFlags();

			logger.info(
				"Dungeon instance with id: {} has been repopulated with {} items and {} npcs.",
				new Object[]
				{
					instance.getInstanceId(),
					instance.getPlaceableItemCache().list().size(),
					instance.getNpcCache          ().list().size()
				}
			);
			
			needsInit=false;
		}
		
		//EMPTY
		if(apii == 0)
		{
			accumulatedInterval+=delta;
			
			//RESET AFTER 15 MINS INACTIVE
			if(accumulatedInterval > timeoutInterval)
			{
				//FIRST, CLEAN UP INSTANCE
				cleanInstance(instance);
				
				//DEHYDRATE INSTANCE
				State.instance().getWorld().closeInstance(instance.getInstanceId());
				
				logger.info("Dungeon instance with id: {} has been reset.",instance.getInstanceId());
			}
		}
		//OCCUPIED
		else
		{
			Collection<PlaceableItemInstance> piis=instance.getPlaceableItemCache().list();
			
			for(PlaceableItemInstance pii:piis)
			{
				Room                        room;
				List<PlaceableItemInstance> lightsources;
				
				try
				{
					//CHECK IF LIGHT STATE CHANGE NEEDED
					if(
						(pii.getUserDefinedData() != null) && 
						isDoor(pii) && 
						pii.getCurrentState().equals("opened") && 
						!openedDoors.contains(pii.getItemInstanceId())
					)
					{
						openedDoors.add(pii.getItemInstanceId());
						
						//SOURCE
						room        =getRoom(dgw,pii,"net.lugdunon.server.worldgen.procedural.source.room.id");
						lightsources=roomsToLightsources.get(room.id);

						if(lightsources != null && lightsources.size() > 0 && !lightsources.get(0).isLightingEnabled())
						{
							for(PlaceableItemInstance lightsource:lightsources)
							{
								lightsource.setLightingEnabled(true);
							}
						}
						
						//TARGET
						room        =getRoom(dgw,pii,"net.lugdunon.server.worldgen.procedural.target.room.id");
						lightsources=roomsToLightsources.get(room.id);
						
						if(lightsources != null && lightsources.size() > 0 && !lightsources.get(0).isLightingEnabled())
						{
							for(PlaceableItemInstance lightsource:lightsources)
							{
								lightsource.setLightingEnabled(true);
							}
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			accumulatedInterval=0;
		}
    }

	private Room getRoom(BaseDungeonGeneratorWrapper dgw, PlaceableItemInstance pii, String roomIdKey) throws JSONException
    {
	    return(
	    	dgw.getDungeon().get(
	    		pii.getUserDefinedData().getInt(
	    			roomIdKey
	    		)
	    	)
	    );
    }

	private boolean isLightsource(PlaceableItemInstance pii) throws JSONException
    {
	    return(
	    	pii.getUserDefinedData().has       ("net.lugdunon.server.worldgen.procedural.room.lightsource") && 
	    	pii.getUserDefinedData().getBoolean("net.lugdunon.server.worldgen.procedural.room.lightsource")
	    );
    }

	private boolean isDoor(PlaceableItemInstance pii) throws JSONException
    {
	    return(
	    	pii.getUserDefinedData().has       ("net.lugdunon.server.worldgen.procedural.room.door") && 
	    	pii.getUserDefinedData().getBoolean("net.lugdunon.server.worldgen.procedural.room.door")
	    );
    }

	private void cleanInstance(Instance instance)
    {
	    //CLEAR INSTANCE OF NPCS
		int    i  =0;
		long[] ids=new long[instance.getNpcCache().list().size()];
		
	    for(NonPlayerCharacter npc:instance.getNpcCache().list())
	    {
	    	ids[i++]=npc.getNpcId();	
	    }
	    
	    for(long id:ids)
	    {
	    	instance.getNpcCache().deleteNpc(id);
	    }

	    //CLEAR INSTANCE OF ITEMS
		i  =0;
		ids=new long[instance.getPlaceableItemCache().list().size()-1];
	    
	    for(PlaceableItemInstance pii:instance.getPlaceableItemCache().list())
	    {
	    	//DON'T REMOVE EXIT PORTAL
	    	if(pii.getItemInstanceId() != ProceduralWorldGen.EXIT_PORTAL_ID)
	    	{
		    	ids[i++]=pii.getItemInstanceId();
	    		instance.getPlaceableItemCache().deletePlaceableItemReference(pii.getItemInstanceId(), false);
	    	}
	    }
	    
	    for(long id:ids)
	    {
	    	instance.getPlaceableItemCache().deletePlaceableItemReference(id, false);
	    }
	    
	    openedDoors        .clear();
	    roomsToLightsources.clear();
    }

	@Override
    public Item getNextItemForPlacement(boolean postGen)
    {
	    // DO NOTHING
    	return(null);
    }

	@Override
    public void itemPlacementCanceled(boolean postGen)
    {
	    
    }
}
