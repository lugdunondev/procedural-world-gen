Namespace.declare("net.lugdunon.server.worldgen.procedural.instance.tile");
Namespace.require ("net.lugdunon.ui.DoubleSlider");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog");

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.getHeaderText=function()
{
	return("Save Tile");
};

////

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.init=function(initData)
{
	this.content          =null;
	this.shown            =false;
	
	this.settingsDetail   =null;
	this.settingsButtons  =null;
	
	this.cursorTile       =null;
	this.selectedTile     =null;
	
	this.selectedTileIndex=0;
	
	this.values           =[
	    this.getDefaultValues(),
	    this.getDefaultValues(),
	    this.getDefaultValues(),
	    this.getDefaultValues()
	];
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.getValues=function()
{
	return(this.values[this.selectedTileIndex]);
}

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.getDefaultValues=function()
{
	return(
		{
			name            :"",
			difficultyRange :[0.0,1.0],
			isTileMirrorable:false,
			doorPuzzle      :[]
		}
	);
}

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.selectedTileChanged=function(tileIndex)
{
	this.selectedTileIndex=tileIndex;
	
	this.nameInput         .val        (this.getValues().name              );
	this.isMirrorableToggle.setValue   (this.getValues().isTileMirrorable  );
	this.tdSlider          .setMinValue(this.getValues().difficultyRange[0]);
	this.tdSlider          .setMaxValue(this.getValues().difficultyRange[1]);
}

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.toggleValueChanged=function(toggle)
{
	this.getValues().isTileMirrorable=toggle.getValue();
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.sliderValueChanged=function(slider)
{
	this.tdMinInput.val(slider.getMinValue().toFixed(2));
	this.tdMaxInput.val(slider.getMaxValue().toFixed(2));

	this.getValues().difficultyRange[0]=parseFloat(slider.getMinValue().toFixed(2));
	this.getValues().difficultyRange[1]=parseFloat(slider.getMaxValue().toFixed(2));
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.renderContent=function()
{
	var i;

	this.content.html(Namespace.requireHTML(this.classId));

	this.settingsDetail =this.content.find(".settingsDetail" );
	this.settingsButtons=this.content.find(".settingsButtons");

	this.settingsDetail .css("width",this.content.width()+"px");
	this.settingsButtons.css("width",this.content.width()+"px");
	
	//BUTTONS
	this.settingsButtons.append(
		"<div class='ccButton'><button name='save'>Save</button></div>"
	);
	this.settingsButtons.find("button[name=save]").on(
		"click",
		{context:this},
		function(e)
		{
			e.data.context.handleSave.call(e.data.context);
		}
	);
	
	this.settingsButtons.append(
		"<div class='ccButton' style='float:right'><button name='close'>Close</button></div>"
	);
	this.settingsButtons.find("button[name=close]").on(
		"click",
		{context:this},
		function(e)
		{
			e.data.context.handleClose.call(e.data.context);
		}
	);
	
	////
	
	this.nameInput         =this.content.find("#tileName");
	
	this.nameInput.on(
		"change",
		{context:this},
		function(e)
		{
			e.data.context.getValues().name=e.data.context.nameInput.val();
		}
	);
	
	this.isMirrorableToggle=new net.lugdunon.ui.Toggle().init(
		{
			changeData    :0,
			parent        :this.content.find("#isTileMirrorable"),
			value         :this.getValues().isTileMirrorable,
			changeDelegate:this
		}
	);

	this.tdMinInput =this.content.find("#tileMinDiff");
	this.tdMaxInput =this.content.find("#tileMaxDiff");

	this.tdMinInput.val(this.getValues().difficultyRange[0].toFixed(2));
	this.tdMaxInput.val(this.getValues().difficultyRange[1].toFixed(2));
	
	this.tdSlider=new net.lugdunon.ui.DoubleSlider().init(
		{
			parent        :this.content.find("#tileDiffSlider"),
			minValue      :this.getValues().difficultyRange[0],
			maxValue      :this.getValues().difficultyRange[1],
			width         :186,
			changeDelegate:this,
			minRange      :0.1
		}
	);
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.handleClose=function()
{
	;
	
	this.hide();
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.handleSave=function()
{
	;
};

////

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.update=function(delta)
{

};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.draw=function(delta)
{

};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.hide=function()
{
	this.shown               =false;
	
	net.lugdunon.ui.Dialog.close(this.content.data("dialogId"));
	
	this.content             =null;
	
	if(game.getCurrentGameState() && game.getCurrentGameState().setCurrentDialog)
	{
		game.getCurrentGameState().setCurrentDialog(null);
	}
};

net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.show=function(settingIndex)
{
	var context=this;
	
	game.console.exec("/warp 16,16");
	
	if(game.getCurrentGameState() && game.getCurrentGameState().setCurrentDialog)
	{
		game.getCurrentGameState().setCurrentDialog(this);
	}
	
	this.content       =net.lugdunon.ui.Dialog.largeDialog(this.getHeaderText(),900,Namespace.isMobile()?436:532);
	this.renderContent();
	this.shown         =true;

	$("#tileSelector").css("background-image","url("+game.minimap.canvas.toDataURL()+")");
	
	this.cursorTile     =$("#cursorTile"  );
	this.selectedTile   =$("#selectedTile");

	this.selectedTile.html  ("0");
	this.selectedTileChanged( 0 );
	
	// 0 2
	// 1 3
	
	$("#tileSelector").on(
		"mousemove",
		function(e)
		{
			var x=Math.floor(e.offsetX/180);
			var y=Math.floor(e.offsetY/180);
			
			context.cursorTile.css("left",(x*180)+"px");
			context.cursorTile.css("top", (y*180)+"px");
			
			     if(x == 0 && y == 0)
			{
				context.cursorTile.html("0");
			}
			else if(x == 0 && y == 1)
			{
				context.cursorTile.html("1");
			}
			else if(x == 1 && y == 0)
			{
				context.cursorTile.html("2");
			}
			else if(x == 1 && y == 1)
			{
				context.cursorTile.html("3");
			}
		}
	);

	$("#tileSelector").on(
		"mouseover",
		function(e)
		{
			context.cursorTile.css("display","block");
		}
	);

	$("#tileSelector").on(
		"mouseout",
		function(e)
		{
			context.cursorTile.css("display","none");
		}
	);

	$("#tileSelector").on(
		"mousedown",
		function(e)
		{
			var x=Math.floor(e.offsetX/180);
			var y=Math.floor(e.offsetY/180);
			
			context.selectedTile.css("left",(x*180)+"px");
			context.selectedTile.css("top", (y*180)+"px");
			
		         if(x == 0 && y == 0)
			{
				context.selectedTile.html  ("0");
				context.selectedTileChanged( 0 );
			}
			else if(x == 0 && y == 1)
			{
				context.selectedTile.html  ("1");
				context.selectedTileChanged( 1 );
			}
			else if(x == 1 && y == 0)
			{
				context.selectedTile.html  ("2");
				context.selectedTileChanged( 2 );
			}
			else if(x == 1 && y == 1)
			{
				context.selectedTile.html  ("3");
				context.selectedTileChanged( 3 );
			}
		}
	);
};


net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog.prototype.tileSaveReturned=function(successful)
{
	if(successful)
	{
		net.lugdunon.ui.Dialog.ok(
			"Success",
			"Tile saved/updated successfully."
		);
	}
	else
	{
		net.lugdunon.ui.Dialog.ok(
			"Error",
			"Tile save/update failed."
		);
	}
};