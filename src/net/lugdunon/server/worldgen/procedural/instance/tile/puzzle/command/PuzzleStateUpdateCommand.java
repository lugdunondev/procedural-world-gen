package net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command;

import java.io.IOException;
import java.util.List;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.state.State;
import net.lugdunon.state.character.PlayerCharacter;

public class PuzzleStateUpdateCommand extends Command implements IServerInvokedCommand
{
	public static final int PUZZLE_STATE_MODE_INIT    =0;
	public static final int PUZZLE_STATE_MODE_DESTRUCT=1;
	public static final int PUZZLE_STATE_MODE_UPDATED =2;
	
	protected Response iRes;

	////
	
	@Override
	public String getCommandId()
	{
	    return("PROCEDURAL.PUZZLE.STATE.UPDATE");
	}

	@Override
	public String getName()
	{
		return("Puzzle State Update");
	}

	@Override
	public String getDescription()
	{
		return("Allows puzzle state updates to include the client.");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
	public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		Response        oRes;
		int             mode      =props.getInt ("mode"      );
		long            instanceId=props.getLong("instanceId");

		iRes=initializeInternalResponse();

		iRes.out.write     (mode                          );
		iRes.out.writeLong (instanceId                    );
		iRes.out.writeShort(props.getShort ("roomId"     ));
		iRes.out.writeUTF  (props.getString("puzzleId"   ));
		
		switch(mode)
		{
			case PUZZLE_STATE_MODE_INIT:
			{
				iRes.out.writeUTF(props.getString("uiComponent"));
				
				break;
			}
			case PUZZLE_STATE_MODE_DESTRUCT:
			{
				break;
			}
			case PUZZLE_STATE_MODE_UPDATED:
			{
				iRes.out.writeJSONObject(props.getJSONObject("state"));
				break;
			}
		}
		
		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		if(props.contains("actor"))
		{
			PlayerCharacter actor=props.getPlayerCharacter("actor");
			
			actor.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
		}
		else
		{
			List<PlayerCharacter> pcs=State.instance().listActiveCharactersInInstance(instanceId);
			
			for(PlayerCharacter pc:pcs)
			{
				pc.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
			}
		}
	}

	@Override
	public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		return;
	}
}