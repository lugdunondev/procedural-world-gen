Namespace.declare("net.lugdunon.server.worldgen.procedural.instance.tile");
Namespace.require("net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.instance.tile.DungeonStarterTileInstance","net.lugdunon.states.tiledGame.instance.Instance");

////

net.lugdunon.server.worldgen.procedural.instance.tile.DungeonStarterTileInstance.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.instance.tile.DungeonStarterTileInstance,"init",[initData])
	
	this.dialog;
	
	return(this);
}

net.lugdunon.server.worldgen.procedural.instance.tile.DungeonStarterTileInstance.prototype.destruct=function()
{
	;
}

net.lugdunon.server.worldgen.procedural.instance.tile.DungeonStarterTileInstance.prototype.showSaveDungeonTileDialog=function()
{
	if(this.dialog == null)
	{
		this.dialog=new net.lugdunon.server.worldgen.procedural.instance.tile.TileSaverDialog().init({});
	}
	
	this.dialog.show();
}