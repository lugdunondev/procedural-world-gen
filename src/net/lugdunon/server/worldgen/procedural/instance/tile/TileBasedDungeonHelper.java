package net.lugdunon.server.worldgen.procedural.instance.tile;

import java.io.File;
import java.io.FilenameFilter;

import net.lugdunon.command.core.edit.DumpScreenRegionCommand;
import net.lugdunon.math.Dimension;
import net.lugdunon.math.Point;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.state.State;
import net.lugdunon.state.item.Item;
import net.lugdunon.util.FastMath;
import net.lugdunon.util.FileUtils;
import net.lugdunon.world.instance.Instance;

import org.json.JSONArray;
import org.json.JSONObject;

public class TileBasedDungeonHelper
{
	public static final String TILE_PATH="/net/lugdunon/server/worldgen/procedural/tiles/";

	public static File[] getRoomDefs()
	{
		return(
			FileUtils.listFilesIn(
				WorldGenerator.getEtcPath()+TILE_PATH,
				new FilenameFilter()
				{
					@Override
					public boolean accept(File dir, String name)
					{
						return(name.endsWith(".json"));
					}
				}
			)
		);
	}
	
	public static int getNextAvailableTileIndex()
	{
		int    nextTileIndex=-1;
		File[] tiles        =getRoomDefs();
		
		for(File tile:tiles)
		{
			try
			{
				int tileIndex=Integer.parseInt(tile.getName().substring(0, tile.getName().length()-5));
				
				if(tileIndex > nextTileIndex)
				{
					nextTileIndex=tileIndex;
				}
			}
			catch(Exception e)
			{
				;
			}
		}
		
		return(nextTileIndex+1);
	}
	
	public static boolean saveTile(int index, Instance instance)
	{
		return(saveTile(index,getNextAvailableTileIndex(),instance));
	}
	
	public static boolean saveTile(int index, int tileIndex, Instance instance)
	{
		boolean success=false;
		Point   coords =indexToCoords(index, instance.getTerrain().getSize());
		
		if(coords != null)
		{
			try
            {
				//TODO: ADDITIONAL METADATA (EXITS / DIFFICULTY / PUZZLE AREAS / ETC)
				
				JSONObject tileData       =DumpScreenRegionCommand.dumpScreenRegion(new Rect(coords,new Dimension(32,32)), instance, false);
				JSONArray  placeables     =tileData.getJSONArray("placeables");
				JSONObject dungeonMetaData=new JSONObject();
				JSONArray  doors          =new JSONArray ();
				
				//SET UP DUNGEON METADATA
				tileData       .put("dungeonMetaData",dungeonMetaData);
				dungeonMetaData.put("doors",          doors          );
				
				//STRIP ORIGINATING DATA
				tileData.remove("originatingData");
				
				for(int i=0;i<placeables.length();i++)
				{
					JSONObject placeable=placeables.getJSONObject(i);
					
					//STARTER TILE
					if("PORTAL.LADDER.WOOD".equals(placeable.getString("itemDef")))
					{
						Item  portalItem =State.instance().getWorld().getItemDefinitions().getItemDef(placeable.getString("itemDef"));
						Point portalLoc  =new Point(placeable.getJSONObject("location"));
						Point arrivalTile=new Point(portalItem.getStateDefinition("main").getJSONObject("instanceTrigger").getJSONObject("arrivalTile"));
						
						placeable      .put("userDefinedData",new JSONObject());
						dungeonMetaData.put("starterTile",    true);
						dungeonMetaData.put("spawn",          FastMath.wrap(portalLoc.add(arrivalTile),instance.getTerrain().getSize()).toJSONObject());
					}
					//REMOVE & REGISTER EXITS
					else if(
							"DUNGEON.GATE.EAST.WEST"  .equals(placeable.getString("itemDef")) ||
							"DUNGEON.GATE.NORTH.SOUTH".equals(placeable.getString("itemDef"))
					)
					{
						Point location=new Point(placeable.getJSONObject("location"));
						
						//NORTH
						if(location.equals(13, -2))
						{
							doors.put(0);
						}
						//SOUTH
						else if(location.equals(13, 30))
						{
							doors.put(1);
						}
						//EAST
						else if(location.equals(29, 13))
						{
							doors.put(2);
						}
						//WEST
						else if(location.equals(-3, 13))
						{
							doors.put(3);
						}

						placeables.remove(i--);
					}
				}
				
				success=FileUtils.writeJSON(
	            	WorldGenerator.getEtcPath()+TILE_PATH+tileIndex+".json",
	            	tileData,
	            	true
	            );
            }
            catch (Exception e)
            {
	            e.printStackTrace();
            }
		}
		
		return(success);
	}
	
	private static Point indexToCoords(int index, int size)
	{
		Point coords=FastMath.indexToPoint(index, size/32).mult((short) 32);
		
		if(coords.x < size && coords.y < size)
		{
			return(coords);
		}
		
		return(null);
	}
}