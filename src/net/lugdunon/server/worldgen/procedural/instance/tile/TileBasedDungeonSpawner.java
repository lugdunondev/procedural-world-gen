package net.lugdunon.server.worldgen.procedural.instance.tile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import net.bytten.metazelda.Edge;
import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.Symbol;
import net.bytten.metazelda.generators.DungeonGenerator;
import net.bytten.metazelda.util.Coords;
import net.bytten.metazelda.util.Direction;
import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.link.Door;
import net.lugdunon.server.worldgen.procedural.instance.tile.room.TileBasedRoomSpawner;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.constraints.PlanarConstraints;
import net.lugdunon.server.worldgen.procedural.room.EdgeId;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.util.FastMath;
import net.lugdunon.util.FileUtils;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.instance.TransientInstance;
import net.lugdunon.world.instance.TransientInstanceCache;
import net.lugdunon.world.instance.spawner.DefaultInstanceSpawner;
import net.lugdunon.world.instance.spawner.ex.InstanceSpawnException;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TileBasedDungeonSpawner extends DefaultInstanceSpawner
{
	private static final Logger LOGGER=LoggerFactory.getLogger(TileBasedDungeonSpawner.class);
	
	private List<TileBasedRoomSpawner > rooms;

	private Set <String        > usedRooms;
	private Set <String        > externalPuzzleRooms;
	private Set <String        > usedExternalPuzzleRooms;
	private Set <String        > starterRooms;
	private List<EdgeId        > completedEdges;
	
	private Random              random;
	
	public TileBasedDungeonSpawner()
    {
	    File[] roomFiles=TileBasedDungeonHelper.getRoomDefs();
	    
	    rooms                  =new ArrayList<TileBasedRoomSpawner>();

		usedRooms              =new TreeSet  <String       >();
		usedExternalPuzzleRooms=new TreeSet  <String       >();
		externalPuzzleRooms    =new TreeSet  <String       >();
		starterRooms           =new TreeSet  <String       >();
		completedEdges         =new ArrayList<EdgeId       >();
	    
	    for(File roomFile:roomFiles)
	    {
	    	try
            {
	            JSONObject    roomDef=FileUtils.getJSONObject(new FileInputStream(roomFile));
	            TileBasedRoomSpawner room   =new TileBasedRoomSpawner(roomFile.getName(),roomDef);
	            
	            rooms.add(room);
            }
            catch (FileNotFoundException e)
            {
	            e.printStackTrace();
            }
	    }
    }
	
	////
	
	
	
	////
	
	@Override
    public TransientInstance spawnInstance(TransientInstanceCache instanceCache, long instanceId, String templateId) throws InstanceSpawnException
    {
		if(instanceExists(instanceId))
		{
			//TODO: NEED TO GET DUNGEON OR SOMETHING
			
	        try
            {
	            return(instanceCache.getInstance(instanceId));
            }
            catch (WorldIdNotSetException e)
            {
	            throw new InstanceSpawnException(e);
            }
		}
		else if(initInstance(instanceCache, instanceId, templateId))
		{
			try
			{
				PlaceableItemCache pic;
				Terrain            terrain;
				IDungeon           mzDungeon;
				JSONObject         instanceDef=getInstanceDefinition(instanceId);
				long               seed       =FastMath.seedToLong(instanceDef.has("seed")?instanceDef.get("seed"):null);
				int                dimension  =instanceDef.getInt("dimension");
				int                roomSize   =instanceDef.getInt("roomSize");
				int                dungeonSize=dimension/roomSize;
				
		        WorldGenerator.setNoState(true);
		        
		        random   =new Random(seed);
		        terrain  =generateEmptyTerrain(instanceId,seed,instanceDef);
		        pic      =new PlaceableItemCache(terrain);
		        mzDungeon=getDungeon(seed,dungeonSize,instanceDef);
				
				buildDungeon(instanceDef,terrain,pic,random,mzDungeon,dungeonSize);
		       
				//SAVE TERRAIN, PLACEABLE ITEM CACHE, AND INSTANCE
				{
					terrain.saveTerrainData(new File(FileUtils.getActualPath(getInstancePath(instanceId,"terrain.dat"))));
					
					FileUtils.writeJSON(
						getInstancePath(instanceId,"placeableItemCache.json"),
						pic.toJSONObject(),
						true
					);
					
					FileUtils.writeJSON(
						getInstancePath(instanceId,"instance.json"),
						instanceDef,
						true
					);
				}
		        
		        WorldGenerator.setNoState(false);
		        WorldGenerator.setTerrain(null );
		        
		        return(instanceCache.getInstance(instanceId));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
		        WorldGenerator.setNoState(false);
		        WorldGenerator.setTerrain(null );
			}
		}
		
		throw new InstanceSpawnException();
    }

	private void buildDungeon(JSONObject instanceDef, Terrain terrain, PlaceableItemCache pic, Random random, IDungeon dungeon, int dungeonSize)
    {
		Point origin;
		
		usedRooms          .clear();
		externalPuzzleRooms.clear();
		starterRooms       .clear();
		
		for(TileBasedRoomSpawner room:rooms)
		{
			if(room.isStarterTile())
			{
				starterRooms.add(room.getId());
			}
			
			//PUZZLE ROOMS
			if(room.isExternalPuzzle())
			{
				externalPuzzleRooms.add(room.getId());
			}
		}
		
		for(Room room:dungeon.getRooms())
		{
			//ENTRANCE
			if(room.isStart())
			{
				origin=findStarterTile  ().createRoom(instanceDef,terrain,pic,random,room,dungeon,true );
			}
			//DOOR PUZZLE ROOM
			else if(room.getItem() != null && isKey(room.getItem(),dungeon))
			{
				//LINK EXTERNAL PUZZLE TO DOOR
				try
                {
	                getDoorPuzzleRoom().getDungeonMetadata().put("externalPuzzleLink", getLockedDoor(room.getItem(),dungeon).toExternalPuzzleLink());
                }
                catch (JSONException e)
                {
	                e.printStackTrace();
                }
				
				origin=getDoorPuzzleRoom().createRoom(instanceDef,terrain,pic,random,room,dungeon,false);

			}
			else
			{
				origin=getNextRoom      ().createRoom(instanceDef,terrain,pic,random,room,dungeon,false);
			}
			
			//HANDLE DOORS
			{
				JSONArray  doors=new JSONArray();
				JSONObject door;
				
				//PLACE DOORS
				for(Edge edge:room.getEdges())
		    	{
					EdgeId edgeId=new EdgeId(room.id,edge.getTargetRoomId());
					
					//FILTER OUT DUPLICATES
					if(!completedEdges.contains(edgeId))
					{
						completedEdges.add(edgeId);
						door=generateDoorForConnector(
							terrain,
							dungeon, 
		    				room, 
							room.getCoords().iterator().next().getDirectionTo(
								dungeon.get(
									edge.getTargetRoomId()
								).getCoords().iterator().next()
							),
							edge,
							origin
						);
						
						if(door != null)
						{
							doors.put(door);
						}
					}
		    	}
				
				try
                {
	                pic.processPlaceables(doors);
                }
                catch (JSONException e)
                {
	                e.printStackTrace();
                }
			}
		}
    }

	private boolean isKey(Symbol item, IDungeon dungeon)
    {
		if(item != null)
		{
			for(Room room:dungeon.getRooms())
			{
				for(Edge edge:room.getEdges())
		    	{
					if(item.equals(edge.getSymbol()))
					{
						return(true);
					}
		    	}
			}
		}
		
		return(false);
    }

	private Door getLockedDoor(Symbol item, IDungeon dungeon)
    {
		if(item != null)
		{
			for(Room room:dungeon.getRooms())
			{
				for(Edge edge:room.getEdges())
		    	{
					if(item.equals(edge.getSymbol()))
					{
						return(
							new Door(
								room,
								room.getCoords().iterator().next().getDirectionTo(
									dungeon.get(
										edge.getTargetRoomId()
									).getCoords().iterator().next()
								)
							)
						);
					}
		    	}
			}
		}
		
		return(null);
    }

	private JSONObject generateDoorForConnector(Terrain terrain, IDungeon dungeon, Room room, Direction directionTo, Edge edge, Point roomOffset)
    {
		JSONObject door         =null;
		String     doorItemDef  =null;
		Direction  directionFrom=Direction.oppositeDirection(directionTo);
		Point      location     =new Point();
		
	         if(directionTo.equals(Direction.N) || directionTo.equals(Direction.S))
	    {
        	doorItemDef="DUNGEON.GATE.NORTH.SOUTH"+(edge.hasSymbol()?".LOCKED":"");
        	location   .set(
        		13,
        		directionTo.equals(Direction.N)?-2:30
        	);
	    }
	    else if(directionTo.equals(Direction.E) || directionTo.equals(Direction.W))
	    {
	    	doorItemDef="DUNGEON.GATE.EAST.WEST"+(edge.hasSymbol()?".LOCKED":"");
        	location   .set(
            	directionTo.equals(Direction.W)?-3:29,
        		13
        	);
	    }
	          
	    if(doorItemDef != null)
	    {
	    	door    =new JSONObject();
		    location=FastMath.wrap(location.add(roomOffset), terrain.getSize());

//	        "userDefinedData": {"statePrefix": ""},
//	        "currentDurability": 0,
//	        "location": {
//	            "y": 45,
//	            "x": 61
//	        },
//	        "currentRespawnTime": 0,
//	        "currentState": "opened",
//	        "itemInstanceId": 94,
//	        "maxRespawnTime": 0,
//	        "itemDef": "DUNGEON.GATE.EAST.WEST",
//	        "placer": "",
//	        "maxDurability": 0

			try
			{
				door.put("itemDef",        doorItemDef            );
				door.put("location",       location.toJSONObject());
				door.put("currentState",   "closed"               );
				door.put("placer",         ""                     );
				door.put("userDefinedData",new JSONObject()       );
				
				door.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.room.door",       true);
				door.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.room.id",         room.id);
				door.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.target.room.id",  edge.getTargetRoomId());
				door.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.direction",       directionTo  .code);
				door.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.target.direction",directionFrom.code);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
	    }
	    
	    return(door);
    }
	
	private TileBasedRoomSpawner getDoorPuzzleRoom()
    {
		TileBasedRoomSpawner nextRoom=null;

		while(nextRoom == null)
		{
			nextRoom=rooms.get(random.nextInt(rooms.size()));
			
			if(!nextRoom.isExternalPuzzle())
			{
				nextRoom=null;
			}
		}

		usedRooms              .add(nextRoom.getId());
		usedExternalPuzzleRooms.add(nextRoom.getId());
		
		//CHECK IF ALL ROOMS ARE USED
		if(rooms.size() <= (usedRooms.size()+starterRooms.size()))
		{
			usedRooms.clear();
		}
		
		//CHECK IF ALL EXTERNAL PUZZLE ROOMS ARE USED
		if(usedExternalPuzzleRooms.size() >= externalPuzzleRooms.size())
		{
			usedExternalPuzzleRooms.clear();
		}
		
	    return(nextRoom);
    }

	private TileBasedRoomSpawner getNextRoom()
    {
		TileBasedRoomSpawner nextRoom=null;

		while(nextRoom == null)
		{
			nextRoom=rooms.get(random.nextInt(rooms.size()));
			
			if(nextRoom.isStarterTile() || usedRooms.contains(nextRoom.getId()))
			{
				nextRoom=null;
			}
		}
		
		usedRooms.add(nextRoom.getId());
		
		//CHECK IF ALL ROOMS ARE USED
		if(rooms.size() <= (usedRooms.size()+starterRooms.size()))
		{
			usedRooms.clear();
		}
		
		if(nextRoom.isExternalPuzzle())
		{
			usedExternalPuzzleRooms.add(nextRoom.getId());
			
			//CHECK IF ALL EXTERNAL PUZZLE ROOMS ARE USED
			if(usedExternalPuzzleRooms.size() >= externalPuzzleRooms.size())
			{
				usedExternalPuzzleRooms.clear();
			}
		}
		
	    return(nextRoom);
    }

	private TileBasedRoomSpawner findStarterTile()
    {
		List<TileBasedRoomSpawner> starterRooms=new ArrayList<TileBasedRoomSpawner>();
		
		for(TileBasedRoomSpawner room:rooms)
		{
			if(room.isStarterTile())
			{
				starterRooms.add(room);
			}
		}
		
	    return(starterRooms.get(random.nextInt(starterRooms.size())));
    }

	private Terrain generateEmptyTerrain(long instanceId, long seed, JSONObject instanceConfig) throws JSONException
    {
		int             dimension    =instanceConfig.getInt("dimension");
		int             terrainSize  =dimension*dimension;
		byte            curElevation =(byte) 0x00;
		byte            nullTerrain  =(byte) 0x00;
		byte []         biomeData    =new byte [terrainSize];
		byte []         elevationData=new byte [terrainSize];
		short[]         terrainData  =new short[terrainSize];

		Arrays.fill(biomeData,     Biome.NULL_BIOME);
		Arrays.fill(elevationData, curElevation    );
		Arrays.fill(terrainData,   nullTerrain     );

		WorldGenerator.setIndependentPreset(instanceConfig.getString("terrain"));
		
		Terrain terrain=new Terrain(
			instanceId,
			terrainData,
			elevationData,
			biomeData,
			WorldGenerator.getTileset(),
			instanceConfig,
			seed
		); 

		WorldGenerator.setIndependentPreset(null);
		
		return(terrain);
    }

	private static IDungeon getDungeon(long seed, int dungeonSize, JSONObject instanceDef) throws JSONException
    {
		IDungeon          dungeon;
		JSONObject        generatorArgs=new JSONObject();
		int               maxRooms     =dungeonSize*dungeonSize;
		int               roomCount    =new Random(seed).nextInt(maxRooms/2)+(maxRooms/2);
		int               keyCount     =roomCount/7;
		PlanarConstraints pc           =new PlanarConstraints(
			dungeonSize,
			dungeonSize,
			roomCount,
			keyCount,
			0
		);
		DungeonGenerator generator     =new DungeonGenerator(
			seed,
			pc
		);
		
		generator.setGenerateGoal  (true);
		generator.setBossRoomLocked(true);
		generator.generate         (    );
		
		dungeon=generator.getDungeon();
		
		generatorArgs.put("seed",          seed             );
		generatorArgs.put("generateGoal",  true             );
		generatorArgs.put("bossRoomLocked",true             );
		generatorArgs.put("constraints",   pc.toJSONObject());
		
		instanceDef  .put("generatorArgs", generatorArgs    );
		
		LOGGER.info("Tiled Dungeon generated with a room count of "+dungeon.roomCount()+" and "+keyCount+" locked room(s).");
		
		return(dungeon);
    }

	public static Room findRoom(IDungeon dungeon, int x, int y)
    {
		Coords[] coord =new Coords[1];
		
	    for(Room room:dungeon.getRooms())
	    {
	    	room.getCoords().toArray(coord);
	    	
	    	if(coord[0].x == x && coord[0].y == y)
	    	{
	    		return(room);
	    	}
	    };
	    
	    return(null);
    }
	
//	public static void logDungeon(IDungeon dungeon)
//	{
//		Bounds b=dungeon.getExtentBounds();
//
//		for(int x=b.left;x<=b.right;x++)
//		{
//			for(int y=b.top;y<=b.bottom;y++)
//			{
//				Room r=findRoom(dungeon,x,y);
//				
//				if(r != null)
//				{
//					if(r.isBoss())
//					{
//						System.out.print("b");
//					}
//					else if(r.isGoal())
//					{
//						System.out.print("g");
//					}
//					else if(r.isStart())
//					{
//						System.out.print("s");
//					}
//					else if(r.isSwitch())
//					{
//						System.out.print("w");
//					}
//					else if(r.getItem() != null)
//					{
//						System.out.print(r.getItem());
//					}
//					else
//					{
//						System.out.print(r.getPrecond().getKeyLevel());
//					}
//				}
//				else
//				{
//					System.out.print("-");
//				}
//				
//				System.out.print(" ");
//			}
//
//			System.out.println();
//			System.out.println();
//		}
//	}
}