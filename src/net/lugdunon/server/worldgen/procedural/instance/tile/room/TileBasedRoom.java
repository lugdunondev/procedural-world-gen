package net.lugdunon.server.worldgen.procedural.instance.tile.room;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.bytten.metazelda.util.Direction;
import net.lugdunon.io.JSONObjectSerializable;
import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.BasePuzzle;
import net.lugdunon.state.State;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.instance.Instance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TileBasedRoom implements JSONObjectSerializable
{
	private JSONObject                  dungeonMetaData;

	private Point                       origin;
	
	private List<PlaceableItemInstance> placeables;
	private Map <String,BasePuzzle    > puzzles;
	
	private int                         id;
	
	private PlaceableItemInstance       northDoor;
	private PlaceableItemInstance       southDoor;
	private PlaceableItemInstance       eastDoor;
	private PlaceableItemInstance       westDoor;
	
	public TileBasedRoom(int id,JSONObject o,Instance instance, List<PlaceableItemInstance> doors)
    {
		this.id=id;
		
	    try
        {
	        fromJSONObject(o);
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
	    
	    init(instance,doors);
    }
	
	protected void init(Instance instance, List<PlaceableItemInstance> doors)
	{
		try
		{
			Direction d=null;
			
			for(PlaceableItemInstance door:doors)
			{
				     if(door.getUserDefinedData().getInt("net.lugdunon.server.worldgen.procedural.room.id") == id)
				{
					d=Direction.fromCode(door.getUserDefinedData().getInt("net.lugdunon.server.worldgen.procedural.direction"));
				}
				else if(door.getUserDefinedData().getInt("net.lugdunon.server.worldgen.procedural.target.room.id") == id)
				{
					d=Direction.fromCode(door.getUserDefinedData().getInt("net.lugdunon.server.worldgen.procedural.target.direction"));
				}
				    
				if(d != null)
				{
					     if(Direction.N.equals(d))
					{
						northDoor=door;
					}
					else if(Direction.S.equals(d))
					{
						southDoor=door;
					}
					else if(Direction.E.equals(d))
					{
						eastDoor=door;
					}
					else if(Direction.W.equals(d))
					{
						westDoor=door;
					}
					     
					d=null;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try
		{
			Collection<PlaceableItemInstance> piis=instance.getPlaceableItemCache().list();
			
			placeables=new ArrayList<PlaceableItemInstance>();
			
			for(PlaceableItemInstance pii:piis)
			{
				if(
					pii.getUserDefinedData().has("net.lugdunon.server.worldgen.procedural.room.id") &&
					(
						pii.getUserDefinedData().getInt("net.lugdunon.server.worldgen.procedural.room.id") == id
					)
			    )
				{
					placeables.add(pii);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void postInit(Instance instance)
	{
		if(puzzles != null)
		{
			for(BasePuzzle puzzle:puzzles.values())
			{
				puzzle.init(
					id,
					instance
				);
			}
		}
	}
	
	////
	
	public Point getOrigin()
	{
		return(origin);
	}
	
	public PlaceableItemInstance getDoor(int d)
	{
		return(getDoor(Direction.fromCode(d)));
	}
	
	public PlaceableItemInstance getDoor(Direction d)
	{
		if(d != null)
		{
			     if(Direction.N.equals(d))
			{
				return(getNorthDoor());
			}
			else if(Direction.S.equals(d))
			{
				return(getSouthDoor());
			}
			else if(Direction.E.equals(d))
			{
				return(getEastDoor());
			}
			else if(Direction.W.equals(d))
			{
				return(getWestDoor());
			}
		}
		
		return(null);
	}
	
	public PlaceableItemInstance getNorthDoor()
	{
		return(northDoor);
	}
	
	public PlaceableItemInstance getSouthDoor()
	{
		return(southDoor);
	}
	
	public PlaceableItemInstance getEastDoor()
	{
		return(eastDoor);
	}
	
	public PlaceableItemInstance getWestDoor()
	{
		return(westDoor);
	}
	
	////
	
	public int getId()
	{
		return(id);
	}
	
	public JSONObject getMetadata()
	{
		return(dungeonMetaData);
	}
	
	////
	
	public void playerEntered(PlayerCharacter pc)
	{
//		System.out.println("playerEntered: "+getId());
		
		if(puzzles != null)
		{
			for(BasePuzzle puzzle:puzzles.values())
			{
				puzzle.playerEnteredRoom(pc);
			}
		}
	}
	
	public void playerLeft(PlayerCharacter pc)
	{
//		System.out.println("playerLeft   : "+getId());
		
		if(puzzles != null)
		{
			for(BasePuzzle puzzle:puzzles.values())
			{
				puzzle.playerLeftRoom(pc);
			}
		}
	}
	
	////
	
	public void update(long delta)
	{
		if(puzzles != null)
		{
			for(BasePuzzle puzzle:puzzles.values())
			{
				puzzle.update(delta);
			}
		}
	}
	
	////

	@Override
    public void fromJSONObject(JSONObject o) throws JSONException
    {
		dungeonMetaData=o;
		
		try
		{
			origin=new Point(dungeonMetaData.getJSONObject("origin"));
			
			if(dungeonMetaData.has("puzzles"))
			{
				JSONArray puzzlesData=dungeonMetaData.getJSONArray("puzzles");
				
				if(puzzlesData.length() > 0)
				{
					puzzles=new HashMap<String,BasePuzzle>();
					
					for(int i=0;i<puzzlesData.length();i++)
					{
						try
						{
							BasePuzzle puzzle=(BasePuzzle) State.makeObjectFromJSONDefinition(puzzlesData.getJSONObject(i));
							
							puzzles.put(puzzle.getId(), puzzle);
						}
						catch(Exception e)
						{
							//TODO: LOG ERROR LOADING PUZZLE
							e.printStackTrace();
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

	@Override
    public JSONObject toJSONObject() throws JSONException
    {
		JSONObject o=new JSONObject();
		
		;
		
	    return(o);
    }
}