package net.lugdunon.server.worldgen.procedural.instance.tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.generators.DungeonGenerator;
import net.lugdunon.math.Location;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.procedural.instance.tile.room.TileBasedRoom;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.constraints.PlanarConstraints;
import net.lugdunon.state.State;
import net.lugdunon.state.character.IPlayerMovedListener;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.instance.Instance;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TileBasedDungeonBiome extends Biome implements IPlayerMovedListener
{
	protected Logger logger;
	
//	private long                                     timeoutInterval;
//	private long                                     accumulatedInterval;
//	
//	private boolean                                  needsInit;

	public TileBasedDungeonBiome(JSONObject o)
    {
	    super(o);

	    logger             =LoggerFactory.getLogger(TileBasedDungeonBiome.class);
	    
		//RESET
//		needsInit          =true;
//		accumulatedInterval=0;
	    
//    	timeoutInterval    =State.instance().getWorld().getWorldConfigProperty("dungeon.timeout.interval", World.DEFAULT_DUNGEON_TIMEOUT_INTERVAL);

//    	openedDoors        =new TreeSet<Long                               >();
//    	roomsToLightsources=new HashMap<Integer,List<PlaceableItemInstance>>();
    }
	
	/////

	@Override
	public void init(Terrain terrain, PlaceableItemCache placeableItemCache)
	{
		
	}
	
	@Override
	public void init(Instance instance)
	{
		//INITIALIZE DUNGEON
		if(!instance.hasRuntimeProperty("net.lugdunon.server.worldgen.procedural.dungeonInstance"))
		{
			IDungeon                                  dungeon            =null;
			Map <Integer,TileBasedRoom              > rooms              =null;
			List<PlaceableItemInstance              > doors              =null;
			Set <Long                               > openedDoors        =null;
			Map <Integer,List<PlaceableItemInstance>> roomsToLightsources=null;
		
			//GET DUNGEON INSTANCE
			try
            {
	            JSONObject       generatorArgs=instance.getInstanceDefinition().getJSONObject("generatorArgs");
	            DungeonGenerator generator    =new DungeonGenerator(
        			generatorArgs.getLong("seed"),
        			new PlanarConstraints(generatorArgs.getJSONObject("constraints"))
        		);

	            generator.setGenerateGoal  (generatorArgs.getBoolean("generateGoal"  ));
	            generator.setBossRoomLocked(generatorArgs.getBoolean("bossRoomLocked"));
	            generator.generate         (                                          );
	            
	            dungeon=generator.getDungeon();
	            
				instance.setRuntimeProperty("net.lugdunon.server.worldgen.procedural.dungeonInstance",dungeon);

				logger.info("Tile Based Dungeon instantiated with seed of {}",generatorArgs.getLong("seed"));
				
				State.instance().addPlayerMovedListener(this);
            }
            catch (JSONException e)
            {
				logger.error("Tile based dungeon initialization failed for instance with id: {}.",instance.getInstanceId(),e);
            }
			
			//SET UP DOOR AND LIGHTING STATES
			{
				Collection<PlaceableItemInstance> piis=instance.getPlaceableItemCache().list();
				
				doors              =new ArrayList<PlaceableItemInstance               >();
				openedDoors        =new TreeSet  <Long                                >();
				roomsToLightsources=new HashMap  <Integer, List<PlaceableItemInstance>>();

				instance.setRuntimeProperty("net.lugdunon.server.worldgen.procedural.doors",              doors              );
				instance.setRuntimeProperty("net.lugdunon.server.worldgen.procedural.openedDoors",        openedDoors        );
				instance.setRuntimeProperty("net.lugdunon.server.worldgen.procedural.roomsToLightsources",roomsToLightsources);
				
				for(PlaceableItemInstance pii:piis)
				{
					if(pii.getUserDefinedData() != null)
					{
						Room room;
						
						try
						{
							if(isLightsource(pii))
							{
								room=getRoom(dungeon, pii, "net.lugdunon.server.worldgen.procedural.room.id");
								
								if(!room.isStart())
								{
									pii.setLightingEnabled(false);
									
									if(!roomsToLightsources.containsKey(room.id))
									{
										roomsToLightsources.put(room.id, new ArrayList<PlaceableItemInstance>());
									}
									
									roomsToLightsources.get(room.id).add(pii);
								}
							}
							else if(isDoor(pii))
							{
								doors.add(pii);
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
			
			//SETUP ROOMS
			try
	        {
				if(instance.getInstanceDefinition().has("rooms"))
				{
		            JSONObject roomsData=instance.getInstanceDefinition().getJSONObject("rooms");
		            JSONArray  roomIds  =roomsData.names();
		            
		            if(roomIds.length() > 0)
		            {
		            	rooms=new HashMap<Integer,TileBasedRoom>();
		            	
		            	for(int i=0;i<roomIds.length();i++)
		            	{
		            		TileBasedRoom room=new TileBasedRoom(
		            			Integer  .parseInt     (roomIds.getString(i)),
		            			roomsData.getJSONObject(roomIds.getString(i)),
		            			instance,
		            			doors
		            		);

			            	rooms.put(room.getId(), room);
		            	}

						instance.setRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms",rooms);
						
						for(TileBasedRoom room:rooms.values())
						{
							room.postInit(instance);
						}
		            }
				}
	        }
	        catch (JSONException e)
	        {
	            e.printStackTrace();
	        }
		}
	}
	
	@SuppressWarnings("unchecked")
    @Override
    public void update(long delta, Instance instance)
    {
		int                                       apii               =State.instance().listActiveCharactersInInstance(instance.getInstanceId()).size();
		
		IDungeon                                  dungeon            =(IDungeon                                 ) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.dungeonInstance"    );
		List<PlaceableItemInstance              > doors              =(List<PlaceableItemInstance              >) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.doors"              );
		Set <Long                               > openedDoors        =(Set <Long                               >) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.openedDoors"        );
		Map <Integer,List<PlaceableItemInstance>> roomsToLightsources=(Map <Integer,List<PlaceableItemInstance>>) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.roomsToLightsources");
		Map <Integer,TileBasedRoom              > rooms              =(Map <Integer,TileBasedRoom              >) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"              );
		
		if(apii > 0)
		{
			List<PlaceableItemInstance> newlyOpenedDoors=new ArrayList<PlaceableItemInstance>();
			
			for(PlaceableItemInstance door:doors)
			{
				Room                        room;
				List<PlaceableItemInstance> lightsources;
				
				try
				{
					//CHECK IF LIGHT STATE CHANGE NEEDED
					if(
						door.getCurrentState().equals("opened") && 
						!openedDoors.contains(door.getItemInstanceId())
					)
					{
						newlyOpenedDoors.add(door                    );
						openedDoors     .add(door.getItemInstanceId());
						
						//SOURCE
						room        =getRoom(dungeon,door,"net.lugdunon.server.worldgen.procedural.room.id");
						lightsources=roomsToLightsources.get(room.id);

						if(lightsources != null && lightsources.size() > 0 && !lightsources.get(0).isLightingEnabled())
						{
							for(PlaceableItemInstance lightsource:lightsources)
							{
								lightsource.setLightingEnabled(true);
							}
						}
						
						//TARGET
						room        =getRoom(dungeon,door,"net.lugdunon.server.worldgen.procedural.target.room.id");
						lightsources=roomsToLightsources.get(room.id);
						
						if(lightsources != null && lightsources.size() > 0 && !lightsources.get(0).isLightingEnabled())
						{
							for(PlaceableItemInstance lightsource:lightsources)
							{
								lightsource.setLightingEnabled(true);
							}
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			if(newlyOpenedDoors.size() > 0)
			{
				doors.removeAll(newlyOpenedDoors);
			}
			
			//CALL ROOM UPDATES
			for(TileBasedRoom room:rooms.values())
			{
				room.update(delta);
			}
		}
    }

	private Room getRoom(IDungeon dungeon, PlaceableItemInstance pii, String roomIdKey) throws JSONException
    {
	    return(
	    	dungeon.get(
	    		pii.getUserDefinedData().getInt(
	    			roomIdKey
	    		)
	    	)
	    );
    }

	private boolean isLightsource(PlaceableItemInstance pii) throws JSONException
    {
	    return(
	    	pii.getUserDefinedData().has       ("net.lugdunon.server.worldgen.procedural.room.lightsource") && 
	    	pii.getUserDefinedData().getBoolean("net.lugdunon.server.worldgen.procedural.room.lightsource")
	    );
    }

	private boolean isDoor(PlaceableItemInstance pii) throws JSONException
    {
	    return(
	    	pii.getUserDefinedData().has       ("net.lugdunon.server.worldgen.procedural.room.door") && 
	    	pii.getUserDefinedData().getBoolean("net.lugdunon.server.worldgen.procedural.room.door")
	    );
    }
	
	////

	@SuppressWarnings("unchecked")
    @Override
    public void playerMoved(PlayerCharacter pc, Location previousLocation, Location currentLocation)
    {
		try
		{
			//INSTANCE TRANSITION
			if(previousLocation == null || (previousLocation.getInstanceId() != currentLocation.getInstanceId()))
			{
				if(previousLocation != null)
				{
					Instance pInstance=State.instance().getWorld().getInstance(previousLocation.getInstanceId());
					
					if(pInstance.hasRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"))
					{
						Map        <Integer,TileBasedRoom> rooms    =(Map<Integer,TileBasedRoom>) pInstance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"  );
						Collection <        TileBasedRoom> roomsList=rooms.values();
						int                                roomDim  =pInstance.getInstanceDefinition().getInt("roomSize");
						Rect                               roomRect =new Rect(0, 0, roomDim, roomDim);
						
						for(TileBasedRoom room:roomsList)
					    {
					    	roomRect.setPosition(
					    		room.getOrigin().x,
					    		room.getOrigin().y
					    	);
					    	
					    	if(roomRect.contains(previousLocation.getLocation()))
					    	{
					    		room.playerLeft(pc);
					    		break;
					    	}
					    }
					}
				}

				Instance cInstance=State.instance().getWorld().getInstance(currentLocation .getInstanceId());
				
				if(cInstance.hasRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"))
				{
					Map        <Integer,TileBasedRoom> rooms    =(Map<Integer,TileBasedRoom>) cInstance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"  );
					Collection <        TileBasedRoom> roomsList=rooms.values();
					int                                roomDim  =cInstance.getInstanceDefinition().getInt("roomSize");
					Rect                               roomRect =new Rect(0, 0, roomDim, roomDim);
					
					for(TileBasedRoom room:roomsList)
				    {
				    	roomRect.setPosition(
				    		room.getOrigin().x,
				    		room.getOrigin().y
				    	);
				    	
				    	if(roomRect.contains(currentLocation.getLocation()))
				    	{
				    		room.playerEntered(pc);
				    		break;
				    	}
				    }
				}
			}
			else
			{
				Instance instance =State.instance().getWorld().getInstance(previousLocation.getInstanceId());
				
				if(instance.hasRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"))
				{
					Map        <Integer,TileBasedRoom> rooms       =(Map<Integer,TileBasedRoom>) instance.getRuntimeProperty("net.lugdunon.server.worldgen.procedural.rooms"  );
					Collection <        TileBasedRoom> roomsList   =rooms.values();
					int                                roomDim     =instance.getInstanceDefinition().getInt("roomSize");
					Rect                               roomRect    =new Rect(0, 0, roomDim, roomDim);

					TileBasedRoom                      previousRoom=null;
					TileBasedRoom                      currentRoom =null;
					
					for(TileBasedRoom room:roomsList)
				    {
				    	roomRect.setPosition(
				    		room.getOrigin().x,
				    		room.getOrigin().y
				    	);

				    	if(roomRect.containsExclusive(previousLocation.getLocation()))
				    	{
				    		previousRoom=room;
				    	}
				    	
				    	if(roomRect.containsExclusive(currentLocation .getLocation()))
				    	{
				    		currentRoom=room;
				    	}
				    	
				    	if(
				    		(previousRoom != null) && 
				    		(currentRoom  != null)
				    	)
				    	{
				    		break;
				    	}
				    }
					
					if(previousRoom != currentRoom)
					{
						previousRoom.playerLeft   (pc);
						currentRoom .playerEntered(pc);
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
}