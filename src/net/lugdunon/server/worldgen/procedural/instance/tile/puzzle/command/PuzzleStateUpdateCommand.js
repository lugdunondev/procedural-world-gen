Namespace.declare("net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command");

Namespace.newClass("net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand","net.lugdunon.command.core.Command");

// //
net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_INIT    =0;
net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_DESTRUCT=1;
net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_UPDATED =2;

net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand,"init",[initData]);

	this.puzzleCache={};
	
	//INSTANCE
	//ROOM
	//PUZZLE ID
	
	//STATE DATA
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.prototype.opInit=function(initData)
{
	//this.actionBarIndex=initData.actionBarIndex;
};

net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.prototype.getCommandLength=function()
{
    return(0);
};

net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.prototype.buildCommand=function(dataView)
{
	//dataView.writeInt8(this.actionBarIndex);
};

net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.prototype.commandResponse=function(res)
{
	var mode      =res.readUint8 ();
	var instanceId=res.readUint64();
	var roomId    =res.readUint16();
	var puzzleId  =res.readString();
	
	if(this.puzzleCache[instanceId] == null)
	{
		this.puzzleCache[instanceId]={};
	}
	
	if(this.puzzleCache[instanceId][roomId] == null)
	{
		this.puzzleCache[instanceId][roomId]={};
	}
	
	if(this.puzzleCache[instanceId][roomId][puzzleId] == null)
	{
		this.puzzleCache[instanceId][roomId][puzzleId]={};
	}
	
	switch(mode)
	{
		case net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_INIT:
		{
			this.puzzleCache[instanceId][roomId][puzzleId]=Namespace.instantiate(res.readString()).init({});
			
			break;
		}
		case net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_DESTRUCT:
		{
			this.puzzleCache[instanceId][roomId][puzzleId].destruct();
			
			break;
		}
		case net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.command.PuzzleStateUpdateCommand.PUZZLE_STATE_MODE_UPDATED:
		{
			this.puzzleCache[instanceId][roomId][puzzleId].stateUpdated(res.readObject());
			
			break;
		}
	}
};