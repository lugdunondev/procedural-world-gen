package net.lugdunon.server.worldgen.procedural.instance.tile.room;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.bytten.metazelda.IDungeon;
import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Coords;
import net.lugdunon.io.JSONObjectSerializable;
import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.procedural.instance.tile.puzzle.BasePuzzle;
import net.lugdunon.state.State;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TileBasedRoomSpawner implements JSONObjectSerializable
{
	private int                    roomTileSize;
	private short[]                terrain;
	private byte []                elevation;
	
	private JSONArray              placeables;
	private JSONObject             dungeonMetaData;

	private short                  roomDimensions;
	
	private boolean                isStarterTile;
	private boolean                hasNorthDoor;
	private boolean                hasEastDoor;
	private boolean                hasSouthDoor;
	private boolean                hasWestDoor;
	
	private Map<String,BasePuzzle> puzzles;
	
	private String                 id;
	
	public TileBasedRoomSpawner(String id, JSONObject o)
    {
		this.id=id;
		
	    try
        {
	        fromJSONObject(o);
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
    }
	
	////
	
	public Point createRoom(JSONObject instanceDef, Terrain terrain, PlaceableItemCache pic, Random random, Room room, IDungeon dungeon, boolean asStart)
	{
		JSONObject rooms       =null;
		int        dungeonSize =terrain.getSize()/getRoomDimensions();
		int        offset      =((dungeonSize/2)-1);
		Coords     coords      =room.getCoords().iterator().next();
		Point      origin      =new Point(coords.x,coords.y);
		Point      curTile     =new Point();
		int        curRoomIndex=0;
		int        curTerrIndex=0;
		
		origin.add (offset             );
		origin.mult(getRoomDimensions());
		
		try
        {
	        dungeonMetaData.put("origin", origin.toJSONObject());
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }

		for(int x=0;x<getRoomDimensions();x++)
		{
			for(int y=0;y<getRoomDimensions();y++)
			{
				curRoomIndex=FastMath.pointToIndex(curTile.set(x, y  ),getRoomDimensions());
				curTerrIndex=FastMath.pointToIndex(curTile.add(origin),terrain.getSize  ());
				
				terrain.getTerrainData  ()[curTerrIndex]=this.terrain  [curRoomIndex];
				terrain.getElevationData()[curTerrIndex]=this.elevation[curRoomIndex];
				
				if(this.terrain[curRoomIndex] > 0)
				{
					terrain.getBiomeData()[curTerrIndex]=(byte) 1;
				}
			}
		}
		
		//HANDLE PORTAL AND SPAWN
		if(isStarterTile())
		{
			if(asStart)
			{
				try
				{
					Point spawn=new Point(getDungeonMetadata().getJSONObject("spawn"));
					
					instanceDef.put("spawn", FastMath.wrap(spawn.add(origin), terrain.getSize()).toJSONObject());
					
					//MAKE SURE PORTAL HAS PII ID
					for(int i=0;i<placeables.length();i++)
					{
						try
	                    {
		                    if("PORTAL.LADDER.WOOD".equals(placeables.getJSONObject(i).getString("itemDef")))
		                    {
		    					placeables.put(0,placeables.remove(i));
		                    	break;
		                    }
	                    }
	                    catch (JSONException e)
	                    {
		                    e.printStackTrace();
	                    }
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				for(int i=0;i<placeables.length();i++)
				{
					try
                    {
	                    if("PORTAL.LADDER.WOOD".equals(placeables.getJSONObject(i).getString("itemDef")))
	                    {
	                    	placeables.remove(i);
	                    	break;
	                    }
                    }
                    catch (JSONException e)
                    {
	                    e.printStackTrace();
                    }
				}
			}
		}
		
		//LIGHTING
		try
        {
			JSONObject lightSource=null;
			
			for(int i=0;i<placeables.length();i++)
			{
				lightSource=placeables.getJSONObject(i);
				
				try
				{
					if("LIGHT.DUNGEON".equals(lightSource.getString("itemDef")))
					{
						lightSource.getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.room.lightsource",true);
					}
				}
				catch(Exception e)
				{
					;
				}
			}

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

		//PLACEABLES
        try
        {
        	for(int i=0;i<placeables.length();i++)
        	{
        		if(!placeables.getJSONObject(i).has("userDefinedData"))
        		{
        			placeables.getJSONObject(i).put("userDefinedData",new JSONObject());
        		}
        		
        		placeables.getJSONObject(i).getJSONObject("userDefinedData").put("net.lugdunon.server.worldgen.procedural.room.id",room.id);
        	}
        	
	        pic.processPlaceables(placeables,origin);
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
		
		//HANDLE PUZZLES
		if(hasPuzzles())
		{
			Map<String,BasePuzzle> puzzles=getPuzzles();
			
			for(String puzzleId:puzzles.keySet())
			{
				puzzles.get(puzzleId).generate(pic,dungeonMetaData,room);
			}
		}
        
        try
        {
	        if(!instanceDef.has("rooms"))
	        {
	        	rooms=new JSONObject();
	        	instanceDef.put("rooms",rooms);
	        }
	        
	        rooms=instanceDef.getJSONObject("rooms");
	        rooms.put(String.valueOf(room.id), dungeonMetaData);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        
        return(origin);
	}
	
	private Map<String, BasePuzzle> getPuzzles()
    {
	    return(puzzles);
    }

//	private boolean isLightsource(JSONObject lightSource) throws JSONException
//    {
//	    Item item=State.instance().getWorld().getItemDefinitions().getItemDef(lightSource.getString("itemDef"));
//	    
//	    return(item.isLightsource());
//    }

	////
	
	public String getId()
	{
		return(id);
	}
	
	public JSONObject getDungeonMetadata()
	{
		return(dungeonMetaData);
	}
	
	////
	
	public boolean hasPuzzles()
	{
		return(puzzles != null);
	}
	
	public boolean isExternalPuzzle()
	{
		if(hasPuzzles())
		{
			for(BasePuzzle puzzle:puzzles.values())
			{
				if(puzzle.isExternallyLinkable())
				{
					return(true);
				}
			}
		}
		
		return(false);
	}
	
	////
	
	public short getRoomDimensions()
	{
		return(roomDimensions);
	}
	
	public boolean isStarterTile()
	{
		return(isStarterTile);
	}
	
	public boolean hasNorthDoor()
	{
		return(hasNorthDoor);
	}
	
	public boolean hasEastDoor()
	{
		return(hasEastDoor);
	}
	
	public boolean hasSouthDoor()
	{
		return(hasSouthDoor);
	}
	
	public boolean hasWestDoor()
	{
		return(hasWestDoor);
	}
	
	////

	@Override
    public void fromJSONObject(JSONObject o) throws JSONException
    {
		JSONArray terrain  =o.getJSONArray("terrain"  );
		JSONArray elevation=o.getJSONArray("elevation");

		roomTileSize  =terrain.length();
		roomDimensions=new Double(Math.sqrt(roomTileSize)).shortValue();
		
		this.terrain  =new short[roomTileSize];
		this.elevation=new byte [roomTileSize];
		
		for(int i=0;i<roomTileSize;i++)
		{
			this.terrain  [i]=(short) terrain  .getInt(i);
			this.elevation[i]=(byte ) elevation.getInt(i);
		}

		placeables     =o.getJSONArray ("placeables"     );
		dungeonMetaData=o.getJSONObject("dungeonMetaData");
		
		{
			JSONArray doors=dungeonMetaData.getJSONArray("doors");
			
			isStarterTile=dungeonMetaData.has("starterTile")?dungeonMetaData.getBoolean("starterTile"):false;
			hasNorthDoor =false;
			hasEastDoor  =false;
			hasSouthDoor =false;
			hasWestDoor  =false;
			
			for(int i=0;i<doors.length();i++)
			{
				     if(doors.getInt(i) == 0)
				{
					hasNorthDoor=true;
				}
				else if(doors.getInt(i) == 1)
				{
					hasEastDoor=true;
				}
				else if(doors.getInt(i) == 2)
				{
					hasSouthDoor=true;
				}
				else if(doors.getInt(i) == 3)
				{
					hasWestDoor=true;
				}
			}
		}
		
		try
		{
			if(dungeonMetaData.has("puzzles"))
			{
				JSONArray puzzlesData=dungeonMetaData.getJSONArray("puzzles");
				
				if(puzzlesData.length() > 0)
				{
					puzzles=new HashMap<String,BasePuzzle>();
					
					for(int i=0;i<puzzlesData.length();i++)
					{
						try
						{
							BasePuzzle puzzle=(BasePuzzle) State.makeObjectFromJSONDefinition(puzzlesData.getJSONObject(i));
							
							puzzles.put(puzzle.getId(), puzzle);
						}
						catch(Exception e)
						{
							//TODO: LOG ERROR LOADING PUZZLE
							e.printStackTrace();
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

	@Override
    public JSONObject toJSONObject() throws JSONException
    {
	    return(null);
    }
}