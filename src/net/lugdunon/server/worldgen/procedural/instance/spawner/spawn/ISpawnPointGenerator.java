package net.lugdunon.server.worldgen.procedural.instance.spawner.spawn;

import java.util.Random;

import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONException;
import org.json.JSONObject;

public interface ISpawnPointGenerator
{
	public void init(JSONObject args);
    public void setSpawnPoint(JSONObject instanceConfig, Random random, Terrain terrain, PlaceableItemCache placeableItemCache) throws WorldIdNotSetException, JSONException;
}
