package net.lugdunon.server.worldgen.procedural.instance.spawner;

import java.io.File;

import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;
import net.lugdunon.server.worldgen.procedural.noise.INoiseGen;
import net.lugdunon.state.State;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.util.FileUtils;
import net.lugdunon.world.instance.TransientInstance;
import net.lugdunon.world.instance.TransientInstanceCache;
import net.lugdunon.world.instance.spawner.DefaultInstanceSpawner;
import net.lugdunon.world.instance.spawner.ex.InstanceSpawnException;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONObject;

public class ProceduralInstanceSpawner extends DefaultInstanceSpawner
{
	@Override
	public TransientInstance spawnInstance(TransientInstanceCache instanceCache, long instanceId, String templateId) throws InstanceSpawnException
	{
		if(instanceExists(instanceId))
		{
			//TODO: NEED TO GET DUNGEON OR SOMETHING
			
	        try
            {
	            return(instanceCache.getInstance(instanceId));
            }
            catch (WorldIdNotSetException e)
            {
	            throw new InstanceSpawnException(e);
            }
		}
		else if(initInstance(instanceCache, instanceId, templateId))
		{
			try
			{
				PlaceableItemCache pic;
				Terrain            terrain;
				JSONObject         instanceDef=getInstanceDefinition(instanceId);
				JSONObject         pigConf    =instanceDef.getJSONObject("proceduralConfiguration");
				JSONObject         noiseDef   =pigConf.getJSONObject("noise");
				int                dimension  =instanceDef.getInt("dimension");
				String             preset     =instanceDef.getString("terrain");
				INoiseGen          noiseGen   =(INoiseGen) ProceduralInstanceSpawner.class.getClassLoader().loadClass(noiseDef.getString("class")).newInstance();
		        float[]            noise      =noiseGen.computeNoise(
		        	State.instance().getWorld().getRandom(),
		        	instanceId,
		        	instanceDef,
		        	dimension,
		        	noiseDef.getJSONObject("args")
		        );
		        
		        WorldGenerator.setNoState(true);

				//GENERATE TERRAIN DATA
		        terrain=ProceduralWorldGen.createTerrain(
		        	instanceId,
	        		instanceDef,
	        		noise,
	        		WorldGenerator.getTileset(),
	        		preset,
	        		WorldGenerator.getRandom(),
	        		WorldGenerator.getRandom().nextLong(),
	        		dimension,
	        		false
	        	);
		        
				//CREATE ITEMS AND NPCS
				pic=ProceduralWorldGen.initBiomes(
					instanceDef,
					terrain,
	        		State.instance().getWorld().getItemDefinitions(),
					State.instance().getWorld().getTileset(),
	        		preset
				);
		        
				//RECACHE OUR TERRAIN
		        WorldGenerator.setTerrain(terrain);
				
				//SET SPAWN POINT
				ProceduralWorldGen.getSpawnPointGenerator(instanceDef).setSpawnPoint(
					instanceDef,
	        		State.instance().getWorld().getRandom(),
	        		terrain,
	        		pic
				);
				
				//SAVE TERRAIN, PLACEABLE ITEM CACHE, AND INSTANCE
				{
					terrain.saveTerrainData(new File(FileUtils.getActualPath(getInstancePath(instanceId,"terrain.dat"))));
					
					FileUtils.writeJSON(
						getInstancePath(instanceId,"placeableItemCache.json"),
						pic.toJSONObject(),
						true
					);
					
					FileUtils.writeJSON(
						getInstancePath(instanceId,"instance.json"),
						instanceDef,
						true
					);
				}
				
		        WorldGenerator.setNoState(false);
		        WorldGenerator.setTerrain(null );
		        
		        return(instanceCache.getInstance(instanceId));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
		        WorldGenerator.setNoState(false);
		        WorldGenerator.setTerrain(null );
			}
		}
		
		throw new InstanceSpawnException();
	}
}
