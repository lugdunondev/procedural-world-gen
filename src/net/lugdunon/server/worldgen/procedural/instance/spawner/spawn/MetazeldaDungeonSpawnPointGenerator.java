package net.lugdunon.server.worldgen.procedural.instance.spawner.spawn;

import java.util.Random;

import net.bytten.metazelda.Room;
import net.bytten.metazelda.util.Coords;
import net.lugdunon.math.Point;
import net.lugdunon.math.Rect;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.server.worldgen.procedural.ProceduralWorldGen;
import net.lugdunon.server.worldgen.procedural.item.placement.metazelda.MetazeldaDungeonPlacementStrategy;
import net.lugdunon.server.worldgen.procedural.noise.INoiseGen;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.MetazeldaDungeon;
import net.lugdunon.server.worldgen.procedural.noise.metazelda.generator.BaseDungeonGeneratorWrapper;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONException;
import org.json.JSONObject;

public class MetazeldaDungeonSpawnPointGenerator implements ISpawnPointGenerator
{
    public void setSpawnPoint(JSONObject instanceConfig, Random random, Terrain terrain, PlaceableItemCache placeableItemCache) throws WorldIdNotSetException, JSONException
    {
		try
		{
			JSONObject                  noiseDef=instanceConfig.getJSONObject("proceduralConfiguration").getJSONObject("noise");
			INoiseGen                   noiseGen=(INoiseGen) MetazeldaDungeonPlacementStrategy.class.getClassLoader().loadClass(noiseDef.getString("class")).newInstance();
			BaseDungeonGeneratorWrapper dgw     =((MetazeldaDungeon) noiseGen).generateDungeon(terrain.getSize(),noiseDef.getJSONObject("args"));
			Room                        start   =dgw.getDungeon().findStart();
			Coords                      c       =start.getCoords().iterator().next().subtract(
				new Coords(
					dgw.getDungeon().getExtentBounds().left,
					dgw.getDungeon().getExtentBounds().top
				)
			);
			Rect                        roomDim =new Rect(
				c.x*dgw.getRoomWidth (),
				c.y*dgw.getRoomHeight(),
				    dgw.getRoomWidth (),
				    dgw.getRoomHeight()
			);
			Point                       spawn   =new Point(
    			roomDim.getPosition().getX()+(dgw.getWallThickness()/2)+2,
    			roomDim.getPosition().getY()+(dgw.getWallThickness()/2)+2
    		);

	    	instanceConfig    .put("spawn",spawn.toJSONObject());
	    	placeableItemCache.getPlaceableItemReference(
        		ProceduralWorldGen.EXIT_PORTAL_ID
        	).setLocation(
        		spawn.sub(
    				placeableItemCache.getPlaceableItemReference(
    					ProceduralWorldGen.EXIT_PORTAL_ID
    		    	).getItemDef().getGroundingPoint()
        		)
        	);
		}
		catch(Exception e)
		{
//			logger.error("Metazelda dungeon generator wrapper initialization failed for instance with id: {}.",instance.getInstanceId(),e);
			e.printStackTrace();
		}
    }

	@Override
    public void init(JSONObject args)
    {
	    ;
    }
}
