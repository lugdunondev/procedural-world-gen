package net.lugdunon.server.worldgen.procedural.instance.spawner.spawn;

import java.util.Random;

import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RandomSpawnPointGenerator implements ISpawnPointGenerator
{
	JSONArray spawnBiomes;
	
    public void setSpawnPoint(JSONObject instanceConfig, Random random, Terrain terrain, PlaceableItemCache placeableItemCache) throws WorldIdNotSetException, JSONException
    {
    	byte  spawnBiome     =(byte) spawnBiomes.getInt(random.nextInt(spawnBiomes.length()));
    	int[] indicesForBiome=terrain.getSpawnableIndicesForBiome(spawnBiome);
    	Point spawn          =FastMath.indexToPoint(indicesForBiome[random.nextInt(indicesForBiome.length)], terrain.getSize());
    	
    	//CONFIGURE RETURN PORTAL
    	if(instanceConfig.has("returnPortal") && !instanceConfig.isNull("returnPortal"))
    	{
        	PlaceableItemInstance returnPortal=placeableItemCache.getPlaceableItemReference(0);
    		Point                 arrivalTile =new Point(returnPortal.getCurrentStateDefinition().getJSONObject("instanceTrigger").getJSONObject("arrivalTile"));

	    	//MOVE UP UNTIL WALL
        	if(instanceConfig.has("returnPortalToWall") && instanceConfig.getBoolean("returnPortalToWall"))
        	{
    	    	while(terrain.getElevationAt(spawn.getX(),spawn.getY()) == terrain.getElevationAt(spawn.getX(),spawn.getY()-3))
    	    	{
    	    		spawn.setY(FastMath.wrap(spawn.getY()-1,terrain.getSize()));
    	    	}
        	}
    		
    		//OFFSET FROM SPAWN LOCATION
    		returnPortal.setLocation(new Point(spawn).sub(arrivalTile));
    	}
    	
	    instanceConfig.put("spawn",spawn.toJSONObject());
    }

	@Override
    public void init(JSONObject args)
    {
		try
		{
			spawnBiomes=args.getJSONArray("biomes");
		}
		catch(Exception e)
		{
			;
		}
    }
}
