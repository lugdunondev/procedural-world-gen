package net.lugdunon.server.worldgen.procedural.item.placement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.state.item.IPlacementStrategy;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.biome.Biome;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GriddedPlacementStrategy implements IPlacementStrategy
{
	protected Logger logger;
	
	protected int    gridSpacing;
	protected int    verticalOffset;
	protected int    horizontalOffset;
	protected int    elevationMinimumDistance;
	protected int    terrainMinimumDistance;
	protected double dynamicPlacementProbability;
	
	public GriddedPlacementStrategy()
	{
	    this.logger=LoggerFactory.getLogger(GriddedPlacementStrategy.class);
	}
	
	@Override
    public void init(JSONObject args)
    {
	    try
	    {
			gridSpacing=args.getInt("spacing");
			
			if(args.has("verticalOffset") && !args.isNull("verticalOffset"))
			{
				verticalOffset=args.getInt("verticalOffset");
			}
			else
			{
				verticalOffset=0;
			}
			
			if(args.has("horizontalOffset") && !args.isNull("horizontalOffset"))
			{
				horizontalOffset=args.getInt("horizontalOffset");
			}
			else
			{
				horizontalOffset=0;
			}
			
			if(args.has("elevationMinimumDistance") && !args.isNull("elevationMinimumDistance"))
			{
				elevationMinimumDistance=args.getInt("elevationMinimumDistance")*args.getInt("elevationMinimumDistance");
			}
			else
			{
				elevationMinimumDistance=9;
			}
			
			if(args.has("terrainMinimumDistance") && !args.isNull("terrainMinimumDistance"))
			{
				terrainMinimumDistance=args.getInt("terrainMinimumDistance")*args.getInt("terrainMinimumDistance");
			}
			else
			{
				terrainMinimumDistance=4;
			}
			
			if(args.has("dynamicPlacementProbability") && !args.isNull("dynamicPlacementProbability"))
			{
				dynamicPlacementProbability=args.getDouble("dynamicPlacementProbability");
			}
			else
			{
				dynamicPlacementProbability=0.0;
			}
	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
	
	@Override
	public void placeItems(JSONObject instanceConfig, Terrain terrain, PlaceableItemCache placeableItemCache)
	{
		Map<Byte,Map<String, Integer>> placedItems=new HashMap<Byte,Map<String,Integer>>();
		List<Point>                    points     =new ArrayList<Point>();
		Point                          location   =null;
		Biome                          biome      =null;
		Item                           item       =null;
		int                            cnt        =0;
		
		logger.info("Item placement commencing with a grid spacing of {}, horizontal offset of {}, and a vertical offset of {}",new Object[]{gridSpacing,horizontalOffset,verticalOffset});	

		for(int x=0;x<terrain.getSize();x+=gridSpacing)
		{
			for(int y=0;y<terrain.getSize();y+=gridSpacing)
			{
				points.add(new Point(x+((((y/gridSpacing)%2)==0)?horizontalOffset:0),y+((((x/gridSpacing)%2)==0)?verticalOffset:0)));
			}
		}
		
		while(points.size() > 0)
		{
			location=points.remove(WorldGenerator.getRandom().nextInt(points.size()));
			biome   =terrain.getBiome(terrain.getBiomeAt(location.getX(),location.getY()));
			
			if(biome != null)
			{
				item=biome.getNextItemForPlacement(false);
				
				if(item != null)
				{
					//ELEVATION & TERRAIN TRANSITION CHECKS
					if(
						!terrain.elevationChangeInRangeOf(location,elevationMinimumDistance) &&
						!terrain.terrainChangeInRangeOf  (location,terrainMinimumDistance  )
					)
					{
						placeableItemCache.newPlaceableItemReference(item,location.sub(item.getGroundingPoint()));
						
						if(!placedItems.containsKey(biome.getId()))
						{
							placedItems.put(biome.getId(),new HashMap<String, Integer>());
						}
						
						if(!placedItems.get(biome.getId()).containsKey(item.getItemId()))
						{
							placedItems.get(biome.getId()).put(item.getItemId(),0);
						}
						
						placedItems.get(biome.getId()).put(
							item.getItemId(),
							placedItems.get(biome.getId()).get(item.getItemId())+1
						);
						
						cnt++;
					}
					else
					{
						biome.itemPlacementCanceled(false);
					}
				}
			}
		}
		
		//LOG
		for(byte b:placedItems.keySet())
		{
			Map<String,Integer> pi=placedItems.get(b);
			
			for(String i:pi.keySet())
			{
				logger.info(
					"Placed {} of {} in {}.",
					new String[]
					{
						String.valueOf(pi.get(i)),
						WorldGenerator.getItemDefinitions().getItemDef(i).getName(),
						terrain.getBiome(b).getName()
					}
				);
			}
		}
		
		logger.info("{} items placed in total.",cnt);
	}
	
	@Override
	public PlaceableItemInstance placeItem(Terrain terrain, PlaceableItemCache placeableItemCache, Point location)
    {
	    PlaceableItemInstance pii=null;

		//VERIFY POINT IS GRIDSPACED
		if(placeableItemCache.getPlaceableItemReference(location) == null && isGridspaced(terrain,location))
		{
		    Biome biome=terrain.getBiome(terrain.getBiomeAt(location.getX(),location.getY()));
		    Item  item =null;
		    
		    if(biome != null)
		    {
		    	item=biome.getNextItemForPlacement(true);
		    	
		    	if(item != null)
		    	{
		    		//PROBABILITY CHECK
		    		if(WorldGenerator.getRandom().nextDouble() < dynamicPlacementProbability)
		    		{
		    			pii=placeableItemCache.newPlaceableItemReference(item,location.sub(item.getGroundingPoint()));
		    		}
		    		else
		    		{
		    			biome.itemPlacementCanceled(true);
		    		}
		    	}
		    }
		}
	    
	    return(pii);
    }

	private boolean isGridspaced(Terrain terrain, Point location)
    {
		int x=0;
		int y=0;
		
		if((horizontalOffset == 0) || (location.getX()%gridSpacing == 0))
		{
			x=(location.getX()/gridSpacing)*gridSpacing;
		}
		else
		{
			x=location.getX()-horizontalOffset;
			x=(location.getX()/gridSpacing)*gridSpacing;
			x+=horizontalOffset;
		}
		
		if((verticalOffset == 0) || (location.getY()%gridSpacing == 0))
		{
			y=(location.getY()/gridSpacing)*gridSpacing;
		}
		else
		{
			y=location.getY()-verticalOffset;
			y=(location.getY()/gridSpacing)*gridSpacing;
			y+=verticalOffset;
		}
		
		if(location.equals(x,y))
		{
			return(true);
		}
		
	    return(false);
    }
}
