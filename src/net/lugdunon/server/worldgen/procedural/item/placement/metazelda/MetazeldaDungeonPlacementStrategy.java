package net.lugdunon.server.worldgen.procedural.item.placement.metazelda;

import net.lugdunon.math.Point;
import net.lugdunon.state.item.IPlacementStrategy;
import net.lugdunon.state.item.PlaceableItemCache;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONObject;

public class MetazeldaDungeonPlacementStrategy implements IPlacementStrategy
{
	@Override
    public void init(JSONObject args)
    {
	    ;
    }
	
	@Override
	public void placeItems(JSONObject instanceConfig, Terrain terrain, PlaceableItemCache placeableItemCache)
	{
//		try
//		{
//			Rect   r;
//			Coords c;
//			JSONObject                  noiseDef=instanceConfig.getJSONObject("proceduralConfiguration").getJSONObject("noise");
//			INoiseGen                   noiseGen=(INoiseGen) MetazeldaDungeonPlacementStrategy.class.getClassLoader().loadClass(
//					noiseDef.getString("class")
//			).newInstance();
//			BaseDungeonGeneratorWrapper dgw     =((MetazeldaDungeon) noiseGen).generateDungeon(terrain.getSize(),noiseDef.getJSONObject("args"));
//			Coords                      d       =new Coords(
//				dgw.getDungeon().getExtentBounds().left,
//				dgw.getDungeon().getExtentBounds().top
//			);
//			int                         b=dgw.getWallThickness()/2;
//			int                         p=dgw.getDoorWidth    ()/2;
//			
//			for(Room room:dgw.getDungeon().getRooms())
//			{
//				c=room.getCoords().iterator().next().subtract(d);
//				r=new Rect(
//					c.x*dgw.getRoomWidth (),
//					c.y*dgw.getRoomHeight(),
//					    dgw.getRoomWidth (),
//					    dgw.getRoomHeight()
//				);
//				
//				dgw.getRoomGenerator().generateItemsForRoom(
//					terrain,
//					placeableItemCache,
//					dgw.getDungeon(),
//					room,
//					r,
//					b,
//					p
//				);
//				
//		    	for(Edge edge:room.getEdges())
//		    	{
//		    		dgw.getRoomGenerator().generateDoorForConnector(
//						terrain,
//						placeableItemCache,
//	    				dgw.getDungeon(), 
//	    				room, 
//	    				r, 
//	    				b, 
//	    				p, 
//						room.getCoords().iterator().next().getDirectionTo(
//							dgw.getDungeon().get(
//								edge.getTargetRoomId()
//							).getCoords().iterator().next()
//						)
//					);
//		    	}
//			}
//		}
//        catch (Exception e)
//        {
//	        e.printStackTrace();
//        }
	}
	
	@Override
    public PlaceableItemInstance placeItem(Terrain terrain, PlaceableItemCache placeableItemCache, Point location)
    {
	    return(null);
    }
}
