Namespace.declare("net.lugdunon.server.worldgen.procedural.item.manifestation");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler","net.lugdunon.state.item.manifestation.DefaultManifestationHandler");

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler,"init",[initData]);
	
	this.dungeonGlyphDialog=$("#dungeonGlyphUI");
	
	if(this.dungeonGlyphDialog.length == 0)
	{
		$("body").append(Namespace.requireHTML(this.classId));

		this.dungeonGlyphDialog=$("#dungeonGlyphUI");
		this.dungeonGlyphDialog.on(
			Namespace.isMobile()?"touchstart":"mousedown",
			function(e)
			{
				$(this).css("display","none");
				
				e.preventDefault();
				e.stopPropagation();
				
				game.getCurrentGameState().setCurrentDialog(null);
				
				return(false);
			}
		);
	}
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler.prototype.userDefinedDataUpdated=function(placeableItemInstance)
{
	assetManager.removeImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler.prototype.showDungeonGlyph=function(placeableItemInstance,inputBegan)
{
	if(!inputBegan)
	{
		return;
	}

	if(game.gameMode == net.lugdunon.GameMode.PLAY && placeableItemInstance.userDefinedData != null && placeableItemInstance.userDefinedData.image != null)
	{
		game.getCurrentGameState().setCurrentDialog(this);
		
		this.dungeonGlyphDialog                                         .css("display"   ,"block");
		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("left"      ,((game.getWindowWidth ()/2)-160)+"px");
		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("top"       ,((game.getWindowHeight()/4)-128)+"px");
		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper").find("div").css("background","url('"+placeableItemInstance.userDefinedData.image+"')");
	}
	else if(game.gameMode == net.lugdunon.GameMode.PLAY)
	{
		game.getCurrentGameState().setCurrentDialog(this);
		
		this.dungeonGlyphDialog                                         .css("display"   ,"block");
		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("left"      ,((game.getWindowWidth ()/2)-160)+"px");
		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("top"       ,((game.getWindowHeight()/4)-128)+"px");
	}
};

//net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler.prototype.drawForeground=function(placeableItemInstance,delta)
//{
//	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler,"drawForeground",[placeableItemInstance,delta]);
//
//	if(placeableItemInstance.userDefinedData != null && placeableItemInstance.userDefinedData.image != null)
//	{
//		if(!assetManager.hasImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId))
//		{
//			assetManager.loadImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId,placeableItemInstance.userDefinedData.image)
//		}
//		
//		if(assetManager.hasImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId))
//		{
//			game.getContext().drawImage(
//				assetManager.getImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId),
//				0                                   ,0                                   ,480,270,
//				placeableItemInstance.screenLoc.x+ 4,placeableItemInstance.screenLoc.y+36, 24, 14
//			);
//		}
//	}	
//};

//net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler.prototype.update=function(placeableItemInstance,delta)
//{
//	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGlyphManifestationHandler,"update",[placeableItemInstance,delta]);
//	
//	if(placeableItemInstance.userDefinedData != null && placeableItemInstance.userDefinedData.image != null)
//	{
//		var sip=$("#DUNGEON.GLYPH\\.IMAGE\\."+placeableItemInstance.itemInstanceId);
//		
//		if(placeableItemInstance == game.getCurrentGameState().overPlaceable)
//		{
//			if(sip.length == 0)
//			{
//				$("body").append(
//					"<div id='DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId+
//					"' class='dungeonGlyphWrapper'><div style=\"background-image:url('"+placeableItemInstance.userDefinedData.image+
//					"')\"/></div>"
//				);
//				sip=$("#DUNGEON.GLYPH\\.IMAGE\\."+placeableItemInstance.itemInstanceId);
//			}
//			
//			sip.css("display","block");
//			sip.css("left",   (game.input.absoluteCursor.x-251)+"px");
//			sip.css("top",    (game.input.absoluteCursor.y-270)+"px");
//		}
//		else
//		{
//			sip.css("display","none");
//		}
//	}
//};