package net.lugdunon.server.worldgen.procedural.item.manifestation;

import java.util.Arrays;

import net.lugdunon.math.Dimension;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.state.item.manifestation.IManifestationHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BollardManifestationHandler implements IManifestationHandler
{
	@Override
	public int[] getImpassable(PlaceableItemInstance pii) throws JSONException
	{
		Dimension  dim      =new Dimension(pii.getItemDef().getProps().getJSONObject("dimensions"));
		JSONObject state    =pii.getItemDef().getProps().getJSONObject("states").getJSONObject(pii.getCurrentState());
		JSONArray  im        =state.getJSONArray("impassable");
		int[]      impassable=new int[dim.getW()*dim.getH()];
		
		Arrays.fill(impassable,0);
		
		for(int i=0;i<impassable.length;i++)
		{
			impassable[i]=im.getInt(i);
		}
		
		return(impassable);
	}

	public void bollardUp(PlaceableItemInstance pii)
	{
		String newState;
		
		if("whiteDown".equals(pii.getCurrentState()))
		{
			newState="whiteUp";
		}
		
		else if("blackDown".equals(pii.getCurrentState()))
		{
			newState="blackUp";
		}
		
		else if("redDown".equals(pii.getCurrentState()))
		{
			newState="redUp";
		}
		
		else if("greenDown".equals(pii.getCurrentState()))
		{
			newState="greenUp";
		}
		
		else if("blueDown".equals(pii.getCurrentState()))
		{
			newState="blueUp";
		}
		
		else
		{
			return;
		}
		
		this.toggle(newState);
	}
	
	public void bollardDown(PlaceableItemInstance pii)
	{
		String newState;
		
		if("whiteUp".equals(pii.getCurrentState()))
		{
			newState="whiteDown";
		}
		
		else if("blackUp".equals(pii.getCurrentState()))
		{
			newState="blackDown";
		}
		
		else if("redUp".equals(pii.getCurrentState()))
		{
			newState="redDown";
		}
		
		else if("greenUp".equals(pii.getCurrentState()))
		{
			newState="greenDown";
		}
		
		else if("blueUp".equals(pii.getCurrentState()))
		{
			newState="blueDown";
		}
		
		else
		{
			return;
		}
		
		toggle(newState);
	}
	
	private void toggle(String state)
	{
		;
	}
}