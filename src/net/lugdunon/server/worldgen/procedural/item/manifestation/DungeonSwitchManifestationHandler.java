package net.lugdunon.server.worldgen.procedural.item.manifestation;

import java.util.Arrays;

import net.lugdunon.math.Dimension;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.state.item.manifestation.IManifestationHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DungeonSwitchManifestationHandler implements IManifestationHandler
{
	@Override
	public int[] getImpassable(PlaceableItemInstance pii) throws JSONException
	{
		Dimension  dim      =new Dimension(pii.getItemDef().getProps().getJSONObject("dimensions"));
		JSONObject state    =pii.getItemDef().getProps().getJSONObject("states").getJSONObject(pii.getCurrentState());
		JSONArray  im        =state.getJSONArray("impassable");
		int[]      impassable=new int[dim.getW()*dim.getH()];
		
		Arrays.fill(impassable,0);
		
		for(int i=0;i<impassable.length;i++)
		{
			impassable[i]=im.getInt(i);
		}
		
		return(impassable);
	}
}
