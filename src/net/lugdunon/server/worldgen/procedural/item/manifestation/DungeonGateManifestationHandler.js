Namespace.declare("net.lugdunon.server.worldgen.procedural.item.manifestation");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler","net.lugdunon.state.item.manifestation.DefaultManifestationHandler");

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler,"init",[initData]);
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler.prototype.openGate=function(placeableItemInstance,inputBegan)
{
	this.toggle(placeableItemInstance,inputBegan,"opened");
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler.prototype.closeGate=function(placeableItemInstance,inputBegan)
{
	this.toggle(placeableItemInstance,inputBegan,"closed");
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonGateManifestationHandler.prototype.toggle=function(placeableItemInstance,inputBegan,state)
{
	if(!inputBegan)
	{
		return;
	}

	if(
		game.gameMode == 0 && 
		net.lugdunon.util.Math.placeableRangeCheck(
			placeableItemInstance.itemDef.props.stateChangeRange,
			game.player.actualLoc,
			placeableItemInstance
		) && 
		(!placeableItemInstance.hasPlacer() || placeableItemInstance.canAccess(game.player) !== false)
	)
	{
		if(placeableItemInstance.conduits.energyIn[0] != null)
		{
			net.lugdunon.ui.Dialog.ok("Item is Powered!","<br/>This item is currently under arc power and cannot be actuated manually.");
		}
		else if(placeableItemInstance.canAccess(game.player) === true)
		{
			game.client.sendCommand(
				game.client.buildCommand(
					"CORE.COMMAND.PLACEABLE.STATE.CHANGE",
					{
						piid :placeableItemInstance.itemInstanceId,
						state:state
					}
				)
			);
		}
		else
		{
			game.client.sendCommand(
				game.client.buildCommand(
					"CORE.COMMAND.CHECK.ACCESS",
					{
						piid    :placeableItemInstance.itemInstanceId,
						callback:function(s)
						{
							if(s)
							{
								game.client.sendCommand(
									game.client.buildCommand(
										"CORE.COMMAND.PLACEABLE.STATE.CHANGE",
										{
											piid :placeableItemInstance.itemInstanceId,
											state:state
										}
									)
								);
							}
						}
					}
				)
			);
		}
	}
};