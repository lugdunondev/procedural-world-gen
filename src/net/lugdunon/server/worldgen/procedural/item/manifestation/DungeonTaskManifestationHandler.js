Namespace.declare("net.lugdunon.server.worldgen.procedural.item.manifestation");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler","net.lugdunon.state.item.manifestation.AnimatedManifestationHandler");

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler.prototype.init=function(initData)
{
	var context=this;
	
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler,"init",[initData]);
	
	this.dungeonTaskUI=$("#dungeonTaskUI");
	
	if(this.dungeonTaskUI.length == 0)
	{
		$("body").append(Namespace.requireHTML(this.classId));

		this.dungeonTaskUI=$("#dungeonTaskUI");
		this.dungeonTaskUI.on(
			Namespace.isMobile()?"touchstart":"mousedown",
			function(e)
			{
				$(this).css("display","none");
				
				context.dungeonTaskUI.html("");
				
				e.preventDefault();
				e.stopPropagation();
				
				game.getCurrentGameState().setCurrentDialog(null);
				
				return(false);
			}
		);
	}
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler.prototype.resetPuzzle=function(placeableItemInstance)
{
	;
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler.prototype.userDefinedDataUpdated=function(placeableItemInstance)
{
	//assetManager.removeImage("DUNGEON.GLYPH.IMAGE."+placeableItemInstance.itemInstanceId);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonTaskManifestationHandler.prototype.showTaskUI=function(placeableItemInstance,inputBegan)
{
	if(!inputBegan)
	{
		return;
	}
	
console.log("tasked");

//	if(game.gameMode == net.lugdunon.GameMode.PLAY && placeableItemInstance.userDefinedData != null && placeableItemInstance.userDefinedData.image != null)
//	{
//		game.getCurrentGameState().setCurrentDialog(this);
//		
//		this.dungeonGlyphDialog                                         .css("display"   ,"block");
//		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("left"      ,((game.getWindowWidth ()/2)-160)+"px");
//		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper")            .css("top"       ,((game.getWindowHeight()/4)-128)+"px");
//		this.dungeonGlyphDialog.find(".dungeonGlyphWrapper").find("div").css("background","url('"+placeableItemInstance.userDefinedData.image+"')");
//	}
	if(game.gameMode == net.lugdunon.GameMode.PLAY)
	{
		game.getCurrentGameState().setCurrentDialog(this);
		
		this.dungeonTaskUI.css("display","block");
	}
};