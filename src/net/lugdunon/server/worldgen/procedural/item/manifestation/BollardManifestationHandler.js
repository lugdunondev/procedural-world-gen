Namespace.declare("net.lugdunon.server.worldgen.procedural.item.manifestation");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler","net.lugdunon.state.item.manifestation.DefaultManifestationHandler");

net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler,"init",[initData]);
	
	return(this);
};

//net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler.prototype.bollardUp=function(placeableItemInstance,inputBegan)
//{
//	var newState;
//	
//	if(placeableItemInstance.currentState == "whiteDown")
//	{
//		newState="whiteUp";
//	}
//	
//	else if(placeableItemInstance.currentState == "blackDown")
//	{
//		newState="blackUp";
//	}
//	
//	else if(placeableItemInstance.currentState == "redDown")
//	{
//		newState="redUp";
//	}
//	
//	else if(placeableItemInstance.currentState == "greenDown")
//	{
//		newState="greenUp";
//	}
//	
//	else if(placeableItemInstance.currentState == "blueDown")
//	{
//		newState="blueUp";
//	}
//	
//	else
//	{
//		return;
//	}
//	
//	this.toggle(newState);
//};
//
//net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler.prototype.bollardDown=function(placeableItemInstance,inputBegan)
//{
//	var newState;
//	
//	if(placeableItemInstance.currentState == "whiteUp")
//	{
//		newState="whiteDown";
//	}
//	
//	else if(placeableItemInstance.currentState == "blackUp")
//	{
//		newState="blackDown";
//	}
//	
//	else if(placeableItemInstance.currentState == "redUp")
//	{
//		newState="redDown";
//	}
//	
//	else if(placeableItemInstance.currentState == "greenUp")
//	{
//		newState="greenDown";
//	}
//	
//	else if(placeableItemInstance.currentState == "blueUp")
//	{
//		newState="blueDown";
//	}
//	
//	else
//	{
//		return;
//	}
//	
//	this.toggle(newState);
//};

net.lugdunon.server.worldgen.procedural.item.manifestation.BollardManifestationHandler.prototype.toggle=function(placeableItemInstance,inputBegan,state)
{
//	if(!inputBegan)
//	{
//		return;
//	}
//
//	if(
//		game.gameMode == 0 && 
//		net.lugdunon.util.Math.placeableRangeCheck(
//			placeableItemInstance.itemDef.props.stateChangeRange,
//			game.player.actualLoc,
//			placeableItemInstance
//		) && 
//		(placeableItemInstance.placer == "" || placeableItemInstance.canAccess(game.player) !== false)
//	)
//	{
//		if(placeableItemInstance.canAccess(game.player) === true)
//		{
//			game.client.sendCommand(
//				game.client.buildCommand(
//					"CORE.COMMAND.PLACEABLE.STATE.CHANGE",
//					{
//						piid :placeableItemInstance.itemInstanceId,
//						state:state
//					}
//				)
//			);
//		}
//		else
//		{
//			game.client.sendCommand(
//				game.client.buildCommand(
//					"CORE.COMMAND.CHECK.ACCESS",
//					{
//						piid    :placeableItemInstance.itemInstanceId,
//						callback:function(s)
//						{
//							if(s)
//							{
//								game.client.sendCommand(
//									game.client.buildCommand(
//										"CORE.COMMAND.PLACEABLE.STATE.CHANGE",
//										{
//											piid :placeableItemInstance.itemInstanceId,
//											state:state
//										}
//									)
//								);
//							}
//						}
//					}
//				)
//			);
//		}
//	}
};