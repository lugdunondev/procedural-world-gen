package net.lugdunon.server.worldgen.procedural.item.manifestation;

import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.state.item.manifestation.AnimatedManifestationHandler;
import net.lugdunon.state.item.manifestation.IManifestationHandler;

import org.json.JSONException;

public class DungeonTaskManifestationHandler extends AnimatedManifestationHandler implements IManifestationHandler
{
	@Override
	public int[] getImpassable(PlaceableItemInstance pii) throws JSONException
	{
		return(super.getImpassable(pii));
	}
}