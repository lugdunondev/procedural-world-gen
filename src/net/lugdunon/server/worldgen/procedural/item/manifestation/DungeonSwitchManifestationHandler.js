Namespace.declare("net.lugdunon.server.worldgen.procedural.item.manifestation");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler","net.lugdunon.state.item.manifestation.DefaultManifestationHandler");

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler,"init",[initData]);
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler.prototype.turnOn=function(placeableItemInstance,inputBegan)
{
	this.toggle(placeableItemInstance,inputBegan,"on");
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler.prototype.turnOff=function(placeableItemInstance,inputBegan)
{
	this.toggle(placeableItemInstance,inputBegan,"off");
};

net.lugdunon.server.worldgen.procedural.item.manifestation.DungeonSwitchManifestationHandler.prototype.toggle=function(placeableItemInstance,inputBegan,state)
{
	if(!inputBegan)
	{
		return;
	}

	if(
		game.gameMode == 0 && 
		net.lugdunon.util.Math.placeableRangeCheck(
			placeableItemInstance.itemDef.props.stateChangeRange,
			game.player.actualLoc,
			placeableItemInstance
		)
	)
	{
		game.client.sendCommand(
			game.client.buildCommand(
				"CORE.COMMAND.PLACEABLE.STATE.CHANGE",
				{
					piid :placeableItemInstance.itemInstanceId,
					state:state
				}
			)
		);
	}
};