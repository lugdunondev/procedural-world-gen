Namespace.declare("net.lugdunon.server.worldgen.procedural.item.tooltip");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonGlyphTooltipExtension","net.lugdunon.state.item.tooltip.ITooltipExtension");

////

net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonGlyphTooltipExtension.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonGlyphTooltipExtension,"init",[initData]);
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonGlyphTooltipExtension.prototype.renderTooltipExtension=function(itemDef,isPlaceableItemInstance,itemInstance)
{
	if(itemInstance != null && itemInstance.itemInstanceId > -1)
	{
		var udd  =null;

		if(isPlaceableItemInstance)
		{
			udd=itemInstance.getUserDefinedData();
		}
		else
		{
			udd=itemInstance.getPlaceableUserDefinedData();
		}
		
		if(udd != null && udd.image != null)
		{
			return("<div class='ttLineItem' style='height: 137px;'><div class='tooltipDungeonGlyphWrapper'><div style='background-image:url("+udd.image+");'></div></div></div>");
		}
		else
		{
			return("<div class='ttLineItem' style='height: 137px;'><div class='tooltipDungeonGlyphWrapper'><div style=''></div></div></div>");
		}
	}
	
	return(null);
};