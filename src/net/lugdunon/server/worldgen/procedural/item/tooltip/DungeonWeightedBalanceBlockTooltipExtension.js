Namespace.declare("net.lugdunon.server.worldgen.procedural.item.tooltip");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonWeightedBalanceBlockTooltipExtension","net.lugdunon.state.item.tooltip.ITooltipExtension");

////

net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonWeightedBalanceBlockTooltipExtension.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonWeightedBalanceBlockTooltipExtension,"init",[initData]);
	return(this);
};

net.lugdunon.server.worldgen.procedural.item.tooltip.DungeonWeightedBalanceBlockTooltipExtension.prototype.renderTooltipExtension=function(itemDef,isPlaceableItemInstance,itemInstance)
{
	if(itemInstance != null && itemInstance.itemInstanceId > -1)
	{
		var udd  =null;

		if(isPlaceableItemInstance)
		{
			udd=itemInstance.getUserDefinedData();
		}
		else
		{
			udd=itemInstance.getPlaceableUserDefinedData();
		}
		
		if(udd != null && udd.weight != null)
		{
			return("<div class='ttLineItem'>This block has a weight of <div style='color:#0F0'>"+udd.weight+"lbs</div></div>");
		}
		else
		{
			return("<div class='ttLineItem'>This block has not been weighted yet. It will be upon dungeon initialization.</div");
		}
	}
	
	return(null);
};