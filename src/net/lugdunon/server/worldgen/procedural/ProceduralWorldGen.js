Namespace.declare("net.lugdunon.server.worldgen.procedural");
Namespace.require("net.lugdunon.ui.SimpleKeyValuePropertyEditor");
Namespace.newClass("net.lugdunon.server.worldgen.procedural.ProceduralWorldGen","net.lugdunon.server.worldgen.IWorldGen");

////

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.ProceduralWorldGen,"init",[initData]);

	this.getConfigurableOptions()["world.cfg"].sort(function(a,b){return((a.key>b.key)?(1):((a.key<b.key)?(-1):(0)));});

	this.pwgConfig=null;
	this.noise    =[];
	this.post     =[];
	
	game.getStandaloneRequest(
		"rest/net/lugdunon/server/worldgen/procedural/configuration",
		{},
		function(data)
		{
			if(data && !data.error && data.pwgConfig)
			{
				this.pwgConfig=data.pwgConfig;
				this.noise    =data.noise;
				this.post     =data.post;
			}
		},
		this
	);
	
	this.propertyEditor=new net.lugdunon.ui.SimpleKeyValuePropertyEditor().init(
		{
			data        :this.getConfigurableOptions()["world.cfg"],
		    addStyles   :["worldGenProps"],
		    kvWidth     :"274",
			keyFieldType:{
				type           :net.lugdunon.ui.table.cell.content.InlineEditorContentCellRenderer.INPUT_WITH_SELECT,
				selectHandler  :this.handleKeySelectMode,
				selectPopulator:this.handleKeySelectPopulation,
				context        :this
			}
		}
	);
	
	return(this);
};

////

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.handleKeySelectMode=function(e)
{
	e.data.inputEl.val  ($(this).val());
	e.data.inputEl.focus(             );
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.handleKeySelectPopulation=function(select)
{
	game.getStandaloneRequest(
		"getAvailableWorldConfigProps",
		{
			worldGen:this.classId,
			worldId :this.worldId,
		},
		function(data)
		{
			select.html("");
			
			if(data)
			{
				for(var i=0;i<data.length;i++)
				{
					select.append($("<option/>").text(data[i].name).val(data[i].key));
				}
			}
		},
		this
	);
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.handlePreviewTerrain=function()
{
	var p      ={};
	var cfg    ={};
	var context=this;
	
	cfg["world.cfg"]={};

	this.wait=net.lugdunon.ui.Dialog.waitWithLog("Creating Preview");
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		cfg["world.cfg"][this.propertyEditor.getData()[i].key]=this.propertyEditor.getData()[i].value;
	}

	cfg["procedural.cfg"]=this.pwgConfig;
	p  ["config"        ]=JSON.stringify(cfg);
	
	game.getStandaloneRequest(
		"rest/net/lugdunon/server/worldgen/procedural/preview/image",
		p,
		function(data)
		{
			this.wait.close();

			//SHOW ERROR OCCURRED DIALOG
			if(data.status == false)
			{ 
				net.lugdunon.ui.Dialog.ok("Error","An error occurred while<br/>generating this preview.<br/>Check logs for more detail.",this,function(){return(true);});
			}
			//SHOW PREVIEW IMAGES UI
			else
			{
				var d=net.lugdunon.ui.Dialog.smallDialog("<span id='c'>World Preview</span>",612,599,{onclose:function(dialog){net.lugdunon.ui.Dialog.close(dialog);}});
				var c=0;
				var e;
				var a;
				var b;
				var p;
				
				d.html(
				    "<div class='terrainPreviewPage leftArrow' id='l'></div>"+
					"<div class='terrainPreview'>"+
					    "<canvas width='512' height='512' class='terrainPreviewImage' id='a' style='opacity:1.0;'/>"+
					    "<canvas width='512' height='512' class='terrainPreviewImage' id='b'/>"+
					"</div>"+
					"<div class='terrainPreviewPage rightArrow' id='r'></div>"
				);
				
				//LOAD IMAGES
				for(var i=0;i<data.order.length;i++)
				{
					var t=new Image();
					t.src=data.images[data.order[i]].image;
					data.images[data.order[i]].image=t;
				}
	
				a=d.find("#a");
				b=d.find("#b");
				e=a;

				a.ctxt=a[0].getContext("2d");
				b.ctxt=b[0].getContext("2d");
				
				p=function(i)
				{
					c+=i;
					
					if(c >= data.order.length)
					{
						c=0;
					}
					else if(c < 0)
					{
						c=data.order.length-1;
					}
	
					e.off("mousemove"            );
					e.off("mouseout"             );
					e.css("opacity",       "0.0" );
					e.css("pointer-events","none");
					e=e==a?b:a;

					e.css("opacity",         "1.0"                                      );
					e.css("pointer-events",  "all"                                      );
					e.ctxt.drawImage(data.images[data.order[c]].image,0,0,512,512);
					d.parent().find("#c").html(data.images[data.order[c]].name);
					
					//ADD PIXEL LEVEL TOOLTIP HANDLER
					if((data.order[c] == "biomes") || (data.order[c] == "terrain"))
					{
						e.on("mouseout",
							function(f)
							{
								game.hideLabel();
							}
						);
						e.on(
							"mousemove",
							function(f)
							{
								var p={x:game.input.offsetX(f),y:game.input.offsetY(f)};
								var t=null;
								var d=null;
								var q=null;
								
								if(data.order[c] == "biomes")
								{
									q=data.tileset.terrainLayers.overWorld.biomes;
								}
								else if(data.order[c] == "terrain")
								{
									q=data.tileset.terrainLayers.overWorld.layers;
								}
								
								d=e.ctxt.getImageData(p.x,p.y,1,1).data;
								d=net.lugdunon.util.Color.toHexString(d[0],d[1],d[2]).substring(1);
								
								for(var i=0;i<q.length;i++)
								{
									if(q[i].color == d)
									{
										t=q[i].name;
										break;
									}
								}
								
								if(t != null)
								{
									game.showLabel(
										t,
										{
											noShim:true,
											top   :game.input.absoluteCursor.y-32,
											left  :game.input.absoluteCursor.x
										}
									);
								}
								else
								{
									game.hideLabel();
								}
							}
						);
					}
				};
				
				d.find("#l").on(
					"click",
					function()
					{
						p(-1);
					}
				);
				d.find("#r").on(
					"click",
					function()
					{
						p( 1);
					}
				);

				e.ctxt.drawImage(data.images[data.order[c]].image,0,0,512,512);
				d.parent().find("#c").html(data.images[data.order[c]].name+" Preview Image");
			}
		},
		this
	);
	
	//KICKOFF LOGGING
	this.getProgress=function()
	{
		game.getStandaloneRequest(
			"rest/net/lugdunon/server/worldgen/procedural/preview/image/progress",
			{},
			function(data)
			{
				if(data && data.logEntry)
				{
					for(var i=0;i<data.logEntry.length;i++)
					{
						context.wait.appendToLog(data.logEntry[i]);
					}
					
					setTimeout(function(){context.getProgress.call(context);},200);
				}
			},
			context
		);
	}
	
	setTimeout(function(){context.getProgress.call(context);},500);
};

////

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.renderExpertUI=function()
{
	this.parent.getContainer().html(Namespace.requireHTML(this.classId+"Expert"));
	
	this.pwgConfig.noise.args.featureSize=64;
	
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.renderAdvancedUI=function()
{
	this.propertyEditor.setContainer(this.parent.getContainer());
	this.propertyEditor.render      ();
	this.parent        .getContainer().find(".tableBody").css("height",(this.parent.getContainer().height()-30)+"px");
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.renderSimplifiedUI=function()
{
	this.parent.getContainer().html(Namespace.requireHTML(this.classId));

	var simpleEd=["name","net.lugdunon.server.worldgen.procedural.world.size","seed"];//"item.death.penalty","vendor.markup","vendor.repair.discount","vendor.max.buyback"];
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		for(var j=0;j<simpleEd.length;j++)
		{
			if(this.propertyEditor.getData()[i].key == simpleEd[j])
			{
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).val (this.propertyEditor.getData()[i].value);
				this.parent.getContainer().find(".label[for='"+simpleEd[j]+"']"      ).html(this.propertyEditor.getData()[i].ttLabel);
				this.parent.getContainer().find(".description[for='"+simpleEd[j]+"']").html(this.propertyEditor.getData()[i].ttDescription);
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).on(
					"blur",
					{key:simpleEd[j],context:this},
					function(e)
					{
						e.data.context.setValueFor.call(e.data.context,e.data.key,$(this).val());
					}
				);
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).on(
					"keydown",
					{key:simpleEd[j],context:this},
					function(e)
					{
						if(e.keyCode == 13)
						{
							e.data.context.setValueFor.call(e.data.context,e.data.key,$(this).val());
						}
					}
				);
				
				break;
			}
		}
	}
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.setValueFor=function(key,value)
{
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		if(this.propertyEditor.getData()[i].key == key)
		{
			this.propertyEditor.getData()[i].value=value;
			break;
		}
	}
};


//////


net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.getName=function()
{
	return("Procedural World Generation");
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.getDescription=function()
{
	return("Creates a new randomly generated instance.");
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.buildCreateUI=function(parent)
{
	this.parent  =parent;
	this.renderSimplifiedUI();
	this.parent.addAuxButton(
		"simpleMode",
		"Simplified",
		this.renderSimplifiedUI,
		this
	);
	this.parent.addAuxButton(
		"advancedMode",
		"Advanced",
		this.renderAdvancedUI,
		this
	);
	this.parent.addAuxButton(
		"expertMode",
		"Expert",
		this.renderExpertUI,
		this
	);
	this.parent.addAuxButton(
		"genPreview",
		"Preview",
		this.handlePreviewTerrain,
		this
	);
};


net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.buildConfigureUI=function(parent)
{
	this.parent  =parent;
	this.renderSimplifiedUI();
	this.parent.addAuxButton(
		"simpleMode",
		"Simplified",
		this.renderSimplifiedUI,
		this
	);
	this.parent.addAuxButton(
		"advancedMode",
		"Advanced",
		this.renderAdvancedUI,
		this
	);
	this.parent.addAuxButton(
		"expertMode",
		"Expert",
		this.renderExpertUI,
		this
	);
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.handleCreation=function()
{
	var p      ={};
	var cfg    ={};
	var context=this;
	
	cfg["world.cfg"]={};
	
	this.wait=net.lugdunon.ui.Dialog.waitWithLog("Creating Campaign");
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		cfg["world.cfg"][this.propertyEditor.getData()[i].key]=this.propertyEditor.getData()[i].value;
	}

	cfg["procedural.cfg"]=this.pwgConfig;
	p  ["worldGen" ]     =this.classId;
	p  ["config"   ]     =JSON.stringify(cfg);
	
	game.postStandaloneRequest(
		"createWorld",
		p,
		this.worldCreationReturned,
		this
	);
	
	//KICKOFF LOGGING
	this.getProgress=function()
	{
		game.getStandaloneRequest(
			"createWorldProgress",
			{},
			function(data)
			{
				if(data && data.logEntry)
				{
					for(var i=0;i<data.logEntry.length;i++)
					{
						context.wait.appendToLog(data.logEntry[i]);
					}
					
					setTimeout(function(){context.getProgress.call(context);},200);
				}
			},
			context
		);
	}

	setTimeout(function(){context.getProgress.call(context);},500);
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.worldCreationReturned=function(data)
{
	this.wait.close();
	
	if(data.status === true)
	{
		game.loadGameState("net.lugdunon.states.list.SinglePlayerOptionsList",null,true);
	}
	else
	{
		net.lugdunon.ui.Dialog.ok("Error!","An error occurred:<br/><br/>"+data.message);
	}
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.handleConfiguration=function()
{
	var cfg={};
	cfg["world.cfg"]={};
	
	this.wait=net.lugdunon.ui.Dialog.wait("Reconfiguring Campaign");
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		cfg["world.cfg"][this.propertyEditor.getData()[i].key]=this.propertyEditor.getData()[i].value;
	}

	game.postStandaloneRequest(
		"configureWorld",
		{
			worldGen :this.classId,
			worldId  :this.worldId,
			config   :JSON.stringify(cfg),
			pwgConfig:this.pwgConfig
		},
		this.worldConfigurationReturned,
		this
	);
};

net.lugdunon.server.worldgen.procedural.ProceduralWorldGen.prototype.worldConfigurationReturned=function(data)
{
	this.wait.close();
	
	if(data.status === true)
	{
		game.loadGameState("net.lugdunon.states.list.SinglePlayerOptionsList",null,true);
	}
	else
	{
		net.lugdunon.ui.Dialog.ok("Error!","An error occurred:<br/><br/>"+data.message);
	}
};