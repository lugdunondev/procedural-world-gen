package net.lugdunon.server.worldgen.procedural.command;

import java.io.IOException;

import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.gm.GmOnlyCommand;
import net.lugdunon.server.worldgen.procedural.instance.tile.TileBasedDungeonHelper;
import net.lugdunon.state.State;
import net.lugdunon.world.instance.Instance;

//TODO: FIX FOR UI OUTPUT.

public class SaveDungeonTileCommand extends GmOnlyCommand
{
	private Response iRes;
	
	@Override
	public String getCommandId()
	{
	    return("PROCEDURAL.SAVE.DUNGEON.TILE");
	}

	@Override
	public String getName()
	{
	    return("Save a Dungeon Tile");
	}

	@Override
	public String getDescription()
	{
	    return("Saves a dungeon tile for use in procedurally generated tiled dungeons.");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(0);
	}
	
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public void handleAsGm(CommandRequest request) throws IOException
    {
    	Response oRes;
    	boolean  success=false;

		if(State.instance().isGm(request.getOrigin().getAccount().getAccountName()))
		{
			Instance instance =State.instance().getWorld().getInstance(request.getOrigin().getAccount().getActiveCharacter().getInstanceId());
//			int      index    =request.getData().read();
//			int      tileIndex=-1;
//			
//			if(request.getData().readBoolean())
//			{
//				tileIndex=request.getData().read();
//			}
//			else
//			{
//				tileIndex=TileBasedDungeonHelper.getNextAvailableTileIndex();
//			}
//			
//			//TODO: READ NAME
//			//TODO: READ PUZZLE FLAGS
//			//TODO: READ DIFFICULTY
//			
//			success=TileBasedDungeonHelper.saveTile(index, tileIndex, instance);
			
			for(int i=0;i<4;i++)
			{
				int      tileIndex=TileBasedDungeonHelper.getNextAvailableTileIndex();
				
				//TODO: READ NAME
				//TODO: READ PUZZLE FLAGS
				//TODO: READ DIFFICULTY
				
				success=TileBasedDungeonHelper.saveTile(i, tileIndex, instance);
			}
		}
		
		iRes=initializeInternalResponse();

		iRes.out.writeBoolean(success);
	    
		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		request.getOrigin().getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
    }
}