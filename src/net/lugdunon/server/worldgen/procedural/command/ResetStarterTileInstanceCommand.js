Namespace.declare("net.lugdunon.command.core.console.gm");

Namespace.newClass("net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand,"init",[initData]);
	
	this.msg;
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand.prototype.opInit=function(initData)
{
	this.msg=initData;
};

net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand.prototype.getCommandLength=function()
{
	if(this.msg != "")
	{
	    return(this.msg.lengthInBytes());
	}
	
	return(0);
};

net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand.prototype.buildCommand=function(dataView)
{
	if(this.msg != "")
	{
		dataView.writeString(this.msg);
	}
};

net.lugdunon.server.worldgen.procedural.command.ResetStarterTileInstanceCommand.prototype.commandResponse=function(res)
{
	if(res.readBoolean())
	{
		console.log(true);
	}
	else
	{
		console.log(false);
	}
};