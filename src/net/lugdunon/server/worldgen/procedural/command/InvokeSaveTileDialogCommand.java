package net.lugdunon.server.worldgen.procedural.command;

import java.io.IOException;

import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.console.gm.GmOnlyConsoleFiredCommand;
import net.lugdunon.state.State;
import net.lugdunon.world.instance.Instance;

import org.json.JSONException;

public class InvokeSaveTileDialogCommand extends GmOnlyConsoleFiredCommand
{
	private Response iRes;
	
	@Override
    public String getConsoleOpCodeAlias()
    {
	    return("proc.savetile");
    }

	@Override
    public String[] getConsoleOpCodeAliases()
    {
	    return(new String[]{"proc.st"});
    }

	@Override
    public ConsoleCommandParameter[] getConsoleCommandParameters()
    {
	    return(
	    	new ConsoleCommandParameter[]
	    	{
		    		new ConsoleCommandParameter("","Invokes the save tile dialog.")
    		}
	    );
    }
	
	@Override
	public String getCommandId()
	{
	    return("PROCEDURAL.INVOKE.SAVE.TILE.UI");
	}

	@Override
	public String getName()
	{
	    return("Open the save dungeon tile dialog");
	}

	@Override
	public String getDescription()
	{
	    return("Opens the save dungeon tile dialog.");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(0);
	}
	
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public void handleAsGm(CommandRequest request, String consoleMessage) throws IOException
    {
		Response oRes;
		Instance instance=State.instance().getWorld().getInstance(request.getOrigin().getAccount().getActiveCharacter().getInstanceId());
		boolean  allowed =false;
		
		iRes=initializeInternalResponse();
		
		try
        {
			allowed=(
	        	//IS GM
	        	State.instance().isGm(request.getOrigin().getAccount().getAccountName()) &&
	        	(
	        		//IS DUNGEON STARTER TILE INSTANCE
	        		instance.getInstanceProperties().has       ("isDungeonStarterTile") && 
	        		instance.getInstanceProperties().getBoolean("isDungeonStarterTile")
	        	)
	        );
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
		
		iRes.out.writeBoolean(allowed);
		
		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		request.getOrigin().getConnection().sendMessage(oRes.bytes(),0,oRes.length());
    }
}