package net.lugdunon.server.worldgen.procedural.command;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import net.lugdunon.Game;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.console.gm.GmOnlyConsoleFiredCommand;
import net.lugdunon.math.Point;
import net.lugdunon.math.Rect;
import net.lugdunon.state.Account;
import net.lugdunon.state.State;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.instance.Instance;
import net.lugdunon.world.terrain.Terrain;

import org.json.JSONException;

public class ResetStarterTileInstanceCommand extends GmOnlyConsoleFiredCommand
{
	private Response iRes;
	
	@Override
    public long getCommandCompletionThreshold()
    {
	    return(Game.INFINITE_COMPLETE_THRESHOLD);
    }

	@Override
    public String getConsoleOpCodeAlias()
    {
	    return("proc.reset.st");
    }

	@Override
    public String[] getConsoleOpCodeAliases()
    {
	    return(new String[]{"proc.rst"});
    }

	@Override
    public ConsoleCommandParameter[] getConsoleCommandParameters()
    {
	    return(
	    	new ConsoleCommandParameter[]
	    	{
		    	new ConsoleCommandParameter("{index}","Resets the specified indexed tile in the starter tile instance."),
		    	new ConsoleCommandParameter("","Resets the starter tile instance.")
    		}
	    );
    }
	
	@Override
	public String getCommandId()
	{
	    return("PROCEDURAL.RESET.STARTER.TILE.INSTANCE");
	}

	@Override
	public String getName()
	{
	    return("Reset the starter tile instance");
	}

	@Override
	public String getDescription()
	{
	    return("Resets the starter tile instance.");
	}
	
	@Override
	public int getCommandLength()
	{
	    return(0);
	}
	
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
    public void handleAsGm(CommandRequest request, String consoleMessage) throws IOException
    {
		Response oRes;
		Instance instance=State.instance().getWorld().getInstance(request.getOrigin().getAccount().getActiveCharacter().getInstanceId());
		boolean  success =false;
		
		iRes=initializeInternalResponse();

		try
        {
	        if(
	        	//IS GM
	        	State.instance().isGm(request.getOrigin().getAccount().getAccountName()) &&
	        	(
	        		//IS DUNGEON STARTER TILE INSTANCE
	        		instance.getInstanceProperties().has       ("isDungeonStarterTile") && 
	        		instance.getInstanceProperties().getBoolean("isDungeonStarterTile")
	        	)
	        )
	        {
	        	int index=-1;
	        	
	    		try
	    		{
	    			index=Integer.parseInt(consoleMessage);
	    		}
	    		catch(Exception e)
	    		{
	    			;
	    		}
	    		
	    		success=resetInstance(request.getOrigin().getAccount(),instance,index);
	        }
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
		
		iRes.out.writeBoolean(success);
		
		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		request.getOrigin().getConnection().sendMessage(oRes.bytes(),0,oRes.length());
    }

	private boolean resetInstance(Account account, Instance instance, int index)
    {
        Set<Integer> chunksUpdated=new TreeSet<Integer>();
		Point   p                 =new Point();
		Terrain t                 =instance.getTerrain();
		short   d                 =t.getSize();
		Rect    bounds            =new Rect(0,0,d,d);
		
		if(index > -1)
		{
			bounds.getSize().setH(32);
			bounds.getSize().setW(32);
			
			switch(index)
			{
				case 0:
				{
					bounds.getPosition().setX( 0);
					bounds.getPosition().setY( 0);
					
					break;
				}
				case 1:
				{
					bounds.getPosition().setX(32);
					bounds.getPosition().setY( 0);
					
					break;
				}
				case 2:
				{
					bounds.getPosition().setX( 0);
					bounds.getPosition().setY(32);
					
					break;
				}
				case 3:
				{
					bounds.getPosition().setX(32);
					bounds.getPosition().setY(32);
					
					break;
				}
				default:
				{
					return(false);
				}
			}
		}
		
		//CLEAR PLACEABLES
		{
			Collection<PlaceableItemInstance> piis=instance.getPlaceableItemCache().listAllItemsInRegion(bounds, false);
			
			for(PlaceableItemInstance pii:piis)
			{
				if(
					!(
						"DUNGEON.GATE.EAST.WEST"  .equals(pii.getItemDef().getItemId()) ||
						"DUNGEON.GATE.NORTH.SOUTH".equals(pii.getItemDef().getItemId())
					)
				)
				{
					try
					{
						CommandProperties props=new CommandProperties();
						
						props.setPlaceableItemInstance("placeableItemInstance",pii                         );
						props.setCharacter            ("playerCharacter",      account.getActiveCharacter());
						
			            State.instance().getGame().addIncomingRequest(
		            		"CORE.COMMAND.REMOVE.PLACEABLE.ITEM",
		            		props
			            );
		            }
		            catch (Exception e)
		            {
			            e.printStackTrace();
		            }
				}
			}
		}
		
		//ZERO ELEVATION AND SET GROUND TO DEFAULT 
		for(int i=bounds.getX();i<bounds.getX()+bounds.getW();i++)
		{
			for(int j=bounds.getY();j<bounds.getY()+bounds.getH();j++)
			{
				if(
					!(
						(i == 0 || i == 31 || i == 32 || i == 63) ||
						(j == 0 || j == 31 || j == 32 || j == 63)
					)
				)
				{
					t.setElevationData(FastMath.pointToIndex(p.set(i,j),d),(byte) 0);
					
					try
					{
						chunksUpdated.addAll(
								t.updateLayers(
									account,
									4, // DUNGEON
									i,
									j,
									Terrain.TERRAIN_EDIT_MODE_EXCAVATE
								)
							);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}

	    //INFORM OF TERRAIN EDIT
		try
        {
			CommandProperties props=new CommandProperties();
			
			props.setInstance("instance",     instance     );
			props.set        ("chunksUpdated",chunksUpdated);
		
	        State.instance().getGame().addIncomingRequest(
	        	"CORE.COMMAND.EDIT.TERRAIN",
	        	props
	        );
        }
        catch (Exception e)
        {
	        e.printStackTrace();
        }
		
	    return(true);
    }
}