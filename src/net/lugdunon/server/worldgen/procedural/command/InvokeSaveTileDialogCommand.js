Namespace.declare("net.lugdunon.command.core.console.gm");

Namespace.newClass("net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand,"init",[initData]);
	
	this.msg;
	
	return(this);
};

net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand.prototype.opInit=function(initData)
{
	this.msg=initData;
};

net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand.prototype.getCommandLength=function()
{
	if(this.msg != "")
	{
	    return(this.msg.lengthInBytes());
	}
	
	return(0);
};

net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand.prototype.buildCommand=function(dataView)
{
	if(this.msg != "")
	{
		dataView.writeString(this.msg);
	}
};

net.lugdunon.server.worldgen.procedural.command.InvokeSaveTileDialogCommand.prototype.commandResponse=function(res)
{
	if(res.readBoolean() && game.instanceImplementation && game.instanceImplementation.showSaveDungeonTileDialog)
	{
		try
		{
			game.instanceImplementation.showSaveDungeonTileDialog();
		}
		catch(e)
		{
			console.log(e);
		}
	}
};